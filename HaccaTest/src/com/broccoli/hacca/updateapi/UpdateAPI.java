package com.broccoli.hacca.updateapi;

import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;


public interface UpdateAPI {
	void updateStudentProfile(StudentPersonnelInfo studentPersonnelInfo);
	void updateCompanyProfile(CompanyPersonnelInfo companyPersonnelInfo);
	void updateProfessorProfile(ProfessorPersonnelInfo professorPersonnelInfo);
}
