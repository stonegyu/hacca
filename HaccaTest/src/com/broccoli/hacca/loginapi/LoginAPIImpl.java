package com.broccoli.hacca.loginapi;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.util.Log;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.passer.JsonParser;
import com.broccoli.hacca.passer.Parser;
import com.broccoli.hacca.urlfactory.UrlType;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;

public class LoginAPIImpl implements LoginAPI,OnDataBaseConnectAPIListener{
	private final String TAG = "LoginAPIImpl";
	private OnLoginAPIListener loginAPIListener;

	public LoginAPIImpl(OnLoginAPIListener loginAPIListener){
		this.loginAPIListener = loginAPIListener;
	}
	
	@Override
	public void login(String loginId, String password) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.LOGIN);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, loginId);
		urlDataStorage.setParameter(UrlParameterType.PASSWORD, password);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
		
		Log.i(TAG, url);
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		//맨앞에 ASCII CODE 65279가 붙음
		result = result.replace(String.valueOf((char)65279), "" );
		
		Log.i(TAG, result+"");
		
		if(result.equals("false")){
			loginAPIListener.onFailLogin();
		}else{
			Parser parser =new Parser(new JsonParser(result));
			loginAPIListener.onSuccessLogin(parser.parse("userType").get(0), parser.parse("userName").get(0));
		}
	}

	@Override
	public void onFailToConnectDataBase() {
		loginAPIListener.onFailLogin();
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		loginAPIListener.onTimeoutToLogin();
	}

}
