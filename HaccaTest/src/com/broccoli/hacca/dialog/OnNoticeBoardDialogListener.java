package com.broccoli.hacca.dialog;

public interface OnNoticeBoardDialogListener {
	void onCompletedEditNoticeBoard(String title,String content);
}
