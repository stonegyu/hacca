package com.broccoli.hacca.dialog;

public interface OnCustomDatePickerDialogSetListener {
	void onCompletedDatePickerDialogSettings(int startYear, int startMonth,
			int startDay, int endYear, int endMonth, int endDay);
}
