package com.broccoli.hacca.dialog;

public interface OnCompleteCommentExamDialogListener {
	void onCompletedCommentExamDialog(String content);
}
