package com.broccoli.hacca.dialog;

import com.broccoli.hacca.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

public class CustiomDatePickerDialog {

	private OnCustomDatePickerDialogSetListener dialogListener;
	private Context context;

	public CustiomDatePickerDialog(Context context) {
		this.context = context;
	}

	public void setOnDialogListener(
			OnCustomDatePickerDialogSetListener dialogListener) {
		this.dialogListener = dialogListener;
	}

	public void show() {
		View dialogView = ((LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.custom_date_picker_layout, null);

		final DatePicker startDatePicker = (DatePicker) dialogView
				.findViewById(R.id.start_date);
		final DatePicker endDatePicker = (DatePicker) dialogView
				.findViewById(R.id.end_date);

		new AlertDialog.Builder(context)
				.setTitle("기간을 설정해주세요")
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						dialogListener.onCompletedDatePickerDialogSettings(
								startDatePicker.getYear(),
								startDatePicker.getMonth(),
								startDatePicker.getDayOfMonth(),
								endDatePicker.getYear(),
								endDatePicker.getMonth(),
								endDatePicker.getDayOfMonth());
						dialog.dismiss();
					}
				})
				.setNegativeButton("취소", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setView(dialogView).show();
	}
}
