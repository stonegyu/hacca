package com.broccoli.hacca.dialog;

public interface OnHansungUnivCertificationDialogListener {
	void onCompletedHansungUnivCertificationDialogSetting(String certificationId,String certificationPassword);
}
