package com.broccoli.hacca.dialog;

public interface OnCustomDatePickerDialogListener {
	void onCompletedCompanyDatePickerDialog(int id,int year,int month,int date);
}
