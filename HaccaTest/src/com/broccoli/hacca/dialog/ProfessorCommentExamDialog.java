package com.broccoli.hacca.dialog;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.broccoli.hacca.R;

public class ProfessorCommentExamDialog extends Dialog  {
	private ListView commentExamListView = null;
	private ListViewAdapter commentExamAdapter = null;
	private String professorCommentList[];
	private String selectedComment;
	private Context mContext;
	private String SelPosition = null;
	private OnCompleteCommentExamDialogListener CommentExamDialogListener;
	private ImageButton commentOkBtn;
	public ProfessorCommentExamDialog(Context context) {
		super(context);
		mContext = context;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setContentView(R.layout.professor_comment_load_layout);
		professorCommentList = mContext.getResources().getStringArray(
				R.array.professor_comment_examlist);
		commentExamListView = (ListView) findViewById(R.id.professor_comment_listview);
		commentExamAdapter = new ListViewAdapter(mContext, professorCommentList.length);
		commentExamListView.setAdapter(commentExamAdapter);
		for (int i = 0; i < professorCommentList.length; i++) {
			commentExamAdapter.addItem(professorCommentList[i]);
		}
	
		commentOkBtn = (ImageButton)findViewById(R.id.professor_comment_addbtn);
		commentOkBtn.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					v.setBackgroundResource(R.drawable.comment_ok_btn_02_ative);
				}
				if(event.getAction() == MotionEvent.ACTION_UP){
					v.setBackgroundResource(R.drawable.comment_ok_btn_01_basic);
					CommentExamDialogListener.onCompletedCommentExamDialog(SelPosition);
				}
				if(event.getAction() == MotionEvent.ACTION_CANCEL){
					v.setBackgroundResource(R.drawable.comment_ok_btn_01_basic);
				}
				return false;
			}
		});
	}

	private class ViewHolder {
		public TextView commentExam;
		public ImageButton commentSelectBtn;
		public LinearLayout commentSelectLayout;
	}

	private class ListViewAdapter extends BaseAdapter {

		private Context mContext = null;
		private ArrayList<ListData> mListData = new ArrayList<ListData>();
		public ListViewAdapter(Context mContext, int size) {
			super();
			this.mContext = mContext;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mListData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mListData.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder;
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.professor_comment_load_child_layout, null);
			convertView.setTag(holder);
			holder.commentExam = (TextView) convertView
					.findViewById(R.id.professor_comment_exam);
			holder.commentSelectBtn = (ImageButton) convertView
					.findViewById(R.id.professor_comment_exam_btn);

			holder.commentSelectLayout = (LinearLayout) convertView
					.findViewById(R.id.professor_comment_layout);
			
			holder.commentSelectLayout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					/*
					for(int i=0; i<CommentBtns.length; i++){
						CommentBtns[i].setBackgroundResource(R.drawable.cheack_btn_01_basic);
					}
					*/
					int position = v.getId();
					ListData data = (ListData)getItem(position);
					SelPosition = data.getComment();
				}
			});

			ListData mData = mListData.get(position);
			holder.commentSelectLayout.setId(position);
			holder.commentExam.setText(mData.getComment());
			return convertView;
		}

		public void addItem(String comment) {
			ListData addInfo = null;
			addInfo = new ListData();
			addInfo.comment = comment;
			mListData.add(addInfo);
		}
	}

	public class ListData {
		private String comment;
		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

	}

	public void setOnDialogListener(
			OnCompleteCommentExamDialogListener CommentExamDialogListener) {
		this.CommentExamDialogListener = CommentExamDialogListener;
	}

}
