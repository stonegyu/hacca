package com.broccoli.hacca.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.broccoli.hacca.R;

public class HansungUnivCertificationDialog {

	private Context context;
	private OnHansungUnivCertificationDialogListener dialogListener;

	public HansungUnivCertificationDialog(Context context){
		this.context = context;
	}
	
	public void setOnDialogListener(
			OnHansungUnivCertificationDialogListener dialogListener) {
				this.dialogListener = dialogListener;
	}

	public void show() {
		View dialogView = ((LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.student_certification_dialog_layout, null);
		
		final EditText certificationId = (EditText) dialogView.findViewById(R.id.certification_id);
		final EditText certificationPassword = (EditText) dialogView.findViewById(R.id.certification_password);
		
		new AlertDialog.Builder(context)
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						
						if(certificationId.getText().toString() == null || certificationId.getText().toString().replace(" ", "").equals("")){
							Toast.makeText(context, "아이디를 입력해주세요.", Toast.LENGTH_SHORT).show();
							return;
						}
						
						if(certificationPassword.getText().toString() == null || certificationPassword.getText().toString().replace(" ", "").equals("")){
							Toast.makeText(context, "비밀번호를 입력해주세요.", Toast.LENGTH_SHORT).show();
							return;
						}
						
						dialogListener.onCompletedHansungUnivCertificationDialogSetting(certificationId.getText().toString(), certificationPassword.getText().toString());
						
						dialog.dismiss();
					}
				})
				.setNegativeButton("취소", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setView(dialogView).show();
	}
}
