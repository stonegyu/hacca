package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.broccoli.hacca.R;

public class CompanyMainPageActivity extends Activity implements OnClickListener, OnTouchListener{

	ImageButton companyPushBtn, companyTelentBtn, companyProfileBtn;
	ImageButton companyPushBtnSub, companyTelentBtnSub, companyProfileBtnSub;
	TextView companyPushCount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.company_main_page_layout);
		
		companyPushBtn = (ImageButton)findViewById(R.id.company_push_btn);
		companyTelentBtn = (ImageButton)findViewById(R.id.company_telent_btn);
		companyProfileBtn = (ImageButton)findViewById(R.id.company_profile_btn);
		companyPushBtnSub = (ImageButton)findViewById(R.id.company_push_direction_btn);
		companyTelentBtnSub = (ImageButton)findViewById(R.id.company_telent_direction_btn);
		companyProfileBtnSub = (ImageButton)findViewById(R.id.company_profile_direction_btn);
		
		companyPushBtn.setOnClickListener(this);
		companyTelentBtn.setOnClickListener(this);
		companyProfileBtn.setOnClickListener(this);
		companyPushBtn.setOnTouchListener(this);
		companyTelentBtn.setOnTouchListener(this);
		companyProfileBtn.setOnTouchListener(this);
		
		companyPushCount = (TextView)findViewById(R.id.company_push_count_text);
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ImageButton view = (ImageButton)v;
		if(v.getId() == R.id.company_push_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_push_02_active);
				companyPushBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_push_01_basic);
				companyPushBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_push_01_basic);
				companyPushBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.company_telent_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_telent_02_active);
				companyTelentBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_telent_01_basic);
				companyTelentBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_telent_01_basic);
				companyTelentBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.company_profile_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_profile_02_active);
				companyProfileBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				companyProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				companyProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		if(v.getId() == R.id.company_push_btn){
			startActivity(new Intent(this,PushNotificationListPageActivity.class));
		}
		if(v.getId() == R.id.company_telent_btn){
<<<<<<< HEAD
			startActivity(new Intent(this,StudentListPageActivity.class));
=======
			startActivity(new Intent(this,TelentCategoryActivity.class));
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		}
		if(v.getId() == R.id.company_profile_btn){
			startActivity(new Intent(this,CompanyViewProfilePageActivity.class));
		}
	}
}
