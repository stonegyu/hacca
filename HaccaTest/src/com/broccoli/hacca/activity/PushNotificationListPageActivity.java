package com.broccoli.hacca.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.broccoli.hacca.R;
import com.broccoli.hacca.adapter.noticeboard.NoticeBoardPagerAdapter;
import com.broccoli.hacca.adapter.pushnotification.PushNotificationPagerAdapter;
import com.broccoli.hacca.opensource.PagerSlidingTabStrip;

public class PushNotificationListPageActivity extends Activity implements OnClickListener{
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private PushNotificationPagerAdapter adapter;
	
	private ImageButton exitBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.push_notification_list_page_activity_layout);
		
		tabs = (PagerSlidingTabStrip)findViewById(R.id.pager_tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		adapter = new PushNotificationPagerAdapter(this);

		tabs.setIndicatorColor(Color.GREEN);
		tabs.setShouldExpand(true);
		pager.setAdapter(adapter);

		tabs.setViewPager(pager);
		
		exitBtn = (ImageButton) findViewById(R.id.exit);
		exitBtn.setImageResource(R.drawable.page_exit_button);
		exitBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.exit){
			finish();
		}
	}
}
