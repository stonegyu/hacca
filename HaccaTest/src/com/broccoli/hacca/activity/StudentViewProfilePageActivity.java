package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.passer.searchparser.StudentPersonnelInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class StudentViewProfilePageActivity extends Activity implements OnTouchListener, OnClickListener, OnSearchAPIListener {
	TextView studentViewName, studentViewSex, studentViewAge, studentViewDepartment,
			studentViewHope, studentViewAbility, studentViewCareer,
<<<<<<< HEAD
			studentViewHomepage, studentViewBlog, studentViewEmail;
=======
			studentViewBlog, studentViewEmail;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	ImageButton studentViewEditBtn;
	private LoadingProgressDialog loadingProgressDialog;
	private String studentLoginId, registrationIdOfDevice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.student_view_profile_page_activity_layout);
		studentViewName = (TextView) findViewById(R.id.student_view_name);
		studentViewSex = (TextView) findViewById(R.id.student_view_sex);
		studentViewAge = (TextView) findViewById(R.id.student_view_age);
		studentViewDepartment = (TextView) findViewById(R.id.student_view_department);
		studentViewHope = (TextView) findViewById(R.id.student_view_hope);
		studentViewAbility = (TextView) findViewById(R.id.student_view_ability);
		studentViewCareer = (TextView) findViewById(R.id.student_view_career);
<<<<<<< HEAD
		studentViewHomepage = (TextView) findViewById(R.id.student_view_hompage);
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		studentViewBlog = (TextView) findViewById(R.id.student_view_blog);
		studentViewEmail = (TextView) findViewById(R.id.student_view_email);
		studentViewEditBtn = (ImageButton) findViewById(R.id.student_view_editbtn);
		studentViewEditBtn.setOnClickListener(this);
		studentViewEditBtn.setOnTouchListener(this);

		loadingProgressDialog = new LoadingProgressDialog(this);

		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);

		studentLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();

		if (studentLoginId != null) {
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new StudentPersonnelInfoParser());
			searchAPI.searchStudentInfo(studentLoginId);
			loadingProgressDialog.show();
		}
	}

	@Override
	public void onClick(View v) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		if(v.getId() == R.id.student_view_editbtn){
			startActivity(new Intent(this, StudentEditProfilePageActivity.class));
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ImageButton view = (ImageButton) v;
		if (view.getId() == R.id.student_view_editbtn) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
		}
		return false;
	}

	private void setStudentInfo(StudentPersonnelInfo studentPersonnelInfo) {
		studentViewName.setText(studentPersonnelInfo.getStudentName());
		studentViewSex.setText(studentPersonnelInfo.getStudentSex());
		studentViewAge.setText(studentPersonnelInfo.getStudentAge());
		studentViewDepartment.setText(studentPersonnelInfo.getStudentDepartment());
		studentViewHope.setText(studentPersonnelInfo.getStudentHope());
		studentViewAbility.setText(studentPersonnelInfo.getStudentAbility());
		studentViewCareer.setText(studentPersonnelInfo.getStudentCareer());
		studentViewBlog.setText(studentPersonnelInfo.getStudentBlog());
		studentViewEmail.setText(studentPersonnelInfo.getStudentMail());
	}
	
	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		StudentPersonnelInfo studentPersonnelInfo = pageInfo
				.getStudentPersonnelInfo();
		setStudentInfo(studentPersonnelInfo);

		loadingProgressDialog.dismiss();
	}

	@Override
	public void onFailSearch() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSearch() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

}
