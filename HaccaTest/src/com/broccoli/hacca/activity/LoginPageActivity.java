package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.DeviceChangeQuestionDalog;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.dialog.OnDeviceChangeQuestionListener;
import com.broccoli.hacca.gcm.GCMIntentService;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.loginapi.LoginAPI;
import com.broccoli.hacca.loginapi.LoginAPIImpl;
import com.broccoli.hacca.loginapi.OnLoginAPIListener;
import com.broccoli.hacca.registrationidofdeviceapi.OnCheckRegistrationIdOfDeviceListener;
import com.broccoli.hacca.registrationidofdeviceapi.OnRegisterIdOfDeviceGetterListener;
import com.broccoli.hacca.registrationidofdeviceapi.OnUpdateRegistrationIdOfDeviceListener;
import com.broccoli.hacca.registrationidofdeviceapi.RegistrationIdOfDeviceAPI;
import com.broccoli.hacca.registrationidofdeviceapi.RegistrationIdOfDeviceAPIImpl;
import com.broccoli.hacca.registrationidofdeviceapi.RegistrationIdOfDeviceGetter;
import com.google.android.gcm.GCMRegistrar;

public class LoginPageActivity extends Activity implements OnLoginAPIListener,
		OnCheckRegistrationIdOfDeviceListener,OnUpdateRegistrationIdOfDeviceListener ,
		OnRegisterIdOfDeviceGetterListener, OnDeviceChangeQuestionListener,OnTouchListener{

	private final String TAG = "LoginPageActivity";
	
	private ImageView LoginBtn;
	private EditText Login_id, Login_pw;
	
	private String registrationIdOfDevice=null;
	private String userType;
	private String userName;
	
	private LoadingProgressDialog loadingProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_page);
		//
		startActivity(new Intent(this, ProfessorMainPageActivity.class));
		//GCM으로 부터 registrationIdOfDevice값 받아오기
<<<<<<< HEAD
		/*
		RegistrationIdOfDeviceGetter.getInstance().setOnGCMRegisterIdListner(this);
=======
		RegistrationIdOfDeviceGetter.getInstance().setOnRegistrationIdGetterListner(this);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);

		final String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) {
			GCMRegistrar.register(this, GCMIntentService.SEND_ID);
		} else {
			Log.i(TAG, "Already registered : "+regId);
			registrationIdOfDevice = regId;
		}
		
		Login_id = (EditText) findViewById(R.id.login_id);
		Login_pw = (EditText) findViewById(R.id.login_passwd);
		LoginBtn = (ImageView) findViewById(R.id.login_btn);

<<<<<<< HEAD
			@Override
			public void onClick(View v) {

				LoginAPI loginAPI = new LoginAPIImpl(LoginPageActivity.this);
				loginAPI.login(Login_id.getText().toString(), Login_pw.getText().toString());
			}
		});
		*/
=======
		LoginBtn.setOnTouchListener(this);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				if(v.getId() == R.id.login_btn){
					LoginBtn.setImageResource(R.drawable.login_btn_02_active);
				}
				
				break;
			case MotionEvent.ACTION_UP:
				if(v.getId() == R.id.login_btn){
					LoginBtn.setImageResource(R.drawable.login_btn_01_basic);
					
					if(Login_id.getText().toString() == null || Login_id.getText().toString().replace(" ", "").equals("")){
						Toast.makeText(this, "아이디를 입력해주세요.", Toast.LENGTH_SHORT).show();
						break;
					}
					
					if(Login_pw.getText().toString() == null || Login_pw.getText().toString().replace(" ", "").equals("")){
						Toast.makeText(this, "비밀번호를 입력해주세요.", Toast.LENGTH_SHORT).show();
						break;
					}
					
					LoginAPI loginAPI = new LoginAPIImpl(LoginPageActivity.this);
					loginAPI.login(Login_id.getText().toString(), Login_pw.getText().toString());
					
					loadingProgressDialog = new LoadingProgressDialog(this);
					loadingProgressDialog.show();
				}
				break;
			default:
				break;
		}
		
		return true;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void onSuccessLogin(String userType,String userName) {
		if(registrationIdOfDevice != null){
			RegistrationIdOfDeviceAPI registrationIdOfDeviceAPI = new RegistrationIdOfDeviceAPIImpl();
			registrationIdOfDeviceAPI.setOnCheckRegistrationIdOfDevice(this);
			registrationIdOfDeviceAPI.checkRegistrationIdOfDevice(
					Login_id.getText().toString(), registrationIdOfDevice);
			
			this.userType = userType;
			this.userName = userName;
			
		}
	}
	
	@Override
	public void onFailLogin() {
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "로그인에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onTimeoutToLogin() {
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccessCheckRegistrationIdOfDevice(
			boolean isSameRegistrationIdOfDevice) {
		if (isSameRegistrationIdOfDevice) {
			InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(this);
			internalStorageAPI.setLoginId(Login_id.getText().toString());
			internalStorageAPI.setRegistrationIdOfDevice(registrationIdOfDevice);
			internalStorageAPI.setUserName(userName);
			internalStorageAPI.setUserType(userType);
			internalStorageAPI.setAutoLogin(true);
			
			UserType.getUserType(internalStorageAPI.getUserType()).intentMainPage(this);
			finish();
			loadingProgressDialog.dismiss();
		} else {
			DeviceChangeQuestionDalog deviceChangeQuestionDalog = new DeviceChangeQuestionDalog(this);
			deviceChangeQuestionDalog.setOnDialogListener(this);
			deviceChangeQuestionDalog.show();
		}
	}

	@Override
	public void onTimeoutToCheckRegistrationIdOfDevice() {
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onSuccessUpdateRegistrationIdOfDevice() {
		Toast.makeText(this, "업데이트에 성공하였습니다.", Toast.LENGTH_SHORT).show();
		
		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(this);
		internalStorageAPI.setLoginId(Login_id.getText().toString());
		internalStorageAPI.setRegistrationIdOfDevice(registrationIdOfDevice);
		internalStorageAPI.setUserName(userName);
		internalStorageAPI.setUserType(userType);
		internalStorageAPI.setAutoLogin(true);
		
		UserType.getUserType(internalStorageAPI.getUserType()).intentMainPage(this);
		finish();
		loadingProgressDialog.dismiss();
	}
	
	@Override
	public void onFailUpdateRegistrationIdOfDevice() {
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "업데이트에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToUpdateRegistrationIdOfDevice() {
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onGetRegisterId(String registerId) {
		registrationIdOfDevice = registerId;
		onSuccessLogin(userType,userName);
	}

	@Override
	public void onPositiveDeviceChange() {
		RegistrationIdOfDeviceAPI registrationIdOfDeviceAPI = new RegistrationIdOfDeviceAPIImpl();
		registrationIdOfDeviceAPI.setOnUpdateRegistrationIdOfDevice(this);
		registrationIdOfDeviceAPI.updateRegistrationIdOfDevice(Login_id.getText().toString(), registrationIdOfDevice);
	}

	@Override
	public void onNegativeDeviceChange() {
		
	}
}
