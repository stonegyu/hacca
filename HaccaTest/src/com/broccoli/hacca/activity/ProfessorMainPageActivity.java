package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.broccoli.hacca.R;

public class ProfessorMainPageActivity extends Activity implements OnClickListener, OnTouchListener{
	
	ImageButton professorNoticeBtn, professorJobBtn, professorTelentBtn, professorProfileBtn;
	ImageButton professorNoticeBtnSub, professorJobBtnSub, professorTelentBtnSub, professorProfileBtnSub;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.professor_main_page_layout);
		
		professorNoticeBtn = (ImageButton)findViewById(R.id.professor_notice_btn);
		professorJobBtn = (ImageButton)findViewById(R.id.professor_job_btn);
		professorTelentBtn = (ImageButton)findViewById(R.id.professor_telent_btn);
		professorProfileBtn = (ImageButton)findViewById(R.id.professor_profile_btn);
		
		professorNoticeBtnSub = (ImageButton)findViewById(R.id.professor_notice_direction_btn);
		professorJobBtnSub = (ImageButton)findViewById(R.id.professor_job_direction_btn);
		professorTelentBtnSub = (ImageButton)findViewById(R.id.professor_telent_direction_btn);
		professorProfileBtnSub = (ImageButton)findViewById(R.id.professor_profile_direction_btn);
		
		professorNoticeBtn.setOnClickListener(this);
		professorJobBtn.setOnClickListener(this);
		professorTelentBtn.setOnClickListener(this);
		professorProfileBtn.setOnClickListener(this);
		professorNoticeBtn.setOnTouchListener(this);
		professorJobBtn.setOnTouchListener(this);
		professorTelentBtn.setOnTouchListener(this);
		professorProfileBtn.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ImageButton view = (ImageButton)v;
		if(v.getId() == R.id.professor_notice_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_notice_02_active);
				professorNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_notice_01_basic);
				professorNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_notice_01_basic);
				professorNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.professor_job_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_job_02_active);
				professorJobBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_job_01_basic);
				professorJobBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_job_01_basic);
				professorJobBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.professor_telent_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_telent_02_active);
				professorTelentBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_telent_01_basic);
				professorTelentBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_telent_01_basic);
				professorTelentBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.professor_profile_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_profile_02_active);
				professorProfileBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				professorProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				professorProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		if(v.getId() == R.id.professor_notice_btn){
			startActivity(new Intent(this,NoticeBoardPageActivity.class));
		}
		if(v.getId() == R.id.professor_job_btn){
			startActivity(new Intent(this,JobCategoryActivity.class));
		}
		if(v.getId() == R.id.professor_telent_btn){
			startActivity(new Intent(this,TelentCategoryActivity.class));
		}
		if(v.getId() == R.id.professor_profile_btn){
			startActivity(new Intent(this,ProfessorViewProfilePageActivity.class));
		}
	}
}
