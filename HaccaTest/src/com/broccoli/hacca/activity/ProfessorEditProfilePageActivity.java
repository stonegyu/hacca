package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.broccoli.hacca.R;
<<<<<<< HEAD
import com.broccoli.hacca.dialog.LoadingProgressDialog;
=======
import com.broccoli.hacca.dialog.HansungUnivCertificationDialog;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.dialog.OnHansungUnivCertificationDialogListener;
import com.broccoli.hacca.gcm.GCMIntentService;
import com.broccoli.hacca.hansungunivcertification.HansungUnivCertificator;
import com.broccoli.hacca.hansungunivcertification.HansungUnivCertificatorImpl;
import com.broccoli.hacca.hansungunivcertification.OnHansungUnivCertificatorListener;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
<<<<<<< HEAD
=======
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
import com.broccoli.hacca.passer.searchparser.ProfessorPersonnelInfoParser;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;
<<<<<<< HEAD
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
=======
import com.broccoli.hacca.registrationidofdeviceapi.OnRegisterIdOfDeviceGetterListener;
import com.broccoli.hacca.registrationidofdeviceapi.RegistrationIdOfDeviceGetter;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.OnSearchHansungUnivIdListener;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;
import com.broccoli.hacca.updateapi.OnUpdateAPIListener;
import com.broccoli.hacca.updateapi.UpdateAPI;
import com.broccoli.hacca.updateapi.UpdateAPIImpl;
<<<<<<< HEAD
=======
import com.google.android.gcm.GCMRegistrar;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958

public class ProfessorEditProfilePageActivity extends Activity implements
		OnItemSelectedListener, OnClickListener, OnTouchListener,
		OnSearchAPIListener, OnRegisterAPIListener, OnFocusChangeListener,
<<<<<<< HEAD
		OnUpdateAPIListener {
	
	private EditText professorId, professorPasswd, professorRepasswd, professorName, professorEmail, professorCode;
	
	private Spinner professorLevelSpinner, professorDepartmentSpinner;
	private ArrayAdapter<String> professorLevelAdapter = null;
	private ArrayAdapter<String> professorDepartmentAdapter = null;
	
	private String[] professorLevelList;
	private String[]  professorDepartmentList;
	
	private String professorLevel;
	private String professorDepartment;
	
	private ImageButton professorCommitBtn, professorCertificationBtn;
	private LoadingProgressDialog loadingProgressDialog;
	
	private String professorLoginId;
	private String registrationIdOfDevice;
	
	private Boolean IsProfessorCodeSuccess = false;
	private Boolean IsDupplicationChecked = false;
	private Button professorDuplicationBtn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.professor_edit_profile_page_activity_layout);
		
		professorId = (EditText)findViewById(R.id.professor_id);
		professorPasswd = (EditText)findViewById(R.id.professor_passwd);
		professorRepasswd = (EditText)findViewById(R.id.professor_repasswd);
		professorName = (EditText)findViewById(R.id.professor_name);
		professorEmail = (EditText)findViewById(R.id.professor_email);
		professorCode = (EditText)findViewById(R.id.professor_number);
		
=======
		OnUpdateAPIListener, OnHansungUnivCertificationDialogListener,
		OnHansungUnivCertificatorListener, OnSearchHansungUnivIdListener,
		OnRegisterIdOfDeviceGetterListener {

	private EditText professorId, professorPasswd, professorRepasswd,
			professorName, professorEmail, professorCode;

	private Spinner professorLevelSpinner, professorDepartmentSpinner;
	private ArrayAdapter<String> professorLevelAdapter = null;
	private ArrayAdapter<String> professorDepartmentAdapter = null;

	private String[] professorLevelList;
	private String[] professorDepartmentList;

	private String professorLevel;
	private String professorDepartment;

	private ImageButton professorCommitBtn, professorCertificationBtn;
	private LoadingProgressDialog loadingProgressDialog;

	private String professorLoginId;
	private String registrationIdOfDevice;

	private Boolean IsProfessorCodeSuccess = false;
	private Boolean IsDupplicationChecked = false;
	private Button professorDuplicationBtn;

	private Button certificationBtn;

	private String certificationId;

	private boolean isCertificated = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.professor_edit_profile_page_activity_layout);

		professorId = (EditText) findViewById(R.id.professor_id);
		professorPasswd = (EditText) findViewById(R.id.professor_passwd);
		professorRepasswd = (EditText) findViewById(R.id.professor_repasswd);
		professorName = (EditText) findViewById(R.id.professor_name);
		professorEmail = (EditText) findViewById(R.id.professor_email);
		professorCode = (EditText) findViewById(R.id.professor_number);

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		professorId.setOnFocusChangeListener(this);
		professorPasswd.setOnFocusChangeListener(this);
		professorRepasswd.setOnFocusChangeListener(this);
		professorName.setOnFocusChangeListener(this);
		professorEmail.setOnFocusChangeListener(this);
		professorCode.setOnFocusChangeListener(this);
<<<<<<< HEAD
		
		professorLevelSpinner = (Spinner)findViewById(R.id.professor_level);
		professorDepartmentSpinner = (Spinner)findViewById(R.id.professor_category);
		
		professorLevelList = getResources().getStringArray(R.array.professor_level_list);
		professorDepartmentList = getResources().getStringArray(R.array.departments);
		
=======

		professorLevelSpinner = (Spinner) findViewById(R.id.professor_level);
		professorDepartmentSpinner = (Spinner) findViewById(R.id.professor_category);

		professorLevelList = getResources().getStringArray(
				R.array.professor_level_list);
		professorDepartmentList = getResources().getStringArray(
				R.array.departments);

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		professorLevelAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, professorLevelList);
		professorLevelAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		professorLevelSpinner.setAdapter(professorLevelAdapter);
		professorLevelSpinner.setSelection(0);
		professorLevelSpinner.setSelection(0);
		professorLevelSpinner.setOnItemSelectedListener(this);
<<<<<<< HEAD
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		professorLevelSpinner.setFocusable(true);
		professorLevelSpinner.setFocusableInTouchMode(true);
		professorLevelSpinner.setFocusableInTouchMode(true);
		professorLevelSpinner.setOnFocusChangeListener(this);
<<<<<<< HEAD
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		professorDepartmentAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, professorDepartmentList);
		professorDepartmentAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		professorDepartmentSpinner.setAdapter(professorDepartmentAdapter);
		professorDepartmentSpinner.setSelection(0);
		professorDepartmentSpinner.setOnItemSelectedListener(this);
<<<<<<< HEAD
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		professorDepartmentSpinner.setFocusable(true);
		professorDepartmentSpinner.setFocusableInTouchMode(true);
		professorDepartmentSpinner.setFocusableInTouchMode(true);
		professorDepartmentSpinner.setOnFocusChangeListener(this);
<<<<<<< HEAD
		
		professorCommitBtn = (ImageButton)findViewById(R.id.professor_commit);
		professorCommitBtn.setOnClickListener(this);
		professorCommitBtn.setOnTouchListener(this);
		
		professorCertificationBtn = (ImageButton)findViewById(R.id.professor_certification_btn);
		professorCertificationBtn.setOnClickListener(this);
		professorCertificationBtn.setOnTouchListener(this);
		
		loadingProgressDialog = new LoadingProgressDialog(this);
		
		professorDuplicationBtn = (Button)findViewById(R.id.professorid_duplicationbtn);
		professorDuplicationBtn.setOnClickListener(this);
=======

		professorCommitBtn = (ImageButton) findViewById(R.id.professor_commit);
		professorCommitBtn.setOnClickListener(this);
		professorCommitBtn.setOnTouchListener(this);

		professorCertificationBtn = (ImageButton) findViewById(R.id.professor_certification_btn);
		professorCertificationBtn.setOnClickListener(this);
		professorCertificationBtn.setOnTouchListener(this);

		loadingProgressDialog = new LoadingProgressDialog(this);

		professorDuplicationBtn = (Button) findViewById(R.id.professorid_duplicationbtn);
		professorDuplicationBtn.setOnClickListener(this);

		certificationBtn = (Button) findViewById(R.id.certification_button);
		certificationBtn.setOnClickListener(this);

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);

		professorLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();

<<<<<<< HEAD
		
		if (professorLoginId != null) {
			professorId.setEnabled(false);
			professorDuplicationBtn.setVisibility(View.GONE);
=======
		if (professorLoginId != null) {
			professorId.setEnabled(false);
			professorDuplicationBtn.setVisibility(View.GONE);
			certificationBtn.setVisibility(View.GONE);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
			professorPasswd.setVisibility(View.GONE);
			professorRepasswd.setVisibility(View.GONE);
			professorCode.setEnabled(false);
			professorCertificationBtn.setVisibility(View.GONE);
<<<<<<< HEAD
			
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new ProfessorPersonnelInfoParser());
			searchAPI.searchProfessorInfo(professorLoginId);
			//professor info make
			loadingProgressDialog.show();
		}
		
		
=======

			SearchAPI searchAPI = new SearchAPIImpl(this,
					new ProfessorPersonnelInfoParser());
			searchAPI.searchProfessorInfo(professorLoginId);
			loadingProgressDialog.show();
		}

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		switch (parent.getId()) {
		case R.id.professor_level:
			professorLevel = parent.getItemAtPosition(position).toString();
			break;
		case R.id.professor_category:
			professorDepartment = parent.getItemAtPosition(position).toString();
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void onClick(View v) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		if (v.getId() == R.id.professor_certification_btn) {
			registerAPI.confirmProfessorCode(professorCode
					.getText().toString(),true);
=======
		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		if (v.getId() == R.id.professor_certification_btn) {
			registerAPI.confirmProfessorCode(
					professorCode.getText().toString(), true);

			loadingProgressDialog.show();
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		}
		if (v.getId() == R.id.professor_commit) {

			if (!isFilledInTheBlanks()) {
				return;
			}

			if (!professorPasswd.getText().toString()
					.equals(professorRepasswd.getText().toString())) {
				Toast.makeText(this, "비밀번호와 비밀번호확인이 일치하지 않습니다.",
						Toast.LENGTH_LONG).show();
			}

<<<<<<< HEAD
			// loginId
			// deviceId
			if(professorLoginId == null){
				registerAPI.registerProfessor(getFilledProfessorPersonnelInfo());
			}else{
				UpdateAPI updateAPI = new UpdateAPIImpl(this);
				updateAPI.updateProfessorProfile(getFilledProfessorPersonnelInfo());
			}
			
			

			
		}
		if(v.getId() == R.id.professorid_duplicationbtn){
			if(professorLoginId == null){
				IsDupplicationChecked = false;
				registerAPI.idDuplicationCheck(professorId.getText().toString());
			}
		}
		loadingProgressDialog.show();
	}
	private boolean isFilledInTheBlanks() {
		if (!isFilledInTheBlank(professorId.getText().toString(), "아이디")) {
			return false;
		} else if (professorLoginId == null && !isFilledInTheBlank(professorPasswd.getText().toString(), "비밀번호")) {
			return false;
		} else if (professorLoginId == null && !isFilledInTheBlank(professorRepasswd.getText().toString(),
				"비밀번호확인")) {
			return false;
		} else if (!isFilledInTheBlank(professorLevel,
				"직위")) {
=======
			if (!isCertificated) {
				Toast.makeText(this, "종합정보시스템 인증을 하여주십니오.", Toast.LENGTH_SHORT)
						.show();
				return;
			}

			if (professorLoginId == null) {
				// GCM으로 부터 registrationIdOfDevice값 받아오기
				RegistrationIdOfDeviceGetter.getInstance()
						.setOnRegistrationIdGetterListner(this);

				GCMRegistrar.checkDevice(this);
				GCMRegistrar.checkManifest(this);

				final String regId = GCMRegistrar.getRegistrationId(this);
				if (regId.equals("")) {
					GCMRegistrar.register(this, GCMIntentService.SEND_ID);
				} else {
					registrationIdOfDevice = regId;

					ProfessorPersonnelInfo professorPersonnelInfo = getFilledProfessorPersonnelInfo();
					professorPersonnelInfo
							.setRegistrationIdOfDevice(registrationIdOfDevice);
					professorPersonnelInfo.setHansungUnivId(certificationId);

					registerAPI.registerProfessor(professorPersonnelInfo);
				}

			} else {
				UpdateAPI updateAPI = new UpdateAPIImpl(this);
				updateAPI
						.updateProfessorProfile(getFilledProfessorPersonnelInfo());
			}

			loadingProgressDialog.show();

		}
		if (v.getId() == R.id.professorid_duplicationbtn) {
			if (professorLoginId == null) {
				IsDupplicationChecked = false;
				registerAPI
						.idDuplicationCheck(professorId.getText().toString());
				loadingProgressDialog.show();
			}
		}

		if (v.getId() == R.id.certification_button) {
			HansungUnivCertificationDialog hansungUnivCertificationDialog = new HansungUnivCertificationDialog(
					this);
			hansungUnivCertificationDialog.setOnDialogListener(this);
			hansungUnivCertificationDialog.show();
		}
	}

	private boolean isFilledInTheBlanks() {
		if (!isFilledInTheBlank(professorId.getText().toString(), "아이디")) {
			return false;
		} else if (professorLoginId == null
				&& !isFilledInTheBlank(professorPasswd.getText().toString(),
						"비밀번호")) {
			return false;
		} else if (professorLoginId == null
				&& !isFilledInTheBlank(professorRepasswd.getText().toString(),
						"비밀번호확인")) {
			return false;
		} else if (!isFilledInTheBlank(professorLevel, "직위")) {
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
			return false;
		} else if (!isFilledInTheBlank(professorDepartment, "소속")) {
			return false;
		} else if (!isFilledInTheBlank(professorName.getText().toString(), "이름")) {
			return false;
<<<<<<< HEAD
		} else if (!isFilledInTheBlank(professorEmail.getText().toString(), "이메일")) {
=======
		} else if (!isFilledInTheBlank(professorEmail.getText().toString(),
				"이메일")) {
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
			return false;
		} else if (professorEmail.getText().toString().contains("@") == false) {
			Toast.makeText(getApplicationContext(), "이메일을 정확히 입력해주세요",
					Toast.LENGTH_LONG).show();
			return false;
<<<<<<< HEAD
		}else if(professorLoginId == null && IsProfessorCodeSuccess == false){
			Toast.makeText(getApplicationContext(), "인증번호를 확인해 주세요",
					Toast.LENGTH_LONG).show();
			return false;
		}else if(professorLoginId == null && IsDupplicationChecked == false){
			Toast.makeText(getApplicationContext(),"아이디 중복검사를 해주세요.",
=======
		} else if (professorLoginId == null && IsProfessorCodeSuccess == false) {
			Toast.makeText(getApplicationContext(), "인증번호를 확인해 주세요",
					Toast.LENGTH_LONG).show();
			return false;
		} else if (professorLoginId == null && IsDupplicationChecked == false) {
			Toast.makeText(getApplicationContext(), "아이디 중복검사를 해주세요.",
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
					Toast.LENGTH_LONG).show();
			return false;
		}

		return true;
	}
<<<<<<< HEAD
	
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	private boolean isFilledInTheBlank(String value, String name) {

		if (value.toString().length() == 0 || value.equals("선택")) {
			Toast.makeText(getApplicationContext(), name + "을 입력해주세요.",
					Toast.LENGTH_LONG).show();
			return false;
		} else {
			return true;
		}
	}
<<<<<<< HEAD
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
=======

	@Override
	public boolean onTouch(View v, MotionEvent event) {
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ImageButton view = (ImageButton) v;

		if (view.getId() == R.id.professor_certification_btn) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.certification_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.certification_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.certification_btn_01_basic);
			}
		} else if (view.getId() == R.id.professor_commit) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.save_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
		}
		return false;
	}
<<<<<<< HEAD
	
	private ProfessorPersonnelInfo getFilledProfessorPersonnelInfo() {

		ProfessorPersonnelInfo professorPersonnelInfo = new ProfessorPersonnelInfo();
		
		professorPersonnelInfo.setProfessorId(professorId.getText().toString());
		professorPersonnelInfo.setProfessorPasswd(professorPasswd.getText().toString());
		professorPersonnelInfo.setProfessorName(professorName.getText().toString());
		professorPersonnelInfo.setProfessorEmail(professorEmail.getText().toString());
		professorPersonnelInfo.setProfessorCode(professorCode.getText().toString());
		professorPersonnelInfo.setProfessorLevel(professorLevel);
		professorPersonnelInfo.setProfessorDepartment(professorDepartment);
		professorPersonnelInfo.setRegistrationIdOfDevice(registrationIdOfDevice);
=======

	private ProfessorPersonnelInfo getFilledProfessorPersonnelInfo() {

		ProfessorPersonnelInfo professorPersonnelInfo = new ProfessorPersonnelInfo();

		professorPersonnelInfo.setProfessorId(professorId.getText().toString());
		professorPersonnelInfo.setProfessorPasswd(professorPasswd.getText()
				.toString());
		professorPersonnelInfo.setProfessorName(professorName.getText()
				.toString());
		professorPersonnelInfo.setProfessorEmail(professorEmail.getText()
				.toString());
		professorPersonnelInfo.setProfessorCode(professorCode.getText()
				.toString());
		professorPersonnelInfo.setProfessorLevel(professorLevel);
		professorPersonnelInfo.setProfessorDepartment(professorDepartment);
		professorPersonnelInfo
				.setRegistrationIdOfDevice(registrationIdOfDevice);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		return professorPersonnelInfo;
	}

	@Override
	public void onSuccessRegister() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		loadingProgressDialog.dismiss();
		if (!IsProfessorCodeSuccess) {
			IsProfessorCodeSuccess = true;
			Toast.makeText(getApplicationContext(), "인증번호가 등록 되었습니다.",
					Toast.LENGTH_SHORT).show();
			professorCertificationBtn.setEnabled(false);
			professorCode.setEnabled(false);
			return;
		}
		Toast.makeText(getApplicationContext(), "등록에 성공하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailRegister() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		if (!IsProfessorCodeSuccess) {
			IsProfessorCodeSuccess=false;
=======
		if (!IsProfessorCodeSuccess) {
			IsProfessorCodeSuccess = false;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
			Toast.makeText(getApplicationContext(), "인증번호가 중복됩니다. 다시 입력 해주세요.",
					Toast.LENGTH_SHORT).show();
			return;
		}
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "등록에 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		
	}
	
=======
		loadingProgressDialog.dismiss();
		Toast.makeText(this, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
	}

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	private void setProfessorInfo(ProfessorPersonnelInfo professorPersonnelInfo) {
		professorId.setText(professorPersonnelInfo.getProfessorId());
		professorPasswd.setText(professorPersonnelInfo.getProfessorPasswd());
		professorName.setText(professorPersonnelInfo.getProfessorName());
		professorEmail.setText(professorPersonnelInfo.getProfessorEmail());
		professorCode.setText(professorPersonnelInfo.getProfessorCode());
		professorLevel = professorPersonnelInfo.getProfessorLevel();
		professorDepartment = professorPersonnelInfo.getProfessorDepartment();
<<<<<<< HEAD
		professorLevelSpinner.setSelection(getSpinnerNumber(professorLevelList, professorLevel));
		professorDepartmentSpinner.setSelection(getSpinnerNumber(professorDepartmentList, professorDepartment));
		
=======
		professorLevelSpinner.setSelection(getSpinnerNumber(professorLevelList,
				professorLevel));
		professorDepartmentSpinner.setSelection(getSpinnerNumber(
				professorDepartmentList, professorDepartment));

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	private int getSpinnerNumber(String[] list, String value) {
		int number = 0;

		value = value.replace(" ", "");

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(value)) {
				number = i;
			}
		}
		return number;
	}
<<<<<<< HEAD
	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		// TODO Auto-generated method stub
=======

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ProfessorPersonnelInfo professorPersonnelInfo = pageInfo
				.getProfessorPersonnelInfo();
		setProfessorInfo(professorPersonnelInfo);

		loadingProgressDialog.dismiss();
	}

	@Override
	public void onFailSearch() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSearch() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		if (v.getId() == R.id.professor_level
				|| v.getId() == R.id.professor_category) {
			Spinner view = (Spinner) v;
			if (hasFocus) {
				view.setBackgroundResource(R.drawable.textbox_large_02_active);
			} else {
				view.setBackgroundResource(R.drawable.textbox_large_01_basic);
			}
			return;
		}

		EditText view = (EditText) v;
		if (hasFocus) {
			view.setBackgroundResource(R.drawable.textbox_large_02_active);
		} else {
			view.setBackgroundResource(R.drawable.textbox_large_01_basic);
		}
		if (v.getId() == R.id.professor_id || v.getId() == R.id.professor_name
				|| v.getId() == R.id.professor_passwd
				|| v.getId() == R.id.professor_repasswd) {
			if (hasFocus) {
				view.setBackgroundResource(R.drawable.textbox_small_02_active);
			} else {
				view.setBackgroundResource(R.drawable.textbox_small_01_basic);
			}
		}
	}

	@Override
	public void onSuccessUpdate() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보를 성공적으로 업데이트 하였습니다.",
				Toast.LENGTH_SHORT).show();
		startActivity(new Intent(this, ProfessorViewProfilePageActivity.class));
	}

	@Override
	public void onFailUpdate() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보 업데이트를 실패 하였습니다. 다시 시도해주세요",
				Toast.LENGTH_SHORT).show();
=======
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(),
				"개인정보 업데이트를 실패 하였습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void onTimeoutToUpdate() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=true;
=======
		loadingProgressDialog.dismiss();
		IsDupplicationChecked = true;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Toast.makeText(getApplicationContext(), "중복되지 않는 아이디입니다. 사용하실수 있습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDupplication() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=false;
=======
		loadingProgressDialog.dismiss();
		IsDupplicationChecked = false;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Toast.makeText(getApplicationContext(), "중복되는 아이디입니다. 다른 아이디를 사용하세요.",
				Toast.LENGTH_SHORT).show();
	}

<<<<<<< HEAD
=======
	@Override
	public void onCompletedHansungUnivCertificationDialogSetting(
			String certificationId, String certificationPassword) {
		if (certificationId.length() != 6) {
			loadingProgressDialog.dismiss();
			Toast.makeText(this, "교수 학번이 아닙니다.", Toast.LENGTH_SHORT).show();
			return;
		}

		this.certificationId = certificationId;
		HansungUnivCertificator univCertificator = new HansungUnivCertificatorImpl(
				this);
		univCertificator.certificate(certificationId, certificationPassword);

		loadingProgressDialog.show();
	}

	@Override
	public void onSuccessToCertificateHansungUniv() {
		SearchAPI searchAPI = new SearchAPIImpl(null, null);
		searchAPI.setOnSearchHansungUnivIdListener(this);
		searchAPI.searchHansungUnivId(certificationId);
	}

	@Override
	public void onCompletedSearchHansungUnivId(boolean isDuplicated) {
		loadingProgressDialog.dismiss();

		if (isDuplicated) {
			Toast.makeText(this, "이미 회원가입된 아이디입니다.", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, "인증 완료하였습니다.", Toast.LENGTH_SHORT).show();
			isCertificated = true;
		}
	}

	@Override
	public void onFailToCertificateHansungUniv() {
		loadingProgressDialog.dismiss();

		Toast.makeText(this, "인증에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onGetRegisterId(String registerId) {
		registrationIdOfDevice = registerId;

		ProfessorPersonnelInfo professorPersonnelInfo = getFilledProfessorPersonnelInfo();
		professorPersonnelInfo
				.setRegistrationIdOfDevice(registrationIdOfDevice);
		professorPersonnelInfo.setHansungUnivId(certificationId);

		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		registerAPI.registerProfessor(professorPersonnelInfo);
	}
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
}
