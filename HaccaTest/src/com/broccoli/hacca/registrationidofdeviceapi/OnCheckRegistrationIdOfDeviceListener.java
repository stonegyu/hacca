package com.broccoli.hacca.registrationidofdeviceapi;

public interface OnCheckRegistrationIdOfDeviceListener {
	void onSuccessCheckRegistrationIdOfDevice(boolean isSameRegistrationIdOfDevice);
	
	void onTimeoutToCheckRegistrationIdOfDevice();
}
