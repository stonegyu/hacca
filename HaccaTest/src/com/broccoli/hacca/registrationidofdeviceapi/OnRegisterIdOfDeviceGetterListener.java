package com.broccoli.hacca.registrationidofdeviceapi;

public interface OnRegisterIdOfDeviceGetterListener {
	void onGetRegisterId(String registerId);
}
