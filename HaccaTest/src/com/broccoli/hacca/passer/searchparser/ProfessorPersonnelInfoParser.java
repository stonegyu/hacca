package com.broccoli.hacca.passer.searchparser;

import android.util.Log;

import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.passer.JsonParser;
import com.broccoli.hacca.passer.Parser;

public class ProfessorPersonnelInfoParser implements ParsingInfoType{
	
	private final String TAG="ProfessorPersonnelInfoParser";
	
	@Override
	public PageInfo parseInfo(String parsingValue) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		Parser parser = new Parser(new JsonParser(parsingValue));
		
		ProfessorPersonnelInfo professorPersonnelInfo = new ProfessorPersonnelInfo();
		professorPersonnelInfo.setProfessorId(getParseInfo(parser, "professorLoginId"));
		professorPersonnelInfo.setProfessorName(getParseInfo(parser, "professorName"));
		professorPersonnelInfo.setProfessorDepartment(getParseInfo(parser, "professorDepartment"));
		professorPersonnelInfo.setProfessorEmail(getParseInfo(parser, "professorMail"));
		professorPersonnelInfo.setProfessorCode(getParseInfo(parser, "professorCode"));
		professorPersonnelInfo.setProfessorLevel(getParseInfo(parser, "professorLevel"));
		PageInfo pageInfo = new PageInfo();
		pageInfo.setProfessorPersonnelInfo(professorPersonnelInfo);
		return pageInfo;
	}

	private String getParseInfo(Parser parser,String parsingKey){
		String result;
		
		try{
			result = parser.parse(parsingKey).get(0);
		}catch(IndexOutOfBoundsException e){
			result="";
		}
		
		return result;
	}
}
