package com.broccoli.hacca.searchapi;

public interface OnSearchHansungUnivIdListener {
	void onCompletedSearchHansungUnivId(boolean isDuplicated);
}
