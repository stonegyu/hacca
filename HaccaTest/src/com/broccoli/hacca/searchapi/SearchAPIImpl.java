package com.broccoli.hacca.searchapi;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.util.Log;

import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.ParsingInfoType;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class SearchAPIImpl implements SearchAPI,OnDataBaseConnectAPIListener {

	private final String TAG="SearchAPIImpl";
	
	private ParsingInfoType parsingInfo;
	private OnSearchAPIListener searchAPIListener;
	
	
	private boolean isSearchUserType= false;

	private OnSearchUserTypeListener onSearchUserTypeListener;

	private OnSearchHansungUnivIdListener onSearchHansungUnivIdListener;

	private boolean isSearchHansungUnivId = false;

	public SearchAPIImpl(OnSearchAPIListener searchAPIListener,ParsingInfoType parsingInfoType){
		this.searchAPIListener = searchAPIListener;
		this.parsingInfo = parsingInfoType;
	}
	
	@Override
	public void searchStudentInfo(String studentLoginId) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.STUDENT_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.STUDENT_LOGINID, studentLoginId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void searchStudentsInfo(String studentDepartment,String searchText,String limit) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.STUDENT_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.STUDENT_DEPARTMENT, studentDepartment);
		urlDataStorage.setParameter(UrlParameterType.SEARCH_TEXT, searchText);
		urlDataStorage.setParameter(UrlParameterType.LIMIT, limit);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void searchCompanyInfo(String companyLoginId) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.COMPANY_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.COMPANY_LOGINID, companyLoginId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void searchCompanysInfo(String companyBusinessType,String searchText, String limit) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.COMPANY_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.COMPANY_BUSINESSTYPE, companyBusinessType);
		urlDataStorage.setParameter(UrlParameterType.SEARCH_TEXT, searchText);
		urlDataStorage.setParameter(UrlParameterType.LIMIT, limit);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	
	@Override
	public void searchProfessorComment(String studentLoginId) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.PROFESSOR_COMMENT_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.STUDENT_LOGINID, studentLoginId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}

	@Override
	public void searchNoticeBoard(String category, String searchText,
			String limit) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.NOTICEBOARDINFO_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.NOTICEBOARD_CATEGORY, category);
		urlDataStorage.setParameter(UrlParameterType.SEARCH_TEXT, searchText);
		urlDataStorage.setParameter(UrlParameterType.LIMIT, limit);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void searchUserType(String loginId) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.USERTYPE_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, loginId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
		
		isSearchUserType = true;
	}
	
	@Override
	public void searchHansungUnivId(String hansungUnivId) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.HANSUNGUNIV_ID_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.HANSUNGUNIV_ID, hansungUnivId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
		
		isSearchHansungUnivId  = true;
	}

	@Override
	public void setOnSearchHansungUnivIdListener(
			OnSearchHansungUnivIdListener onSearchHansungUnivIdListener) {
				this.onSearchHansungUnivIdListener = onSearchHansungUnivIdListener;
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		Log.i(TAG, result+"");
		
		if(isSearchUserType && onSearchUserTypeListener != null){
			isSearchUserType = false;
			//맨앞에 ASCII CODE 65279가 붙음
			result = result.replace(String.valueOf((char)65279), "" );
			
			UserType userType = UserType.getUserType(result);
			
			onSearchUserTypeListener.onGetUserType(userType);
			
			return;
		}
		
		if(isSearchHansungUnivId && onSearchHansungUnivIdListener != null){
			isSearchHansungUnivId = false;
			//맨앞에 ASCII CODE 65279가 붙음
			result = result.replace(String.valueOf((char)65279), "" );
			
			onSearchHansungUnivIdListener.onCompletedSearchHansungUnivId(Boolean.valueOf(result));
			
			return;
		}
		
		PageInfo pageInfo = parsingInfo.parseInfo(result);
		searchAPIListener.onSuccessSearch(pageInfo);
	}

	@Override
	public void onFailToConnectDataBase() {
		searchAPIListener.onFailSearch();
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		searchAPIListener.onTimeoutToSearch();
	}

	@Override
	public void searchProfessorInfo(String professorLoginId) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.PROFESSOR_SEARCH);
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, professorLoginId);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
<<<<<<< HEAD
		Log.d("llog",url);
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
=======
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}

	@Override
	public void setOnSearchUserTypeListener(
			OnSearchUserTypeListener onSearchUserTypeListener) {
				this.onSearchUserTypeListener = onSearchUserTypeListener;
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}
}
