package com.broccoli.hacca.searchapi;

import com.broccoli.hacca.activity.UserType;

public interface OnSearchUserTypeListener {
	void onGetUserType(UserType userType);
}
