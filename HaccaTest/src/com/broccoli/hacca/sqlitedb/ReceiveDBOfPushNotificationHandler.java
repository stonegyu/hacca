package com.broccoli.hacca.sqlitedb;

import com.broccoli.hacca.pageinfo.PushNotificationInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ReceiveDBOfPushNotificationHandler {
	private ReceiveDBOfPushNotification handler;
	private SQLiteDatabase db;

	public ReceiveDBOfPushNotificationHandler(Context cnt) {
		this.handler = new ReceiveDBOfPushNotification(cnt, "hacca_receiveDBOfPushNotification.db", null, 1);
		this.db = handler.getWritableDatabase();
	}
	
	public static ReceiveDBOfPushNotificationHandler open(Context ctx) throws SQLException {
		ReceiveDBOfPushNotificationHandler handler = new ReceiveDBOfPushNotificationHandler(ctx);

		return handler;
	}

	public void close() {
		handler.close();
	}

	public long insert(PushNotificationInfo pushNotificationInfo) {
		ContentValues values = new ContentValues();
		values.put("senderLoginId", pushNotificationInfo.getSenderLoginId());
		values.put("senderName", pushNotificationInfo.getSenderName());
		values.put("title", pushNotificationInfo.getTitle());
		values.put("message", pushNotificationInfo.getMessage());
		values.put("date", pushNotificationInfo.getDate());
		values.put("isOpen", false);

		return db.insert("hacca_receiveDBOfPushNotification", null, values);
	}
	
	public long delete(String number){
		
		return db.delete("hacca_receiveDBOfPushNotification", "number="+number, null);
	}

	public long updateIsOpen(String number){
		ContentValues values = new ContentValues();
		values.put("isOpen", true);
		
		return db.update("hacca_receiveDBOfPushNotification", values, "number="+number, null);
	}

	public Cursor select(int start, int end) throws SQLException {
		Cursor cursor = db.rawQuery(
				"select * from hacca_receiveDBOfPushNotification order by number desc limit "+start+","+end, null);

		if (cursor != null)
			cursor.moveToFirst();

		return cursor;
	}
	
	public int getCountOfReceive(){
		Cursor cursor = db.rawQuery(
				"select * from hacca_receiveDBOfPushNotification where isOpen = 0", null);
		if (cursor != null)
			cursor.moveToFirst();
		
		return cursor.getCount();
	}
}
