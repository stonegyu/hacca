package com.broccoli.hacca.sqlitedb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class SendDBOfPushNotification extends SQLiteOpenHelper {

	public SendDBOfPushNotification(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String table = "create table hacca_sendDBOfPushNotification("
				+ "number integer primary key autoincrement,"
				+ "receiverLoginId text," + "receiverName text," + "title text,"
				+ "message text," + "date text," + "isOpen integer" + ");";
		db.execSQL(table);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exist hacca_sendDBOfPushNotification");
		onCreate(db);

	}
}
