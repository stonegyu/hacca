package com.broccoli.hacca.dataconnectapi;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

public interface DataBaseConnectAPI {
	void connectDataBase(String url,ArrayList<NameValuePair> nameValuePairs);
}
