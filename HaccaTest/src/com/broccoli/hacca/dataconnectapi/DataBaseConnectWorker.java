package com.broccoli.hacca.dataconnectapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;
import android.util.Log;

public class DataBaseConnectWorker extends
		AsyncTask<ArrayList<NameValuePair>, Void, String> {
	private final String TAG = "DataBaseConnectWorker";
	private OnDataBaseConnectWorkerListner workerListner;
	private java.lang.String url;

	public DataBaseConnectWorker(OnDataBaseConnectWorkerListner workerListner,
			String url) {
		this.workerListner = workerListner;
		this.url = url.replace(" ", "");
	}

	@Override
	protected String doInBackground(ArrayList<NameValuePair>... nameValuePairs) {
		String result = "";

		ArrayList<NameValuePair> valuePairs = nameValuePairs[0];

		DefaultHttpClient client = new DefaultHttpClient();

		try {
<<<<<<< HEAD
			String url = urls[0].replace(" ", "" );
			
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
			HttpPost post = new HttpPost(url);

			HttpParams httpParams = client.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 3000);
			HttpConnectionParams.setSoTimeout(httpParams, 3000);

			if (valuePairs != null) {
				UrlEncodedFormEntity entityRequest = new UrlEncodedFormEntity(
						valuePairs, "utf-8");

				post.setEntity(entityRequest);
			}

			HttpResponse response = client.execute(post);
			BufferedReader bufreader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent(),
							"utf-8"));

			String line;
			while ((line = bufreader.readLine()) != null) {
				result += line;
			}

		} catch (ConnectTimeoutException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		workerListner.onCompletedDataBaseConnect(result);
	}

}
