package com.broccoli.hacca.dataconnectapi;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.util.Log;


public class DataBaseConnectAPIImpl implements DataBaseConnectAPI,OnDataBaseConnectWorkerListner{
	private final String TAG = "DataBaseConnectAPIImpl";
	
	private OnDataBaseConnectAPIListener dataBaseConnectAPIListener;

	public DataBaseConnectAPIImpl(OnDataBaseConnectAPIListener dataBaseConnectAPIListener){
		this.dataBaseConnectAPIListener = dataBaseConnectAPIListener;
	}
	
	@Override
	public void connectDataBase(String url, ArrayList<NameValuePair> nameValuePairs) {
		
		Log.i(TAG, url+"");
		
		DataBaseConnectWorker worker = new DataBaseConnectWorker(this,url);
		worker.execute(nameValuePairs);
	}

	@Override
	public void onCompletedDataBaseConnect(String result) {
		
		Log.i(TAG, result+"");
		
		//맨앞에 ASCII CODE 65279가 붙음
		result = result.replace(String.valueOf((char)65279), "" );
				
		if(result.equals("null")){
			Log.i(TAG, result+" null");
			dataBaseConnectAPIListener.onFailToConnectDataBase();
		}else{
			Log.i(TAG, result+" exist");
			dataBaseConnectAPIListener.onSuccessToConnectDataBase(result);
		}
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		dataBaseConnectAPIListener.onDataBaseConnectTimeOut();
	}
}
