package com.broccoli.hacca.deleteapi;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class DeleteAPIImpl implements DeleteAPI, OnDataBaseConnectAPIListener {

	private OnDeleteAPIListener onDeleteAPIListener;

	public DeleteAPIImpl(OnDeleteAPIListener onDeleteAPIListener) {
		this.onDeleteAPIListener = onDeleteAPIListener;
	}

	@Override
	public void deleteNoticeBoard(String professorLoginId, String number) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.NOTICEBOARD_DELETE);
		urlDataStorage
				.setParameter(UrlParameterType.NOTICEBOARD_NUMBER, number);
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, professorLoginId);

		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		// 맨앞에 ASCII CODE 65279가 붙음
		result = result.replace(String.valueOf((char) 65279), "");

		if (result.equals("true")) {
			onDeleteAPIListener.onCompletedDelete();
		} else if (result.equals("false")) {
			
		}
	}

	@Override
	public void onFailToConnectDataBase() {

	}

	@Override
	public void onDataBaseConnectTimeOut() {
		onDeleteAPIListener.onTimeoutToDelete();
	}

}
