package com.broccoli.hacca.deleteapi;

public interface DeleteAPI {
	void deleteNoticeBoard(String professorLoginId, String number);
}
