package com.broccoli.hacca.adapter.company;

import android.util.DisplayMetrics;
import android.view.animation.Animation;

import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;

public class CompanyAdapterItem {
	private ChildViewAdapterSettable childViewAdapterSettable;
	private CompanyPersonnelInfo companyPersonnelInfo;

	public CompanyPersonnelInfo getCompanyPersonnelInfo() {
		return companyPersonnelInfo;
	}

	public CompanyAdapterItem(CompanyPersonnelInfo companyPersonnelInfo) {
		this.companyPersonnelInfo = companyPersonnelInfo;
	}

	public ChildViewAdapterSettable getChildViewAdapterSettable() {
		return childViewAdapterSettable;
	}

	public void setChildViewAdapterSettable(
			ChildViewAdapterSettable childViewAdapterSettable) {
		this.childViewAdapterSettable = childViewAdapterSettable;
	}
}
