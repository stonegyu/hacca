package com.broccoli.hacca.adapter.company;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.adapter.OnAdapterSetterListener;

public class CompanyListPageAdapter extends BaseExpandableListAdapter implements
		OnAdapterSetterListener {
	
	private final String TAG = "CompanyListPageAdapter";
	private LayoutInflater layoutInflater;

	private ArrayList<CompanyAdapterItem> adapterItems;

	private int parentViewLayout;
	private int childViewLayout;

	private Context context;

	public CompanyListPageAdapter(Context context,
			ArrayList<CompanyAdapterItem> adapterItems) {
		this.context = context;
		layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.adapterItems = adapterItems;
	}

	public void setGroupViewLayout(int groupViewLayout) {
		this.parentViewLayout = groupViewLayout;
	}

	public void setChildViewLayout(int childViewLayout) {
		this.childViewLayout = childViewLayout;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return adapterItems.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		if (adapterItems.get(groupPosition).getChildViewAdapterSettable() == null) {
			adapterItems.get(groupPosition).setChildViewAdapterSettable(
					new CompanyAdapterChildViewSetter(context, this));
			
			adapterItems
					.get(groupPosition)
					.getChildViewAdapterSettable()
					.setView(
							layoutInflater.inflate(childViewLayout, parent,
									false), adapterItems.get(groupPosition));
		}
		
		convertView = adapterItems.get(groupPosition).getChildViewAdapterSettable().getView();
		adapterItems.get(groupPosition).getChildViewAdapterSettable().startAnimation();

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return adapterItems.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return adapterItems.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = layoutInflater.inflate(parentViewLayout, parent,
					false);
		}

		TextView name = (TextView) convertView
				.findViewById(R.id.index_entityname);
		name.setText(adapterItems.get(groupPosition).getCompanyPersonnelInfo().getCompanyName());

		TextView recruitmentArea = (TextView) convertView
				.findViewById(R.id.index_company_Businesstype);
		recruitmentArea.setText(adapterItems.get(groupPosition).getCompanyPersonnelInfo().getCompanyBusinessType());

<<<<<<< HEAD
=======
		String deadline = adapterItems.get(groupPosition).getCompanyPersonnelInfo().getComapanyRecruitmentPeriod();
		
		try{
			deadline = deadline.split("~")[1].replace(" ", "");
		} catch(Exception e){
			
		}
		
		TextView recruitPeriod = (TextView) convertView.findViewById(R.id.company_deadline);
		recruitPeriod.setText("(~"+deadline+")");
		
		boolean isDeadLine = false,isDeadLineImminent=false;
		
		try{
			long now = System.currentTimeMillis();
			Date currentDate = new Date(now);
			
			SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyy.MM.dd", java.util.Locale.getDefault());
			
			Date deadLineDate = dateFormat.parse(deadline);
			Date deadLineDateOfImminent = dateFormat.parse(deadline);
			
			deadLineDate.setDate(deadLineDate.getDate()+1);
			deadLineDateOfImminent.setDate(deadLineDateOfImminent.getDate()-7);
			
			isDeadLine = currentDate.after(deadLineDate);
			isDeadLineImminent = currentDate.after(deadLineDateOfImminent);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		ImageView deadLineImminentImage = (ImageView) convertView.findViewById(R.id.company_deadline_imminent_image);
		
		if(!isDeadLine && isDeadLineImminent){
			deadLineImminentImage.setImageResource(R.drawable.company_deadline_imminent);
		}else {
			deadLineImminentImage.setImageBitmap(null);
		}
		
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		ImageView indicator = (ImageView) convertView
				.findViewById(R.id.groupindicater);

		if (isExpanded) {
			indicator.setImageResource(R.drawable.expand_btn_02_basic);
		} else {
			indicator.setImageResource(R.drawable.expand_btn_01_basic);
			
			if(adapterItems.get(groupPosition).getChildViewAdapterSettable() != null){
				adapterItems.get(groupPosition).getChildViewAdapterSettable().resetAnimation();
			}
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public void onFailSettings() {

	}

	@Override
	public void onCompletedSettings() {
		this.notifyDataSetChanged();
	}
}
