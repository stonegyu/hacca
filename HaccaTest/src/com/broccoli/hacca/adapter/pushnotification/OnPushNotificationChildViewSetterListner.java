package com.broccoli.hacca.adapter.pushnotification;

public interface OnPushNotificationChildViewSetterListner {
	void onDeletePushNotification(int position);
	void onCompletedSetting(int position);
}
