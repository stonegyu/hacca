package com.broccoli.hacca.adapter.pushnotification;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.pageinfo.PushNotificationInfo;
import com.broccoli.hacca.sqlitedb.ReceiveDBOfPushNotificationHandler;
import com.broccoli.hacca.sqlitedb.SendDBOfPushNotificationHandler;

public class PushNotificationListView implements OnScrollListener,OnPushNotificationListViewApaterChangedListener{
	
	private final String TAG = "PushNotificationListView";
	
	private View pagerItemView;
	private View footerView;
	private ExpandableListView expandableListView;
	
	private PushNotificationListViewAdapter adapter;
	private ArrayList<PushNotificationAdapterItem> pushNotificationAdapterItems;
	
	private String category;
	
	private boolean isSearchingData = false;
	private Context context;
	
	public PushNotificationListView(Context context,String category) {
		this.context = context;
		this.category = category;
		
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		pagerItemView = layoutInflater.inflate(R.layout.noticeboard_listview_layout, null);
		
		expandableListView = (ExpandableListView) pagerItemView.findViewById(R.id.expandableListView);
		
		footerView = layoutInflater.inflate(R.layout.listviewfooter, null);
		
		pushNotificationAdapterItems = new ArrayList<PushNotificationAdapterItem>();
		
		adapter = new PushNotificationListViewAdapter(context,pushNotificationAdapterItems,category);
		adapter.setOnPushNotificationListViewAdapterChangedListner(this);
		adapter.setGroupViewLayout(R.layout.pushnotification_list_page_adapter_parent_layout);
		adapter.setChildViewLayout(R.layout.pushnotification_list_page_adapter_child_layout);
		
		expandableListView.setAdapter(adapter);

		expandableListView.setDivider(null);
		expandableListView.setOnScrollListener(this);
	}

	public View getView(){
		return pagerItemView;
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (firstVisibleItem + visibleItemCount >= totalItemCount
				&& !isSearchingData) {
			
			expandableListView.addFooterView(footerView);
			
			isSearchingData = true;
			
			Cursor cursor = null;
			
			if(category.equals("receive")){
				ReceiveDBOfPushNotificationHandler receiveDBOfPushNotificationHandler = new ReceiveDBOfPushNotificationHandler(context);
				cursor = receiveDBOfPushNotificationHandler.select(pushNotificationAdapterItems.size(), pushNotificationAdapterItems.size()+10);
			}else if(category.equals("send")){
				SendDBOfPushNotificationHandler sendDBOfPushNotificationHandler = new SendDBOfPushNotificationHandler(context);
				cursor = sendDBOfPushNotificationHandler.select(pushNotificationAdapterItems.size(), pushNotificationAdapterItems.size()+10);
			}
			
			if(cursor.getCount() == 0){
				expandableListView.removeFooterView(footerView);
				return;
			}
			
			for (int i = 0; i < cursor.getCount(); i++) {
				PushNotificationInfo pushNotificationInfo = new PushNotificationInfo();
				
				if(category.equals("receive")){
					pushNotificationInfo.setNumber(cursor.getInt(0));
					pushNotificationInfo.setSenderLoginId(cursor.getString(1));
					pushNotificationInfo.setSenderName(cursor.getString(2));
					pushNotificationInfo.setTitle(cursor.getString(3));
					pushNotificationInfo.setMessage(cursor.getString(4));
					pushNotificationInfo.setDate(cursor.getString(5));
					pushNotificationInfo.setIsOpen(cursor.getInt(6));
				}else if(category.equals("send")){
					Log.i(TAG, cursor.getInt(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
					pushNotificationInfo.setNumber(cursor.getInt(0));
					pushNotificationInfo.setReceiverLoginId(cursor.getString(1));
					pushNotificationInfo.setReceiverName(cursor.getString(2));
					pushNotificationInfo.setTitle(cursor.getString(3));
					pushNotificationInfo.setMessage(cursor.getString(4));
					pushNotificationInfo.setDate(cursor.getString(5));
					pushNotificationInfo.setIsOpen(cursor.getInt(6));
				}
				
				PushNotificationAdapterItem pushNotificationAdapterItem = new PushNotificationAdapterItem();
				pushNotificationAdapterItem.setPushNotificationInfo(pushNotificationInfo);
				
				pushNotificationAdapterItems.add(pushNotificationAdapterItem);
				
				cursor.moveToNext();
			}
			
			isSearchingData = false;
			expandableListView.removeFooterView(footerView);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}
	
	@Override
	public void onDeletePushNotification(int position) {
		int number = pushNotificationAdapterItems.get(position).getPushNotificationInfo().getNumber();
		
		if(category.equals("receive")){
			ReceiveDBOfPushNotificationHandler receiveDBOfPushNotificationHandler = new ReceiveDBOfPushNotificationHandler(context);
			receiveDBOfPushNotificationHandler.delete(number+"");
		}else if(category.equals("send")){
			SendDBOfPushNotificationHandler sendDBOfPushNotificationHandler = new SendDBOfPushNotificationHandler(context);
			sendDBOfPushNotificationHandler.delete(number+"");
		}
		
		pushNotificationAdapterItems.remove(position);
		
		for(int i=0;i<adapter.getGroupCount();i++){
			expandableListView.collapseGroup(i);
		}
		
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onUpdateIsOpen(int position) {
		pushNotificationAdapterItems.get(position).getPushNotificationInfo().setIsOpen(1);
		adapter.notifyDataSetChanged();
	}
}
