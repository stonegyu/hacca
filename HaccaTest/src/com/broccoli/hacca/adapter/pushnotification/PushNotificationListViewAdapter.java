package com.broccoli.hacca.adapter.pushnotification;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.OnAdapterSetterListener;
import com.broccoli.hacca.adapter.student.StudentAdapterChildViewSetter;

public class PushNotificationListViewAdapter extends BaseExpandableListAdapter
		implements OnPushNotificationChildViewSetterListner {

	private final String TAG = "PushNotificationListViewAdapter";

	private LayoutInflater layoutInflater;

	private ArrayList<PushNotificationAdapterItem> adapterItems;

	private int parentViewLayout;
	private int childViewLayout;

	private Context context;

	private OnPushNotificationListViewApaterChangedListener onPushNotificationListViewApaterChangedListener;

	private String category;

	public PushNotificationListViewAdapter(Context context,
			ArrayList<PushNotificationAdapterItem> adapterItems,String category) {
		this.context = context;
		this.category = category;
		layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.adapterItems = adapterItems;
	}
	
	public void setOnPushNotificationListViewAdapterChangedListner(OnPushNotificationListViewApaterChangedListener onPushNotificationListViewApaterChangedListener){
		this.onPushNotificationListViewApaterChangedListener = onPushNotificationListViewApaterChangedListener;
	}

	public void setGroupViewLayout(int groupViewLayout) {
		this.parentViewLayout = groupViewLayout;
	}

	public void setChildViewLayout(int childViewLayout) {
		this.childViewLayout = childViewLayout;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return adapterItems.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		if(adapterItems.get(groupPosition).getChildViewAdapterSettable() != null && adapterItems.get(groupPosition).getChildViewAdapterSettable()
				.isCompletedSetting()){
			convertView = adapterItems.get(groupPosition).getChildViewAdapterSettable().getView();
			adapterItems.get(groupPosition).getChildViewAdapterSettable().startAnimation();
		}else{
			convertView = layoutInflater.inflate(R.layout.listviewfooter, parent, false);
		}
		
		if (adapterItems.get(groupPosition).getChildViewAdapterSettable() == null) {
			adapterItems.get(groupPosition).setChildViewAdapterSettable(
					new PushNotificationChildViewSetter(context, groupPosition,this,category));
			
			adapterItems
					.get(groupPosition)
					.getChildViewAdapterSettable()
					.setView(
							layoutInflater.inflate(childViewLayout, parent,
									false), adapterItems.get(groupPosition));
		}
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return adapterItems.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return adapterItems.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = layoutInflater.inflate(parentViewLayout, parent,
					false);
		}

		if(category.equals("receive")){
			setReceiveMessage(convertView, groupPosition);
		}else if(category.equals("send")){
			setSendMessage(convertView, groupPosition);
		}
		
		ImageView pushNotificationIsOpen = (ImageView)convertView.findViewById(R.id.notification_open);
		
		if(adapterItems.get(groupPosition).getPushNotificationInfo().getIsOpen() == 1){
			pushNotificationIsOpen.setImageResource(R.drawable.pushnotification_open_image);
		}
		
		ImageView indicator = (ImageView) convertView
				.findViewById(R.id.groupindicater);

		if (isExpanded) {
			//indicator
				//	.setImageResource(R.drawable.list_page_adapter_indicator_up);
		} else {
			//indicator
				//	.setImageResource(R.drawable.list_page_adapter_indicator_down);

			if (adapterItems.get(groupPosition).getChildViewAdapterSettable() != null
					&& adapterItems.get(groupPosition)
							.getChildViewAdapterSettable().isCompletedSetting()) {
				adapterItems.get(groupPosition).getChildViewAdapterSettable()
						.resetAnimation();
			}
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public void onDeletePushNotification(int position) {
		onPushNotificationListViewApaterChangedListener.onDeletePushNotification(position);
	}
	
	private void setReceiveMessage(View convertView,int groupPosition){
		((TextView)convertView.findViewById(R.id.notification_name)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getSenderName());
		((TextView)convertView.findViewById(R.id.notification_date)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getDate());
		((TextView)convertView.findViewById(R.id.notification_title)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getTitle());
	}
	
	private void setSendMessage(View convertView,int groupPosition){
		((TextView)convertView.findViewById(R.id.notification_name)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getReceiverName());
		((TextView)convertView.findViewById(R.id.notification_date)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getDate());
		((TextView)convertView.findViewById(R.id.notification_title)).setText(adapterItems.get(groupPosition).getPushNotificationInfo().getTitle());
	}

	@Override
	public void onCompletedSetting(int position) {
		onPushNotificationListViewApaterChangedListener.onUpdateIsOpen(position);
	}
}
