package com.broccoli.hacca.adapter.pushnotification;

import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.pageinfo.PushNotificationInfo;

public class PushNotificationAdapterItem {
	private PushNotificationInfo pushNotificationInfo;
	private ChildViewAdapterSettable childViewAdapterSettable;

	public ChildViewAdapterSettable getChildViewAdapterSettable() {
		return childViewAdapterSettable;
	}

	public void setChildViewAdapterSettable(
			ChildViewAdapterSettable childViewAdapterSettable) {
		this.childViewAdapterSettable = childViewAdapterSettable;
	}

	public PushNotificationInfo getPushNotificationInfo() {
		return pushNotificationInfo;
	}

	public void setPushNotificationInfo(PushNotificationInfo pushNotificationInfo) {
		this.pushNotificationInfo = pushNotificationInfo;
	}
}
