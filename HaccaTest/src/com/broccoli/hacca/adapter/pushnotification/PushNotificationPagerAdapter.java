package com.broccoli.hacca.adapter.pushnotification;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.broccoli.hacca.R;
import com.broccoli.hacca.opensource.PagerSlidingTabStrip.IconTabProvider;

public class PushNotificationPagerAdapter extends PagerAdapter implements
		IconTabProvider {
	//https://github.com/astuetz/PagerSlidingTabStrip/issues/95
	//Changing tabs dynamically
	
	private final String TAG = "PushNotificationPagerAdapter";
	
	private Context context;

	private final int[] ICONS = { R.drawable.noticeboard_gongmozon,R.drawable.noticeboard_alba};
	private final String[] categoryList={"receive","send"};
	
	public PushNotificationPagerAdapter(Context context) {
		super();
		this.context = context;
	}

	@Override
	public int getCount() {
		return ICONS.length;
	}

	@Override
	public int getPageIconResId(int position) {
		return ICONS[position];
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = new PushNotificationListView(context, categoryList[position]).getView();
		
		container.addView(view, 0);
		
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object view) {
		container.removeView((View) view);
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		return v == ((View) o);
	}
}
