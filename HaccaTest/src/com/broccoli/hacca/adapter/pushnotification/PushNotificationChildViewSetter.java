package com.broccoli.hacca.adapter.pushnotification;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.deleteapi.DeleteAPI;
import com.broccoli.hacca.deleteapi.DeleteAPIImpl;
import com.broccoli.hacca.deleteapi.OnDeleteAPIListener;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.dialog.NoticeBoardEditableBoardDialog;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.passer.searchparser.CompanyPersonnelInfoParser;
import com.broccoli.hacca.passer.searchparser.StudentPersonnelInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.OnSearchUserTypeListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;
import com.broccoli.hacca.sqlitedb.ReceiveDBOfPushNotificationHandler;
import com.broccoli.hacca.sqlitedb.SendDBOfPushNotificationHandler;

public class PushNotificationChildViewSetter implements ChildViewAdapterSettable,
		OnSearchAPIListener,OnSearchUserTypeListener,OnClickListener{

	private final String TAG = "PushNotificationChildViewSetter";
	
	private View childView;
	private boolean isCompletedSetting = false;
	private PushNotificationAdapterItem adapterItem;

	private ImageButton deleteBtn;

	private Animation anim;
	
	private OnPushNotificationChildViewSetterListner listener;
	private int groupPosition;

	private LayoutInflater layoutInflater;
	private LinearLayout portfolioLayout;
	
	private UserType senderUserType;

	private String category;

	private Context context;
	
	public PushNotificationChildViewSetter(Context context,
			int groupPosition,OnPushNotificationChildViewSetterListner adapterSetterListener,String category) {
		this.context = context;
		this.groupPosition = groupPosition;
		this.listener = adapterSetterListener;
		this.category = category;
		
		this.layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public void setView(View view, Object personnelInfo) {
		this.childView = view;
		this.adapterItem = (PushNotificationAdapterItem) personnelInfo;
		
		((TextView)view.findViewById(R.id.notification_message)).setText(adapterItem.getPushNotificationInfo().getMessage());
		deleteBtn = (ImageButton)view.findViewById(R.id.delete_icon);
		
		portfolioLayout = (LinearLayout)view.findViewById(R.id.notification_portfolio);
		
		deleteBtn.setImageResource(R.drawable.delete_button);
		deleteBtn.setOnClickListener(this);
		
		SearchAPI searchAPI = new SearchAPIImpl(this,null);
		searchAPI.setOnSearchUserTypeListener(this);
		if(category.equals("receive")){
			searchAPI.searchUserType(adapterItem.getPushNotificationInfo().getSenderLoginId());
		}else if(category.equals("send")){
			searchAPI.searchUserType(adapterItem.getPushNotificationInfo().getReceiverLoginId());
		}
	}

	@Override
	public View getView() {
		return childView;
	}

	@Override
	public boolean isCompletedSetting() {
		return isCompletedSetting;
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.delete_icon) {
			listener.onDeletePushNotification(groupPosition);
		}
	}

	@Override
	public void startAnimation() {
		if (anim == null) {
			anim = new ScaleAnimation(1, 1, 0, 1);
			anim.setDuration(500);
			childView.startAnimation(anim);
		}
	}

	@Override
	public void resetAnimation() {
		anim = null;
	}
	
	private void setStudentPortfolio(StudentPersonnelInfo studentPersonnelInfo){
		View portfolioView = layoutInflater.inflate(
				R.layout.student_portfolio_layout, null);

		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_sex)).setText(studentPersonnelInfo.getStudentSex());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_dateofbrith)).setText("");
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_department))
				.setText(studentPersonnelInfo.getStudentDepartment());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_hope)).setText(studentPersonnelInfo.getStudentHope());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_ability))
				.setText(studentPersonnelInfo.getStudentAbility());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_career))
				.setText(studentPersonnelInfo
						.getStudentCareer());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_blog)).setText(studentPersonnelInfo.getStudentBlog());
		((TextView) portfolioView
				.findViewById(R.id.student_portfolio_email))
				.setText(studentPersonnelInfo.getStudentMail());
		
		portfolioLayout.addView(portfolioView);
	}
	
	private void setCompanyPortfolio(CompanyPersonnelInfo companyPersonnelInfo){
		View portfolioView = layoutInflater.inflate(
				R.layout.company_portfolio_layout, null);
		
		((TextView)portfolioView.findViewById(R.id.company_portfolio_size)).setText(companyPersonnelInfo.getCompanySize());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_businesstype)).setText(companyPersonnelInfo.getCompanyBusinessType());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_recruitment_area)).setText(companyPersonnelInfo.getCompanyRecruitmentArea());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_worktype)).setText(companyPersonnelInfo.getCompanyWorkType());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_area)).setText(companyPersonnelInfo.getCompanyWorkingArea());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_salary)).setText(companyPersonnelInfo.getCompanySalary());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_recruitvolume)).setText(companyPersonnelInfo.getCompanyRecruitmentVolume());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_recruitperiod)).setText(companyPersonnelInfo.getComapanyRecruitmentPeriod());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_homepage)).setText(companyPersonnelInfo.getCompanyHomepage());
		((TextView)portfolioView.findViewById(R.id.company_portfolio_email)).setText(companyPersonnelInfo.getCompanyEmail());
		
		portfolioLayout.addView(portfolioView);
	}

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		if(senderUserType == UserType.COMPANY){
			setCompanyPortfolio(pageInfo.getCompanyPersonnelInfo());
		}else if(senderUserType == UserType.STUDENT){
			setStudentPortfolio(pageInfo.getStudentPersonnelInfo());
		}
		
		if(category.equals("receive")){
			ReceiveDBOfPushNotificationHandler receiveDBOfPushNotificationHandler = new ReceiveDBOfPushNotificationHandler(context);
			receiveDBOfPushNotificationHandler.updateIsOpen(adapterItem.getPushNotificationInfo().getNumber()+"");
		}else if(category.equals("send")){
			SendDBOfPushNotificationHandler sendDBOfPushNotificationHandler = new SendDBOfPushNotificationHandler(context);
			sendDBOfPushNotificationHandler.updateIsOpen(adapterItem.getPushNotificationInfo().getNumber()+"");
		}
		
		isCompletedSetting = true;
		listener.onCompletedSetting(groupPosition);
	}

	@Override
	public void onFailSearch() {
		
	}

	@Override
	public void onTimeoutToSearch() {
		
	}

	@Override
	public void onGetUserType(UserType userType) {
		Log.i(TAG, userType.getName()+"");
		
		senderUserType = userType;
		
		if(userType == UserType.COMPANY){
			SearchAPI searchAPI = new SearchAPIImpl(this,new CompanyPersonnelInfoParser());
			if(category.equals("receive")){
				searchAPI.searchCompanyInfo(adapterItem.getPushNotificationInfo().getSenderLoginId());
			}else if(category.equals("send")){
				searchAPI.searchCompanyInfo(adapterItem.getPushNotificationInfo().getReceiverLoginId());
			}
		}else if(userType == UserType.STUDENT){
			SearchAPI searchAPI = new SearchAPIImpl(this,new StudentPersonnelInfoParser());
			if(category.equals("receive")){
				searchAPI.searchStudentInfo(adapterItem.getPushNotificationInfo().getSenderLoginId());
			}else if(category.equals("send")){
				searchAPI.searchStudentInfo(adapterItem.getPushNotificationInfo().getReceiverLoginId());
			}
		}
	}
}
