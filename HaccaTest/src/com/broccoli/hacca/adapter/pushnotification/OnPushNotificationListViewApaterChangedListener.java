package com.broccoli.hacca.adapter.pushnotification;

public interface OnPushNotificationListViewApaterChangedListener {
	void onDeletePushNotification(int position);
	void onUpdateIsOpen(int position);
}
