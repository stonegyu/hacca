package com.broccoli.hacca.adapter;


import android.view.View;

public interface ChildViewAdapterSettable {
	void setView(View view,Object personnelInfo);
	View getView();
	boolean isCompletedSetting();
	void startAnimation();
	void resetAnimation();
}
