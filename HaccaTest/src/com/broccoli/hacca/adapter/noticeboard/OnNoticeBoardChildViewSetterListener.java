package com.broccoli.hacca.adapter.noticeboard;

public interface OnNoticeBoardChildViewSetterListener {
	void onDeleteNoticeBoard(int groupPosition);
}
