package com.broccoli.hacca.adapter.noticeboard;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.deleteapi.DeleteAPI;
import com.broccoli.hacca.deleteapi.DeleteAPIImpl;
import com.broccoli.hacca.deleteapi.OnDeleteAPIListener;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.dialog.NoticeBoardEditableBoardDialog;
import com.broccoli.hacca.dialog.OnNoticeBoardDialogListener;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.NoticeBoardInfo;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;

public class NoticeBoardChildViewSetter implements ChildViewAdapterSettable,
		OnTouchListener, OnNoticeBoardDialogListener, OnRegisterAPIListener,
		OnDeleteAPIListener {
	
	private final String TAG = "NoticeBoardChildViewSetter";
	
	private View childView;
	private boolean isCompletedSetting = false;
	private NoticeBoardAdapterItem adapterItem;

	private ImageView editBtn;
	private ImageView deleteBtn;
	private UserType userType;
	private Context context;
	private NoticeBoardEditableBoardDialog noticeBoardEditableBoardDialog;
	private LoadingProgressDialog loadingProgressDialog;

	private Animation anim;

	private String professorLoginId;

	private String title;
	private String content;

	private TextView titleTextView;
	private TextView contentTextView;

	private OnNoticeBoardChildViewSetterListener listener;
	private int groupPosition;

	public NoticeBoardChildViewSetter(Context context, int groupPosition,
			OnNoticeBoardChildViewSetterListener adapterSetterListener) {
		this.context = context;
		this.groupPosition = groupPosition;
		this.listener = adapterSetterListener;

		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(context);
		userType = UserType.getUserType(internalStorageAPI.getUserType());
		professorLoginId = internalStorageAPI.getLoginId();
	}

	@Override
	public void setView(View view, Object personnelInfo) {
		this.childView = view;
		this.adapterItem = (NoticeBoardAdapterItem) personnelInfo;

		contentTextView = (TextView) childView
				.findViewById(R.id.inform_content);
		contentTextView.setText(adapterItem.getNoticeBoardInfo().getContent());

		editBtn = (ImageView) childView.findViewById(R.id.inform_editbtn);
		deleteBtn = (ImageView) childView.findViewById(R.id.inform_deletebtn);

		if (userType == UserType.PROFESSOR
				&& adapterItem.getNoticeBoardInfo().getProfessorLoginId()
						.equals(professorLoginId)) {
			editBtn.setOnTouchListener(this);
			deleteBtn.setOnTouchListener(this);
		} else {
			editBtn.setImageBitmap(null);
			deleteBtn.setImageBitmap(null);
		}
	}

	@Override
	public View getView() {
		return childView;
	}

	@Override
	public boolean isCompletedSetting() {
		return isCompletedSetting;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.inform_editbtn) {

			if (event.getAction() == MotionEvent.ACTION_UP) {

				noticeBoardEditableBoardDialog = new NoticeBoardEditableBoardDialog(
						context);
				noticeBoardEditableBoardDialog.setOnDialogListener(this);
				noticeBoardEditableBoardDialog.setTitle(adapterItem
						.getNoticeBoardInfo().getTitle());
				noticeBoardEditableBoardDialog.setContent(adapterItem
						.getNoticeBoardInfo().getContent());
				noticeBoardEditableBoardDialog.show();
			}
		} else if (v.getId() == R.id.inform_deletebtn) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				DeleteAPI deleteAPI = new DeleteAPIImpl(this);
				deleteAPI.deleteNoticeBoard(professorLoginId, adapterItem
						.getNoticeBoardInfo().getNumber());
			}
		}

		return true;
	}

	@Override
	public void startAnimation() {
		if (anim == null) {
			anim = new ScaleAnimation(1, 1, 0, 1);
			anim.setDuration(500);
			childView.startAnimation(anim);
		}
	}

	@Override
	public void resetAnimation() {
		anim = null;
	}

	@Override
	public void onSuccessRegister() {
		titleTextView.setText(title);
		contentTextView.setText(content);

		loadingProgressDialog.dismiss();
		Toast.makeText(context, "등록에 성공하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailRegister() {
		Toast.makeText(context, "등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCompletedEditNoticeBoard(String title, String content) {
		this.title = title;
		this.content = content;

		noticeBoardEditableBoardDialog.dismiss();

		NoticeBoardInfo noticeBoardInfo = new NoticeBoardInfo();
		noticeBoardInfo.setNumber(adapterItem.getNoticeBoardInfo().getNumber());
		noticeBoardInfo.setProfessorLoginId(professorLoginId);
		noticeBoardInfo.setTitle(title);
		noticeBoardInfo.setContent(content);
		noticeBoardInfo.setCategory(adapterItem.getNoticeBoardInfo()
				.getCategory());

		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		registerAPI.registerNoticeBoard(noticeBoardInfo);

		loadingProgressDialog = new LoadingProgressDialog(context);
		loadingProgressDialog.show();
	}

	@Override
	public void onCompletedDelete() {
		listener.onDeleteNoticeBoard(groupPosition);
	}

	@Override
	public void onTimeoutToDelete() {
		Toast.makeText(context, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
		Toast.makeText(context, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void onDupplication() {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		
=======

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}
}
