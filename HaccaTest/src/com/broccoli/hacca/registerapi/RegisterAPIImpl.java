package com.broccoli.hacca.registerapi;

<<<<<<< HEAD
=======
import java.util.ArrayList;

import org.apache.http.NameValuePair;

>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
import android.util.Log;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.NoticeBoardInfo;
import com.broccoli.hacca.pageinfo.ProfessorCommentInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class RegisterAPIImpl implements RegisterAPI,OnDataBaseConnectAPIListener{

	private final String TAG="RegisterAPIImpl";
	
	private OnRegisterAPIListener registerAPIListener;

	public RegisterAPIImpl(OnRegisterAPIListener registerAPIListener){
		this.registerAPIListener = registerAPIListener;
	}
	
	@Override
	public void registerStudent(StudentPersonnelInfo studentPersonnelInfo) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.STUDENT_REGISTER);
		urlDataStorage.setParameter(UrlParameterType.STUDENT_LOGINID, studentPersonnelInfo.getStudentLoginId());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_PASSWORD, studentPersonnelInfo.getStudentPassword());
		urlDataStorage.setParameter(UrlParameterType.REGISTRATION_ID_OF_DEVICE, studentPersonnelInfo.getRegistrationIdOfDevice());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_NAME, studentPersonnelInfo.getStudentName());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_CAREER, studentPersonnelInfo.getStudentCareer());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_HOPE, studentPersonnelInfo.getStudentHope());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_BLOG, studentPersonnelInfo.getStudentBlog());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_MAIL, studentPersonnelInfo.getStudentMail());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_ABILITY, studentPersonnelInfo.getStudentAbility());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_AGE, studentPersonnelInfo.getStudentAge());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_SEX, studentPersonnelInfo.getStudentSex());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_DEPARTMENT, studentPersonnelInfo.getStudentDepartment());
		urlDataStorage.setParameter(UrlParameterType.HANSUNGUNIV_ID, studentPersonnelInfo.getHansungUnivId());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void registerCompany(CompanyPersonnelInfo companyPersonnelInfo) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.COMPANY_REGISTER);
		urlDataStorage.setParameter(UrlParameterType.COMPANY_LOGINID, companyPersonnelInfo.getCompanyLoginId());
		urlDataStorage.setParameter(UrlParameterType.REGISTRATION_ID_OF_DEVICE, companyPersonnelInfo.getRegistrationIdOfDevice());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_NAME, companyPersonnelInfo.getCompanyName());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_AREA, companyPersonnelInfo.getCompanyWorkingArea());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_BUSINESSTYPE, companyPersonnelInfo.getCompanyBusinessType());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_HOMEPAGE, companyPersonnelInfo.getCompanyHomepage());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_MAIL, companyPersonnelInfo.getCompanyEmail());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_PASSWORD, companyPersonnelInfo.getCompanyPasswd());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITPERIOD, companyPersonnelInfo.getComapanyRecruitmentPeriod());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITVOLUME, companyPersonnelInfo.getCompanyRecruitmentVolume());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_SALARY, companyPersonnelInfo.getCompanySalary());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_SIZE, companyPersonnelInfo.getCompanySize());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_WORKTYPE, companyPersonnelInfo.getCompanyWorkType());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITMENTAREA, companyPersonnelInfo.getCompanyRecruitmentArea());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_CODE, companyPersonnelInfo.getCompanyCertificationNumber());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
<<<<<<< HEAD
		Log.d("llog",url);
=======
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void registerProfessorComment(ProfessorCommentInfo commentInfo) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.PROFESSOR_COMMENT_REGISTER);
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, commentInfo.getProfessorLoginId());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_LOGINID, commentInfo.getStudentLoginId());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_COMMENT, commentInfo.getComment());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}
	
	@Override
	public void confirmProfessorCode(String code, Boolean isProfessor){
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.CONFIRM_PROFESSOR_CODE);
		urlDataStorage.setParameter(UrlParameterType.CONFIRM_PROFESSOR_CODE, code);
		urlDataStorage.setParameter(UrlParameterType.IS_PROFESSOR, ""+isProfessor);
<<<<<<< HEAD
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
=======
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}
	@Override
	public void registerNoticeBoard(NoticeBoardInfo noticeBoardInfo) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.NOTICEBOARD_REGISTER);
		urlDataStorage.setParameter(UrlParameterType.NOTICEBOARD_NUMBER, noticeBoardInfo.getNumber());
		urlDataStorage.setParameter(UrlParameterType.NOTICEBOARD_TITLE, noticeBoardInfo.getTitle());
		urlDataStorage.setParameter(UrlParameterType.NOTICEBOARD_CONTENT, noticeBoardInfo.getContent());
		urlDataStorage.setParameter(UrlParameterType.NOTICEBOARD_CATEGORY, noticeBoardInfo.getCategory());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, noticeBoardInfo.getProfessorLoginId());
		
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		
		//맨앞에 ASCII CODE 65279가 붙음
<<<<<<< HEAD
		
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		result = result.replace(String.valueOf((char)65279), "" );
		result = result.replace(String.valueOf("\""), "" );
		Log.i("llog","result "+result);
		if(result.equals("true")) {
			registerAPIListener.onSuccessRegister();
		}else if(result.equals("false")){
			registerAPIListener.onFailRegister();
		}else if(result.equals("dup")){
			Log.i("llog","duppppcall");
			registerAPIListener.onDupplication();
		}else if(result.equals("notdup")){
			Log.i("llog","notduppppcall");
			registerAPIListener.onNotDupplication();
		}
	}

	@Override
	public void onFailToConnectDataBase() {
		registerAPIListener.onFailRegister();
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		registerAPIListener.onTimeoutToRegister();
	}

	@Override
	public void registerProfessor(ProfessorPersonnelInfo professorPersonnelInfo) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
=======
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.PROFESSOR_REGISTER);
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, professorPersonnelInfo.getProfessorId());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_PASSWORD, professorPersonnelInfo.getProfessorPasswd());
		urlDataStorage.setParameter(UrlParameterType.REGISTRATION_ID_OF_DEVICE, professorPersonnelInfo.getRegistrationIdOfDevice());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_NAME, professorPersonnelInfo.getProfessorName());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LEVEL, professorPersonnelInfo.getProfessorLevel());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_DEPARTMENT, professorPersonnelInfo.getProfessorDepartment());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_EMAIL, professorPersonnelInfo.getProfessorEmail());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_CODE, professorPersonnelInfo.getProfessorCode());
<<<<<<< HEAD
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
=======
		urlDataStorage.setParameter(UrlParameterType.HANSUNGUNIV_ID, professorPersonnelInfo.getHansungUnivId());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}

	@Override
	public void idDuplicationCheck(String id) {
<<<<<<< HEAD
		// TODO Auto-generated method stub
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.ID_DUPLICATION_CHECK);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, id);
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
=======
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.ID_DUPLICATION_CHECK);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, id);
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		ArrayList<NameValuePair> nameValuePairs = UrlFactory.getInstance().getNameValuePairs(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url,nameValuePairs);
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	}
}
