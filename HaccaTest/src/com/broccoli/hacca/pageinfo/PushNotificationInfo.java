package com.broccoli.hacca.pageinfo;

public class PushNotificationInfo {
	private int number;
	private int isOpen;
	private String senderLoginId;
	private String receiverLoginId;
	private String senderName;
	private String receiverName;
	private String title;
	private String message;
	private String date;
	
	
	public int getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSenderLoginId() {
		return senderLoginId;
	}
	public void setSenderLoginId(String senderLoginId) {
		this.senderLoginId = senderLoginId;
	}
	public String getReceiverLoginId() {
		return receiverLoginId;
	}
	public void setReceiverLoginId(String receiverLoginId) {
		this.receiverLoginId = receiverLoginId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
