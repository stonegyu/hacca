package com.broccoli.hacca.hansungunivcertification;

public class HansungUnivCertificatorImpl implements HansungUnivCertificator,OnHansungUnivCertificatorWorkerListener{ 
	private final String url = "http://info.hansung.ac.kr/servlet/s_gong.gong_login_ssl";
	
	private OnHansungUnivCertificatorListener onHansungUnivCertificatorListener;

	public HansungUnivCertificatorImpl(OnHansungUnivCertificatorListener onHansungUnivCertificatorListener){
		this.onHansungUnivCertificatorListener = onHansungUnivCertificatorListener;
	}
	
	@Override
	public void certificate(String loginId, String password) {
		HansungUnivCertificatorWorker worker = new HansungUnivCertificatorWorker(this);
		worker.execute(url,loginId,password);
	}

	@Override
	public void onCompletedHansungUnivCertification(String result) {
		if(result.equals("true")){
			onHansungUnivCertificatorListener.onSuccessToCertificateHansungUniv();
		}else if(result.equals("false")){
			onHansungUnivCertificatorListener.onFailToCertificateHansungUniv();
		}
	}
}
