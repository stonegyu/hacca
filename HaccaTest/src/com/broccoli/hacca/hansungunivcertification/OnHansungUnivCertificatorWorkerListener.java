package com.broccoli.hacca.hansungunivcertification;

public interface OnHansungUnivCertificatorWorkerListener {
	void onCompletedHansungUnivCertification(String result);
}
