package com.broccoli.hacca.gcm;

import android.content.Context;

import com.broccoli.hacca.R;
import com.google.android.gcm.GCMBroadcastReceiver;

public class GCMReceiver extends GCMBroadcastReceiver{

	@Override
	protected String getGCMIntentServiceClassName(Context context) {
		return context.getString(R.string.gcm_service_class);
	}	
	
}
