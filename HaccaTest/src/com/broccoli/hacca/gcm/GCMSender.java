package com.broccoli.hacca.gcm;

import com.broccoli.hacca.pageinfo.PushNotificationInfo;

public interface GCMSender {
	void sendGCMMessage(PushNotificationInfo pushNoticeInfo);
}
