package com.broccoli.hacca.urlfactory;

public enum UrlType {

	LOGIN("login/login.php?"), 
	
	REGISTRATIONIDOFDEVICE_CHECK("registrationIdOfDevice/checkRegistrationIdOfDevice.php?"),
	REGISTRATIONIDOFDEVICE_UPDATE("registrationIdOfDevice/updateRegistrationIdOfDevice.php?"),
	
	STUDENT_REGISTER("register/student_register.php?"), 
	COMPANY_REGISTER("register/company_register.php?"),
	PROFESSOR_REGISTER("register/professor_register.php?"),
	
	PROFESSOR_COMMENT_REGISTER("register/professorComment_register.php?"), 
	NOTICEBOARD_REGISTER("register/noticeBoardInfo_register.php?"), 
	
	STUDENT_SEARCH("search/student_search.php?"),
	COMPANY_SEARCH("search/company_search.php?"), 
	PROFESSOR_SEARCH("search/professor_search.php?"),
	PROFESSOR_COMMENT_SEARCH("search/professorComment_search.php?"), 
	NOTICEBOARDINFO_SEARCH("search/noticeBoardInfo_search.php?"), 
	
	NOTICEBOARD_DELETE("delete/noticeBoardInfo_delete.php?"),
	
	PUSHNOTICE_SEND_MESSAGE("GCM/GCMSendMessage.php?"),

	CONFIRM_PROFESSOR_CODE("register/professor_code.php?"),
	
	STUDENT_UPDATE_PROFILE("update/student_profile.php?"),
	PROFESSOR_UPDATE_PROFILE("update/professor_profile.php?"),
	COMPANY_UPDATE_PROFILE("update/company_profile.php?"),
	
<<<<<<< HEAD
	ID_DUPLICATION_CHECK("register/id_duplication_check.php?");
=======
	ID_DUPLICATION_CHECK("register/id_duplication_check.php?"),
	
	USERTYPE_SEARCH("search/userType_search.php?"),
	
	HANSUNGUNIV_ID_SEARCH("search/hansungUnivId_search.php?");
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958
	
	private String name;

	private UrlType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
