package com.broccoli.hacca.urlfactory;

public enum UrlParameterType {
	LOGINID("loginId"), REGISTRATION_ID_OF_DEVICE("registrationIdOfDevice"), PASSWORD(
			"password"),

	STUDENT_LOGINID("studentLoginId"), STUDENT_NAME("studentName"), STUDENT_CAREER(
			"studentCareer"), STUDENT_HOPE("studentHope"), STUDENT_BLOG(
			"studentBlog"), STUDENT_MAIL("studentMail"), STUDENT_ABILITY(
			"studentAbility"), STUDENT_AGE("studentAge"), STUDENT_SEX(
			"studentSex"), STUDENT_DEPARTMENT("studentDepartment"), STUDENT_PASSWORD(
			"studentPassword"),

	COMPANY_LOGINID("companyLoginId"), COMPANY_NAME("companyName"), COMPANY_SALARY(
			"companySalary"), COMPANY_RECRUITVOLUME("companyRecruitVolume"), COMPANY_RECRUITPERIOD(
			"companyRecruitPeriod"), COMPANY_MAIL("companyMail"), COMPANY_HOMEPAGE(
			"companyHomePage"), COMPANY_PASSWORD("companyPassword"), COMPANY_AREA(
			"companyArea"), COMPANY_SIZE("companySize"), COMPANY_BUSINESSTYPE(
			"companyBusinessType"), COMPANY_WORKTYPE("companyWorkType"), COMPANY_RECRUITMENTAREA(
			"companyRecruitmentArea"), PROFESSOR_CODE("professorCode"), IS_PROFESSOR("isProfessor"),

	PROFESSOR_LOGINID("professorLoginId"), PROFESSOR_NAME("professorName"), PROFESSOR_PASSWORD(
			"professorPasswd"), PROFESSOR_LEVEL("professorLevel"), PROFESSOR_DEPARTMENT(
			"professorDepartment"), PROFESSOR_EMAIL("professorEmail"),

	PROFESSOR_COMMENT("comment"),

	NOTICEBOARD_CATEGORY("category"), NOTICEBOARD_NUMBER("number"), NOTICEBOARD_TITLE(
			"title"), NOTICEBOARD_CONTENT("content"),

	PUSHNOTICE_SENDER_LOGINID("senderLoginId"), PUSHNOTICE_RECEIVER_LOGINID(
			"receiverLoginId"), PUSHNOTICE_MESSAGE("message"), PUSHNOTICE_TITLE(
			"title"),PUSHNOTICE_SENDER_NAME("senderName"),

	SEARCH_TEXT("searchText"), LIMIT("limit"),

<<<<<<< HEAD
	SEARCH_TEXT("searchText"), LIMIT("limit"),

	CONFIRM_PROFESSOR_CODE("professorCode");
=======
	CONFIRM_PROFESSOR_CODE("professorCode"),
>>>>>>> cb91bfc96f6a07cb4f0b08770ee8ef4b2c44a958

	HANSUNGUNIV_ID("hansungUnivId");
	private String name;

	private UrlParameterType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
