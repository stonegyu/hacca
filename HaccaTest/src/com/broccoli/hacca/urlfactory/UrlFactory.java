package com.broccoli.hacca.urlfactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class UrlFactory {
	private final String TAG = "UrlFactory";
	
	private final String serverUrl="http://xargus.cafe24.com/hacca/";
	private static UrlFactory urlFactory = new UrlFactory();
	
	private UrlFactory(){
		
	}
	
	public static UrlFactory getInstance(){
		return urlFactory;
	}
	
	public String getUrl(UrlDataStorage urlDataStorage){
		
		return serverUrl+urlDataStorage.getUrlType().getName();
				//+makeUrlParameters(urlDataStorage);
	}
	
	public ArrayList<NameValuePair> getNameValuePairs(UrlDataStorage urlDataStorage){
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		
		Map<String, String> map = urlDataStorage.getUrlDataSet();
		
		Iterator<String> iterator = map.keySet().iterator();
		
		while (iterator.hasNext()) {
			String key = iterator.next();
			nameValuePairs.add(new BasicNameValuePair(key, map.get(key)));
			
			Log.i(TAG, key+" "+map.get(key));
		}
		
		return nameValuePairs;
	}
	
//	private String makeUrlParameters(UrlDataStorage dataStorage) {
//		StringBuffer buffer = new StringBuffer();
//		Map<String, String> map = dataStorage.getUrlDataSet();
//
//		Iterator<String> iterator = map.keySet().iterator();
//
//		while (iterator.hasNext()) {
//			String key = iterator.next();
//			buffer.append(key + "=" + map.get(key));
//			buffer.append("&");
//		}
//
//		return buffer.toString();
//	}
}
