package com.broccoli.hacca.registerapi;

import com.broccoli.hacca.pageinfo.ProfessorCommentInfo;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.NoticeBoardInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;

public interface RegisterAPI {
	void registerStudent(StudentPersonnelInfo studentPersonnelInfo);
	void registerCompany(CompanyPersonnelInfo companyPersonnelInfo);
	void registerProfessor(ProfessorPersonnelInfo professorPersonnelInfo);
	void registerProfessorComment(ProfessorCommentInfo commentInfo);
	void registerNoticeBoard(NoticeBoardInfo noticeBoardInfo);
	void confirmProfessorCode(String code, Boolean IsProfessor);
	void idDuplicationCheck(String id);
}
