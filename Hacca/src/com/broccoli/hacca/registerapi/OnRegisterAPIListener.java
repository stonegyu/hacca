package com.broccoli.hacca.registerapi;

public interface OnRegisterAPIListener {
	void onSuccessRegister();
	void onFailRegister();
	void onTimeoutToRegister();
	void onNotDupplication();
	void onDupplication();
}
