package com.broccoli.hacca.pageinfo;

public class CompanyPersonnelInfo {
	private String  companyName, companyPasswd, companyRepasswd,
			companyRecruitmentArea, companyWorkingArea, companySalary,
			companyRecruitmentVolume, comapanyRecruitmentPeriod, companyEmail,
			companyHomepage, companyCertificationNumber;
	private String companyLoginId;
	private String registrationIdOfDevice;
	
	private String companySize, companyBusinessType, companyWorkType;


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyPasswd() {
		return companyPasswd;
	}

	public void setCompanyPasswd(String companyPasswd) {
		this.companyPasswd = companyPasswd;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getCompanyBusinessType() {
		return companyBusinessType;
	}

	public void setCompanyBusinessType(String companyBusinessType) {
		this.companyBusinessType = companyBusinessType;
	}

	public String getCompanyWorkType() {
		return companyWorkType;
	}

	public void setCompanyWorkType(String companyWorkType) {
		this.companyWorkType = companyWorkType;
	}

	public String getCompanyRepasswd() {
		return companyRepasswd;
	}

	public void setCompanyRepasswd(String companyRepasswd) {
		this.companyRepasswd = companyRepasswd;
	}

	public String getCompanyRecruitmentArea() {
		return companyRecruitmentArea;
	}

	public void setCompanyRecruitmentArea(String companyRecruitmentArea) {
		this.companyRecruitmentArea = companyRecruitmentArea;
	}

	public String getCompanyWorkingArea() {
		return companyWorkingArea;
	}

	public void setCompanyWorkingArea(String companyWorkingArea) {
		this.companyWorkingArea = companyWorkingArea;
	}

	public String getCompanySalary() {
		return companySalary;
	}

	public void setCompanySalary(String companySalary) {
		this.companySalary = companySalary;
	}

	public String getCompanyRecruitmentVolume() {
		return companyRecruitmentVolume;
	}

	public void setCompanyRecruitmentVolume(String companyRecruitmentVolume) {
		this.companyRecruitmentVolume = companyRecruitmentVolume;
	}

	public String getComapanyRecruitmentPeriod() {
		return comapanyRecruitmentPeriod;
	}

	public void setComapanyRecruitmentPeriod(String comapanyRecruitmentPeriod) {
		this.comapanyRecruitmentPeriod = comapanyRecruitmentPeriod;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getCompanyHomepage() {
		return companyHomepage;
	}

	public void setCompanyHomepage(String companyHomepage) {
		this.companyHomepage = companyHomepage;
	}

	public String getCompanyCertificationNumber() {
		return companyCertificationNumber;
	}

	public void setCompanyCertificationNumber(String companyCertificationNumber) {
		this.companyCertificationNumber = companyCertificationNumber;
	}

	public String getCompanyLoginId() {
		return companyLoginId;
	}

	public void setCompanyLoginId(String companyLoginId) {
		this.companyLoginId = companyLoginId;
	}

	public String getRegistrationIdOfDevice() {
		return registrationIdOfDevice;
	}

	public void setRegistrationIdOfDevice(String registrationIdOfDevice) {
		this.registrationIdOfDevice = registrationIdOfDevice;
	}
}
