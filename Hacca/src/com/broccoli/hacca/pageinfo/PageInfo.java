package com.broccoli.hacca.pageinfo;

import java.util.ArrayList;

public class PageInfo {

	private StudentPersonnelInfo studentPersonnelInfo;
	private CompanyPersonnelInfo companyPersonnelInfo;
	private ProfessorPersonnelInfo professorPersonnelInfo;
	
	private ArrayList<StudentPersonnelInfo> studentPersonnelInfos;
	private ArrayList<CompanyPersonnelInfo> companyPersonnelInfos;
	private ArrayList<ProfessorPersonnelInfo> professorPersonnelInfos;
	private ArrayList<ProfessorCommentInfo> commentInfos;
	private ArrayList<NoticeBoardInfo> noticeBoardInfos;
	
	public ProfessorPersonnelInfo getProfessorPersonnelInfo() {
		return professorPersonnelInfo;
	}

	public void setProfessorPersonnelInfo(
			ProfessorPersonnelInfo professorPersonnelInfo) {
		this.professorPersonnelInfo = professorPersonnelInfo;
	}

	public ArrayList<ProfessorPersonnelInfo> getProfessorPersonnelInfos() {
		return professorPersonnelInfos;
	}

	public void setProfessorPersonnelInfos(
			ArrayList<ProfessorPersonnelInfo> professorPersonnelInfos) {
		this.professorPersonnelInfos = professorPersonnelInfos;
	}

	public ArrayList<NoticeBoardInfo> getNoticeBoardInfos() {
		return noticeBoardInfos;
	}

	public void setNoticeBoardInfos(ArrayList<NoticeBoardInfo> noticeBoardInfos) {
		this.noticeBoardInfos = noticeBoardInfos;
	}

	public ArrayList<CompanyPersonnelInfo> getCompanyPersonnelInfos() {
		return companyPersonnelInfos;
	}

	public void setCompanyPersonnelInfos(
			ArrayList<CompanyPersonnelInfo> companyPersonnelInfos) {
		this.companyPersonnelInfos = companyPersonnelInfos;
	}

	public ArrayList<ProfessorCommentInfo> getCommentInfos() {
		return commentInfos;
	}

	public void setCommentInfos(ArrayList<ProfessorCommentInfo> commentInfos) {
		this.commentInfos = commentInfos;
	}

	public ArrayList<StudentPersonnelInfo> getStudentPersonnelInfos() {
		return studentPersonnelInfos;
	}

	public void setStudentPersonnelInfos(
			ArrayList<StudentPersonnelInfo> studentPersonnelInfos) {
		this.studentPersonnelInfos = studentPersonnelInfos;
	}

	public CompanyPersonnelInfo getCompanyPersonnelInfo() {
		return companyPersonnelInfo;
	}

	public void setCompanyPersonnelInfo(CompanyPersonnelInfo companyPersonnelInfo) {
		this.companyPersonnelInfo = companyPersonnelInfo;
	}

	public StudentPersonnelInfo getStudentPersonnelInfo() {
		return studentPersonnelInfo;
	}

	public void setStudentPersonnelInfo(StudentPersonnelInfo studentPersonnelInfo) {
		this.studentPersonnelInfo = studentPersonnelInfo;
	}
	
	
}
