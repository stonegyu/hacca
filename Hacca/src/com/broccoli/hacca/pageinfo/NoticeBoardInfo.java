package com.broccoli.hacca.pageinfo;

public class NoticeBoardInfo {
	private String number;
	private String professorName;
	private String title;
	private String content;
	private String date;
	private String category;
	private String professorLoginId;
	
	
	public String getProfessorLoginId() {
		return professorLoginId;
	}
	public void setProfessorLoginId(String professorLoginId) {
		this.professorLoginId = professorLoginId;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getProfessorName() {
		return professorName;
	}
	public void setProfessorName(String professorName) {
		this.professorName = professorName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
}
