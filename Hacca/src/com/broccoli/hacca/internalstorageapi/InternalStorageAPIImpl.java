package com.broccoli.hacca.internalstorageapi;

import android.content.Context;
import android.content.SharedPreferences;

public class InternalStorageAPIImpl implements InternalStorageAPI{
	private Context context;
	private final String LoginIDStoreKey = "LOGINID_KEY";
	private final String DeviceIDStoreKey = "DEVICEID_KEY";
	private final String UserTypeIDStoreKey = "USERTYPE_KEY";
	private final String UserNameIDStoreKey = "USERNAME_KEY";
	private final String AutoLoginStoreKey = "AUTILOGIN_KEY";
	private final String sharedPreferenceName = "STORE_NAME";

	public InternalStorageAPIImpl(Context context){
		this.context = context;
	}
	
	@Override
	public String getLoginId() {
		return getStoredInfo(LoginIDStoreKey);
	}

	@Override
	public String getRegistrationIdOfDevice() {
		return getStoredInfo(DeviceIDStoreKey);
	}

	@Override
	public void setLoginId(String loginID) {
		storeInfo(LoginIDStoreKey, loginID);
	}

	@Override
	public void setRegistrationIdOfDevice(String deviceID) {
		storeInfo(DeviceIDStoreKey, deviceID);
	}

	@Override
	public String getUserName() {
		return getStoredInfo(UserNameIDStoreKey);
	}

	@Override
	public void setUserName(String userName) {
		storeInfo(UserNameIDStoreKey, userName);
	}
	
	@Override
	public String getUserType() {
		return getStoredInfo(UserTypeIDStoreKey);
	}

	@Override
	public void setUserType(String userType) {
		storeInfo(UserTypeIDStoreKey, userType);
	}
	
	private void storeInfo(String key, String info) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				sharedPreferenceName, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, info);
		editor.commit();
	}

	private String getStoredInfo(String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				sharedPreferenceName, context.MODE_PRIVATE);
		return sharedPreferences.getString(key, null);
	}

	@Override
	public boolean isAutoLogin() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				sharedPreferenceName, context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(AutoLoginStoreKey, false);
	}

	@Override
	public void setAutoLogin(boolean isAutoLogin) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				sharedPreferenceName, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(AutoLoginStoreKey, isAutoLogin);
		editor.commit();
	}
}
