package com.broccoli.hacca.internalstorageapi;


public interface InternalStorageAPI {
	String getLoginId();
	String getUserName();
	String getRegistrationIdOfDevice();
	String getUserType();
	boolean isAutoLogin();
	void setLoginId(String loginId);
	void setUserName(String userName);
	void setRegistrationIdOfDevice(String registrationIdOfDevice);
	void setUserType(String userType);
	void setAutoLogin(boolean isAutoLogin);
}
