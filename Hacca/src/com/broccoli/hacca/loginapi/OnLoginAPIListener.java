package com.broccoli.hacca.loginapi;

public interface OnLoginAPIListener {
	void onSuccessLogin(String UserType,String UserName);
	void onFailLogin();
	void onTimeoutToLogin();
}
