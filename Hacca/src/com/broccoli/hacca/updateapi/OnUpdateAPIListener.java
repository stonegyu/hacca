package com.broccoli.hacca.updateapi;

public interface OnUpdateAPIListener {
	void onSuccessUpdate();
	void onFailUpdate();
	void onTimeoutToUpdate();
}
