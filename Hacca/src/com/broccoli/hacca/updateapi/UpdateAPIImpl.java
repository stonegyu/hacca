package com.broccoli.hacca.updateapi;

import android.util.Log;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class UpdateAPIImpl implements UpdateAPI, OnDataBaseConnectAPIListener{
	private final String TAG="UpdateAPIImpl";
	private OnUpdateAPIListener updateAPIListener;
	
	public UpdateAPIImpl(OnUpdateAPIListener updateAPIListener){
		this.updateAPIListener = updateAPIListener;
	}

	@Override
	public void updateStudentProfile(StudentPersonnelInfo studentPersonnelInfo) {
		// TODO Auto-generated method stub
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.STUDENT_UPDATE_PROFILE);
		urlDataStorage.setParameter(UrlParameterType.STUDENT_LOGINID, studentPersonnelInfo.getStudentLoginId());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_NAME, studentPersonnelInfo.getStudentName());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_CAREER, studentPersonnelInfo.getStudentCareer());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_HOPE, studentPersonnelInfo.getStudentHope());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_BLOG, studentPersonnelInfo.getStudentBlog());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_MAIL, studentPersonnelInfo.getStudentMail());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_ABILITY, studentPersonnelInfo.getStudentAbility());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_AGE, studentPersonnelInfo.getStudentAge());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_SEX, studentPersonnelInfo.getStudentSex());
		urlDataStorage.setParameter(UrlParameterType.STUDENT_DEPARTMENT, studentPersonnelInfo.getStudentDepartment());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
	}

	@Override
	public void updateCompanyProfile(CompanyPersonnelInfo companyPersonnelInfo) {
		// TODO Auto-generated method stub
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.COMPANY_UPDATE_PROFILE);
		urlDataStorage.setParameter(UrlParameterType.COMPANY_LOGINID, companyPersonnelInfo.getCompanyLoginId());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_NAME, companyPersonnelInfo.getCompanyName());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_AREA, companyPersonnelInfo.getCompanyWorkingArea());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_BUSINESSTYPE, companyPersonnelInfo.getCompanyBusinessType());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_HOMEPAGE, companyPersonnelInfo.getCompanyHomepage());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_MAIL, companyPersonnelInfo.getCompanyEmail());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITPERIOD, companyPersonnelInfo.getComapanyRecruitmentPeriod());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITVOLUME, companyPersonnelInfo.getCompanyRecruitmentVolume());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_SALARY, companyPersonnelInfo.getCompanySalary());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_SIZE, companyPersonnelInfo.getCompanySize());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_WORKTYPE, companyPersonnelInfo.getCompanyWorkType());
		urlDataStorage.setParameter(UrlParameterType.COMPANY_RECRUITMENTAREA, companyPersonnelInfo.getCompanyRecruitmentArea());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		Log.d("llog",url);
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
	}

	@Override
	public void updateProfessorProfile(
			ProfessorPersonnelInfo professorPersonnelInfo) {
		// TODO Auto-generated method stub
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.PROFESSOR_UPDATE_PROFILE);
		
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LOGINID, professorPersonnelInfo.getProfessorId());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_NAME, professorPersonnelInfo.getProfessorName());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_LEVEL, professorPersonnelInfo.getProfessorLevel());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_DEPARTMENT, professorPersonnelInfo.getProfessorDepartment());
		urlDataStorage.setParameter(UrlParameterType.PROFESSOR_EMAIL, professorPersonnelInfo.getProfessorEmail());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
	}
	
	@Override
	public void onSuccessToConnectDataBase(String result) {
		// TODO Auto-generated method stub
		result = result.replace(String.valueOf((char)65279), "" );
		
		if(result.equals("true")) {
			updateAPIListener.onSuccessUpdate();
		}else if(result.equals("false")){
			updateAPIListener.onFailUpdate();
		}
	}

	@Override
	public void onFailToConnectDataBase() {
		// TODO Auto-generated method stub
		updateAPIListener.onFailUpdate();
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		// TODO Auto-generated method stub
		updateAPIListener.onTimeoutToUpdate();
	}

}
