package com.broccoli.hacca.deleteapi;

public interface OnDeleteAPIListener {
	void onCompletedDelete();
	void onTimeoutToDelete();
}
