package com.broccoli.hacca.registrationidofdeviceapi;


public class RegistrationIdOfDeviceGetter {
	private static RegistrationIdOfDeviceGetter gcmRegisterIdGetter = new RegistrationIdOfDeviceGetter();
	private OnRegisterIdOfDeviceGetterListener onGCMRegisterIdListener;
	private String deviceId;
	
	private RegistrationIdOfDeviceGetter(){
		
	}
	
	public static RegistrationIdOfDeviceGetter getInstance(){
		return gcmRegisterIdGetter;
	}
	
	public void setOnGCMRegisterIdListner(OnRegisterIdOfDeviceGetterListener onGCMRegisterIdListener){
		this.onGCMRegisterIdListener = onGCMRegisterIdListener;
	}
	
	public void setDeviceId(String registerId){
		this.deviceId = registerId;
		this.onGCMRegisterIdListener.onGetRegisterId(registerId);
	}
	
	public String getDeviceId(){
		return deviceId;
	}
}
