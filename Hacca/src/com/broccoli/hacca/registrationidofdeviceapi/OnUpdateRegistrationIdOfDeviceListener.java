package com.broccoli.hacca.registrationidofdeviceapi;

public interface OnUpdateRegistrationIdOfDeviceListener {
	void onSuccessUpdateRegistrationIdOfDevice();
	void onFailUpdateRegistrationIdOfDevice();
	
	void onTimeoutToUpdateRegistrationIdOfDevice();
}
