package com.broccoli.hacca.registrationidofdeviceapi;

import android.util.Log;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class RegistrationIdOfDeviceAPIImpl implements
		RegistrationIdOfDeviceAPI, OnDataBaseConnectAPIListener {

	private final String TAG = "RegistrationIdOfDeviceAPIImpl";
	
	private OnCheckRegistrationIdOfDeviceListener onCheckRegistrationIdOfDeviceListener;
	private OnUpdateRegistrationIdOfDeviceListener onUpdateRegistrationIdOfDeviceListener;

	private boolean isCheckRegistrationIdOfDevice = false;
	private boolean isUpdateRegistrationIdOfDevice = false;

	@Override
	public void checkRegistrationIdOfDevice(String loginId,
			String registrationIdOfDevice) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.REGISTRATIONIDOFDEVICE_CHECK);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, loginId);
		urlDataStorage.setParameter(UrlParameterType.REGISTRATION_ID_OF_DEVICE,
				registrationIdOfDevice);

		String url = UrlFactory.getInstance().getUrl(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);

		isCheckRegistrationIdOfDevice = true;
		isUpdateRegistrationIdOfDevice = false;
	}

	@Override
	public void updateRegistrationIdOfDevice(String loginId,
			String registrationIdOfDevice) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(
				UrlType.REGISTRATIONIDOFDEVICE_UPDATE);
		urlDataStorage.setParameter(UrlParameterType.LOGINID, loginId);
		urlDataStorage.setParameter(UrlParameterType.REGISTRATION_ID_OF_DEVICE,
				registrationIdOfDevice);

		String url = UrlFactory.getInstance().getUrl(urlDataStorage);

		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
		
		isCheckRegistrationIdOfDevice = false;
		isUpdateRegistrationIdOfDevice = true;
	}

	@Override
	public void setOnCheckRegistrationIdOfDevice(
			OnCheckRegistrationIdOfDeviceListener onCheckRegistrationIdOfDeviceListener) {
		this.onCheckRegistrationIdOfDeviceListener = onCheckRegistrationIdOfDeviceListener;
	}

	@Override
	public void setOnUpdateRegistrationIdOfDevice(
			OnUpdateRegistrationIdOfDeviceListener onUpdateRegistrationIdOfDeviceListener) {
		this.onUpdateRegistrationIdOfDeviceListener = onUpdateRegistrationIdOfDeviceListener;
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		// 맨앞에 ASCII CODE 65279가 붙음
		result = result.replace(String.valueOf((char) 65279), "");

		if (result.equals("true")) {
			if(isCheckRegistrationIdOfDevice && onCheckRegistrationIdOfDeviceListener != null){
				onCheckRegistrationIdOfDeviceListener.onSuccessCheckRegistrationIdOfDevice(true);
			}else if(isUpdateRegistrationIdOfDevice && onUpdateRegistrationIdOfDeviceListener != null){
				onUpdateRegistrationIdOfDeviceListener.onSuccessUpdateRegistrationIdOfDevice();
			}
		} else if (result.equals("false")) {
			if(isCheckRegistrationIdOfDevice && onCheckRegistrationIdOfDeviceListener != null){
				onCheckRegistrationIdOfDeviceListener.onSuccessCheckRegistrationIdOfDevice(false);
			}else if(isUpdateRegistrationIdOfDevice && onUpdateRegistrationIdOfDeviceListener != null){
				onUpdateRegistrationIdOfDeviceListener.onFailUpdateRegistrationIdOfDevice();
			}
		}
	}

	@Override
	public void onFailToConnectDataBase() {
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		if (isCheckRegistrationIdOfDevice
				&& onCheckRegistrationIdOfDeviceListener != null) {
			onCheckRegistrationIdOfDeviceListener
					.onTimeoutToCheckRegistrationIdOfDevice();
		}else if (isUpdateRegistrationIdOfDevice
				&& onUpdateRegistrationIdOfDeviceListener != null) {
			onUpdateRegistrationIdOfDeviceListener
					.onTimeoutToUpdateRegistrationIdOfDevice();
		}
	}
}
