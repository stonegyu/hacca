package com.broccoli.hacca.searchapi;

public interface SearchAPI {
	void searchStudentInfo(String studentLoginId);
	void searchStudentsInfo(String studentDepartment,String searchText,String limit);
	void searchCompanyInfo(String companyLoginId);
	void searchCompanysInfo(String companyBusinessType,String searchText,String limit);
	void searchProfessorComment(String studentLoginId);
	void searchNoticeBoard(String category,String searchText,String limit);
	void searchProfessorInfo(String professorLoginId);
}
