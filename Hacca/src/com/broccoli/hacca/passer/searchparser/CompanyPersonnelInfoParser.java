package com.broccoli.hacca.passer.searchparser;

import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.passer.JsonParser;
import com.broccoli.hacca.passer.Parser;

public class CompanyPersonnelInfoParser implements ParsingInfoType{

	@Override
	public PageInfo parseInfo(String parsingValue) {
		
		Parser parser = new Parser(new JsonParser(parsingValue));
		
		CompanyPersonnelInfo companyPersonnelInfo = new CompanyPersonnelInfo();
		
		companyPersonnelInfo.setCompanyLoginId(getParseInfo(parser, "companyLoginId"));
		companyPersonnelInfo.setCompanyWorkingArea(getParseInfo(parser, "companyArea"));
		companyPersonnelInfo.setCompanyBusinessType(getParseInfo(parser, "companyBusinessType"));
		companyPersonnelInfo.setCompanyHomepage(getParseInfo(parser, "companyHomePage"));
		companyPersonnelInfo.setCompanyEmail(getParseInfo(parser, "companyMail"));
		companyPersonnelInfo.setCompanyName(getParseInfo(parser, "companyName"));
		companyPersonnelInfo.setComapanyRecruitmentPeriod(getParseInfo(parser, "companyRecruitPeriod"));
		companyPersonnelInfo.setCompanyRecruitmentVolume(getParseInfo(parser, "companyRecruitVolume"));
		companyPersonnelInfo.setCompanySalary(getParseInfo(parser, "companySalary"));
		companyPersonnelInfo.setCompanySize(getParseInfo(parser, "companySize"));
		companyPersonnelInfo.setCompanyWorkType(getParseInfo(parser, "companyWorkType"));
		companyPersonnelInfo.setCompanyRecruitmentArea(getParseInfo(parser, "companyRecruitmentArea"));
		companyPersonnelInfo.setCompanyCertificationNumber(getParseInfo(parser,"professorCode"));
		PageInfo pageInfo = new PageInfo();
		pageInfo.setCompanyPersonnelInfo(companyPersonnelInfo);
		
		return pageInfo;
	}
	
	private String getParseInfo(Parser parser,String parsingKey){
		String result;
		
		try{
			result = parser.parse(parsingKey).get(0);
		}catch(IndexOutOfBoundsException e){
			result="";
		}
		
		return result;
	}
}
