package com.broccoli.hacca.gcm;

public interface OnGCMSenderListner {
	void onCompletedSendMessage();
	void onFailToSendMessage();
	void onTimeoutToSendMessage();
}
