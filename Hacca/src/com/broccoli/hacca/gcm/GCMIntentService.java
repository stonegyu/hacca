package com.broccoli.hacca.gcm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.SplashPageActivity;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.registrationidofdeviceapi.RegistrationIdOfDeviceGetter;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	private static final String tag = "GCMIntentService";
	public static final String SEND_ID = "656800541625";

	public GCMIntentService() {
		this(SEND_ID);
	}

	public GCMIntentService(String senderId) {
		super(senderId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
			showMessage(context, intent);
		}
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.d(tag, "onError. errorId : " + errorId);
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		Log.d(tag, "onRegistered. regId : " + regId);
		RegistrationIdOfDeviceGetter.getInstance().setDeviceId(regId);
	}

	@Override
	protected void onUnregistered(Context context, String regId) {
		Log.d(tag, "onUnregistered. regId : " + regId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		Log.d(tag, "onRecoverableError. errorId : " + errorId);
		return super.onRecoverableError(context, errorId);
	}
	
	private void showMessage(Context context, Intent intent) {
		String title = intent.getStringExtra("title");
		String msg = intent.getStringExtra("message");

		if(title.equals("autoLogin") && msg.equals("false")){
			releaseAutoLogin(context,false);
		}else{
			awakeDevice(context, title, msg);
		}
	}
	
	private void releaseAutoLogin(Context context,boolean isAutoLogin){
		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(context);
		internalStorageAPI.setAutoLogin(isAutoLogin);
		internalStorageAPI.setLoginId(null);
		internalStorageAPI.setRegistrationIdOfDevice(null);
		internalStorageAPI.setUserName(null);
		internalStorageAPI.setUserType(null);
	}
	
	private void awakeDevice(Context context,String title,String msg){
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Activity.NOTIFICATION_SERVICE);


		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				new Intent(context, SplashPageActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);

		Notification notification = new Notification();
		notification.icon = R.drawable.ic_launcher;
		notification.when = System.currentTimeMillis();
		notification.vibrate = new long[] { 500, 500, 500, 500 };
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(context, title, msg, pendingIntent);

		notificationManager.notify(0, notification);
	}
}