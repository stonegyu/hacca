package com.broccoli.hacca.gcm;

import android.util.Log;

import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPI;
import com.broccoli.hacca.dataconnectapi.DataBaseConnectAPIImpl;
import com.broccoli.hacca.dataconnectapi.OnDataBaseConnectAPIListener;
import com.broccoli.hacca.pageinfo.PushNotificationInfo;
import com.broccoli.hacca.urlfactory.UrlDataStorage;
import com.broccoli.hacca.urlfactory.UrlFactory;
import com.broccoli.hacca.urlfactory.UrlParameterType;
import com.broccoli.hacca.urlfactory.UrlType;

public class GCMSenderImpl implements GCMSender,OnDataBaseConnectAPIListener{

	private final String TAG = "GCMSenderImpl";
	private OnGCMSenderListner onGCMSenderListner;

	public GCMSenderImpl(OnGCMSenderListner onGCMSenderListner){
		this.onGCMSenderListner = onGCMSenderListner;
	}
	
	@Override
	public void sendGCMMessage(PushNotificationInfo pushNoticeInfo) {
		UrlDataStorage urlDataStorage = new UrlDataStorage(UrlType.PUSHNOTICE_SEND_MESSAGE);
		
		urlDataStorage.setParameter(UrlParameterType.PUSHNOTICE_SENDER_LOGINID, pushNoticeInfo.getSenderLoginId());
		urlDataStorage.setParameter(UrlParameterType.PUSHNOTICE_RECEIVER_LOGINID, pushNoticeInfo.getReceiverLoginId());
		urlDataStorage.setParameter(UrlParameterType.PUSHNOTICE_TITLE, pushNoticeInfo.getTitle());
		urlDataStorage.setParameter(UrlParameterType.PUSHNOTICE_MESSAGE, pushNoticeInfo.getMessage());
		
		String url = UrlFactory.getInstance().getUrl(urlDataStorage);
		
		DataBaseConnectAPI dataBaseConnectAPI = new DataBaseConnectAPIImpl(this);
		dataBaseConnectAPI.connectDataBase(url);
	}

	@Override
	public void onSuccessToConnectDataBase(String result) {
		
		Log.i(TAG, result);
		//맨앞에 ASCII CODE 65279가 붙음
		result = result.replace(String.valueOf((char)65279), "" );
		
		if(result.equals("true")) {
			onGCMSenderListner.onCompletedSendMessage();
		}else if(result.equals("false")){
			onGCMSenderListner.onFailToSendMessage();
		}
	}

	@Override
	public void onFailToConnectDataBase() {
		onGCMSenderListner.onFailToSendMessage();
	}

	@Override
	public void onDataBaseConnectTimeOut() {
		onGCMSenderListner.onTimeoutToSendMessage();
	}
}
