package com.broccoli.hacca.dialog;

public interface OnDeviceChangeQuestionListener {
	void onPositiveDeviceChange();
	void onNegativeDeviceChange();
}
