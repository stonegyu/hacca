package com.broccoli.hacca.dialog;

public interface OnCommentDialogListener {
	void onCompletedCommentDialog(String content);
}
