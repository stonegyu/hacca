package com.broccoli.hacca.dialog;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;

public class CustionDatePickerDialog {

	private OnCustomDatePickerDialogListener dialogListener;
	private Context context;
	private int id;

	public CustionDatePickerDialog(Context context) {
		this.context = context;
	}

	public void setOnDialogListener(OnCustomDatePickerDialogListener dialogListener,int id) {
		this.dialogListener = dialogListener;
		this.id = id;
	}

	public void show() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_YEAR, 1);
		final DatePickerDialog dateDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
		    boolean fired = false;
		    public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
		        
		        if (fired == true) {
		        	dialogListener.onCompletedCompanyDatePickerDialog(id, year, monthOfYear, dayOfMonth);
		            return;
		        } else {
		            fired = true;
		        }
		    }
		}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		dateDialog.show();
	}
}
