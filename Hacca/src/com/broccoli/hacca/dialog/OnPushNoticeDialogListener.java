package com.broccoli.hacca.dialog;

public interface OnPushNoticeDialogListener {
	void onCompletedPushNoticeDialog(String pushComment);
}
