package com.broccoli.hacca.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.broccoli.hacca.R;

public class CommentDialog extends Dialog implements OnTouchListener, OnCompleteCommentExamDialogListener{
	
	private ImageButton commitBtn;
	private ImageButton commentLoadBtn;
	private ImageButton commentExitBtn;
	private EditText editText;
	private String title;
	private String content;
	private Context mContext;
	private OnCommentDialogListener dialogListener;
	ProfessorCommentExamDialog  professorCommentExamDialog;
	public CommentDialog(Context context) {
		super(context);
		mContext=context;
	}
	
	public void setOnDialogListener(OnCommentDialogListener dialogListener){
		this.dialogListener = dialogListener;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setContent(String content){
		this.content = content;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setContentView(R.layout.professor_commentboard_dialog_layout);
		
		commitBtn = (ImageButton) findViewById(R.id.commentok_button);
		commitBtn.setOnTouchListener(this);
		commentLoadBtn = (ImageButton) findViewById(R.id.commentload_btn);
		commentLoadBtn.setOnTouchListener(this);
		commentExitBtn = (ImageButton) findViewById(R.id.professor_comment_exit);
		commentExitBtn.setOnTouchListener(this);
		editText = (EditText)findViewById(R.id.edit_comment);
		editText.setText(content);
		
		((TextView)findViewById(R.id.professorid)).setText(title);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(v.getId() == R.id.commentok_button){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.recommodation_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.recommodation_btn_01_basic);
				dialogListener.onCompletedCommentDialog(editText.getText().toString());
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.recommodation_btn_01_basic);
			}
			
		}
		if(v.getId() == R.id.commentload_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.loadrecommodation_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.loadrecommodation_btn_01_basic);

				professorCommentExamDialog= new ProfessorCommentExamDialog(mContext);
				professorCommentExamDialog.setOnDialogListener(this);
				professorCommentExamDialog.show();
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.loadrecommodation_btn_01_basic);
			}
		}
		if(v.getId() == R.id.professor_comment_exit){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.close_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
				dismiss();
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
		}
		return true;
	}

	@Override
	public void onCompletedCommentExamDialog(String content) {
		// TODO Auto-generated method stub
		editText.setText(content);
		professorCommentExamDialog.dismiss();
		
	}
}
