package com.broccoli.hacca.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class DeviceChangeQuestionDalog {

	private OnDeviceChangeQuestionListener dialogListener;
	private Context context;

	public DeviceChangeQuestionDalog(Context context) {
		this.context = context;
	}

	public void setOnDialogListener(OnDeviceChangeQuestionListener dialogListener) {
		this.dialogListener = dialogListener;
	}

	public void show() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("현재 디바이스로 변경하시겠습니까?")
				.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialogListener.onPositiveDeviceChange();
					}
				}).setNegativeButton("NO", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialogListener.onNegativeDeviceChange();
					}
				});
		builder.create().show();
	}
}
