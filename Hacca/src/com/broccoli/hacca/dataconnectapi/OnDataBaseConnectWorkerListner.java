package com.broccoli.hacca.dataconnectapi;

public interface OnDataBaseConnectWorkerListner {
	void onCompletedDataBaseConnect(String result);
	void onDataBaseConnectTimeOut();
}
