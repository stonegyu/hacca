package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.ProfessorPersonnelInfo;
import com.broccoli.hacca.passer.searchparser.ProfessorPersonnelInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class ProfessorViewProfilePageActivity extends Activity implements
		OnClickListener, OnTouchListener, OnSearchAPIListener {
	TextView professorViewName, professorViewLevel, professorViewDepartment,
			professorViewMail;
	ImageButton professorViewEditBtn;
	private LoadingProgressDialog loadingProgressDialog;

	String professorLoginId, registrationIdOfDevice;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.professor_view_profile_page_activity_layout);
		professorViewName = (TextView) findViewById(R.id.professor_view_name);
		professorViewLevel = (TextView) findViewById(R.id.professor_view_level);
		professorViewDepartment = (TextView) findViewById(R.id.professor_view_department);
		professorViewMail = (TextView) findViewById(R.id.professor_view_mail);
		professorViewEditBtn = (ImageButton) findViewById(R.id.professor_view_editbtn);
		professorViewEditBtn.setOnClickListener(this);
		professorViewEditBtn.setOnTouchListener(this);
		loadingProgressDialog = new LoadingProgressDialog(this);

		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);

		professorLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();
		
		if (professorLoginId != null) {
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new ProfessorPersonnelInfoParser());
			searchAPI.searchProfessorInfo(professorLoginId);
			loadingProgressDialog.show();
		}
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		ImageButton view = (ImageButton) v;
		if (view.getId() == R.id.professor_view_editbtn) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
		} 
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.professor_view_editbtn){
			startActivity(new Intent(this, ProfessorEditProfilePageActivity.class));
		}
	}
	private void setProfessorInfo(ProfessorPersonnelInfo professorPersonnelInfo) {
		professorViewName.setText(professorPersonnelInfo.getProfessorName());
		professorViewLevel.setText(professorPersonnelInfo.getProfessorLevel());
		professorViewDepartment.setText(professorPersonnelInfo.getProfessorDepartment());
		professorViewMail.setText(professorPersonnelInfo.getProfessorEmail());
	}
	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		// TODO Auto-generated method stub
		ProfessorPersonnelInfo professorPersonnelInfo = pageInfo
				.getProfessorPersonnelInfo();
		setProfessorInfo(professorPersonnelInfo);

		loadingProgressDialog.dismiss();
	}

	@Override
	public void onFailSearch() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSearch() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

}
