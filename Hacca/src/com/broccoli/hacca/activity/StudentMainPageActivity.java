package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.broccoli.hacca.R;



public class StudentMainPageActivity extends Activity implements OnClickListener, OnTouchListener{

	ImageButton studentPushBtn, studentNoticeBtn, studentJobBtn, studentProfileBtn;
	ImageButton studentPushBtnSub, studentNoticeBtnSub, studentJobBtnSub, studentProfileBtnSub;
	TextView studentPushCount;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.student_main_page_layout);
		studentPushBtn = (ImageButton)findViewById(R.id.student_push_btn);
		studentNoticeBtn = (ImageButton)findViewById(R.id.student_notice_btn);
		studentJobBtn = (ImageButton)findViewById(R.id.student_job_btn);
		studentProfileBtn = (ImageButton)findViewById(R.id.student_profile_btn);
		studentPushBtnSub = (ImageButton)findViewById(R.id.student_push_direction_btn);
		studentNoticeBtnSub = (ImageButton)findViewById(R.id.student_notice_direction_btn);
		studentJobBtnSub = (ImageButton)findViewById(R.id.student_job_direction_btn);
		studentProfileBtnSub = (ImageButton)findViewById(R.id.student_profile_direction_btn);
				
		studentPushCount = (TextView)findViewById(R.id.student_push_count_text);
		
		studentPushBtn.setOnClickListener(this);
		studentNoticeBtn.setOnClickListener(this);
		studentJobBtn.setOnClickListener(this);
		studentProfileBtn.setOnClickListener(this);
		studentPushBtn.setOnTouchListener(this);
		studentNoticeBtn.setOnTouchListener(this);
		studentJobBtn.setOnTouchListener(this);
		studentProfileBtn.setOnTouchListener(this);
		
		///pushcount 로직
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		ImageButton view = (ImageButton)v;
		if(v.getId() == R.id.student_push_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_push_02_active);
				studentPushBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_push_01_basic);
				studentPushBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_push_01_basic);
				studentPushBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.student_notice_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_notice_02_active);
				studentNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_notice_01_basic);
				studentNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_notice_01_basic);
				studentNoticeBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.student_job_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_job_02_active);
				studentJobBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_job_01_basic);
				studentJobBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_job_01_basic);
				studentJobBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		if(v.getId() == R.id.student_profile_btn){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.main_menu_profile_02_active);
				studentProfileBtnSub.setBackgroundResource(R.drawable.select_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				studentProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.main_menu_profile_01_basic);
				studentProfileBtnSub.setBackgroundResource(R.drawable.select_btn_01_basic);
			}
		}
		return false;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.student_push_btn){
			startActivity(new Intent(this,PushNotificationListPageActivity.class));
		}
		if(v.getId() == R.id.student_notice_btn){
			startActivity(new Intent(this,NoticeBoardPageActivity.class));
		}
		if(v.getId() == R.id.student_job_btn){
			startActivity(new Intent(this,StudentListPageActivity.class));
		}
		if(v.getId() == R.id.student_profile_btn){
			startActivity(new Intent(this,StudentViewProfilePageActivity.class));
		}
	}

	
}
