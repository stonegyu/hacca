package com.broccoli.hacca.activity;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.NoticeBoardInfo;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;

public class NotificationAddActivity extends Activity implements
		OnTouchListener, OnClickListener, OnItemSelectedListener, OnRegisterAPIListener, OnFocusChangeListener {

	private EditText titleBox, contextBox;
	private Spinner categoryBox;
	private ImageButton notificationAddBtn;
	private String[] notificationCategoryList;
	private String notificationCategory;
	private ArrayAdapter<String> notificationCategoryAdapter;
	private ImageButton notificationAddExit;
	private String professorLoginId;
	private LoadingProgressDialog loadingProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification_add_page);

		notificationCategoryList = getResources().getStringArray(
				R.array.notification_category);
		categoryBox = (Spinner) findViewById(R.id.notification_add_category_box);
		notificationCategoryAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, notificationCategoryList);
		categoryBox.setAdapter(notificationCategoryAdapter);
		notificationCategoryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoryBox.setSelection(0);
		categoryBox.setOnItemSelectedListener(this);

		titleBox = (EditText) findViewById(R.id.notification_add_title_box);
		contextBox = (EditText) findViewById(R.id.notification_add_context_box);
		notificationAddBtn = (ImageButton) findViewById(R.id.notification_add_commit);
		notificationAddExit = (ImageButton) findViewById(R.id.notification_add_exit);
		notificationAddExit.setOnTouchListener(this);
		notificationAddExit.setOnClickListener(this);
		titleBox.setOnTouchListener(this);
		categoryBox.setOnTouchListener(this);
		contextBox.setOnTouchListener(this);
		notificationAddBtn.setOnTouchListener(this);
		notificationAddBtn.setOnClickListener(this);
		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);
		storageAPI.setLoginId("1");
		professorLoginId = storageAPI.getLoginId();
		loadingProgressDialog = new LoadingProgressDialog(this);
		titleBox.setOnFocusChangeListener(this);
		contextBox.setOnFocusChangeListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		if (v.getId() == R.id.notification_add_commit) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.save_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
		}
		if (v.getId() == R.id.notification_add_exit) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.close_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.notification_add_commit) {
			loadingProgressDialog.show();
			RegisterAPI registerAPI = new RegisterAPIImpl(this);
			registerAPI.registerNoticeBoard(getFilledNoticeBoardInfo());
		}
		if (v.getId() == R.id.notification_add_exit) {
			finish();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent.getId() == R.id.notification_add_category_box) {
			notificationCategory = parent.getItemAtPosition(position)
					.toString();
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccessRegister() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "등록에 성공하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailRegister() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		
		Toast.makeText(getApplicationContext(), "등록에 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDupplication() {
		// TODO Auto-generated method stub
		
	}
	
	private NoticeBoardInfo getFilledNoticeBoardInfo() {

		NoticeBoardInfo noticeBoardInfo = new NoticeBoardInfo();

		noticeBoardInfo.setTitle(titleBox.getText().toString());
		noticeBoardInfo.setContent(contextBox.getText().toString());
		noticeBoardInfo.setCategory(notificationCategory);
		noticeBoardInfo.setProfessorLoginId(professorLoginId);
		noticeBoardInfo.setNumber(null);

		return noticeBoardInfo;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		EditText view = (EditText)v;
		if(hasFocus){
			if (v.getId() == R.id.notification_add_title_box) {
				view.setBackgroundResource(R.drawable.title_box_02_active);
			}
			if (v.getId() == R.id.notification_add_context_box) {
				view.setBackgroundResource(R.drawable.text_box_02_active);
			}
		}else{
			if (v.getId() == R.id.notification_add_title_box) {
				view.setBackgroundResource(R.drawable.title_box_01_basic);
			}
			if (v.getId() == R.id.notification_add_context_box) {
				view.setBackgroundResource(R.drawable.text_box_01_basic);
			}
		}
	}

}
