package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.CompanyPersonnelInfoParser;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;
import com.broccoli.hacca.updateapi.OnUpdateAPIListener;
import com.broccoli.hacca.updateapi.UpdateAPI;
import com.broccoli.hacca.updateapi.UpdateAPIImpl;

public class CompanyEditProfilePageActivity extends Activity implements
		OnItemSelectedListener, OnClickListener, OnTouchListener,
		OnRegisterAPIListener, OnSearchAPIListener, OnFocusChangeListener, OnUpdateAPIListener {

	private EditText companyId, companyName, companyPasswd, companyRepasswd,
			companyRecruitmentArea, companyWorkingArea, companySalary,
			companyRecruitmentVolume, comapanyRecruitmentPeriod, companyEmail,
			companyHomepage, companyCertificationNumber;

	private Spinner companySizeSpinner, companyBusinessTypeSpinner,
			companyWorkTypeSpinner;

	private ArrayAdapter<String> companySizeAdapter,
			companyBusinessTypeAdapter, companyWorkTypeAdapter;

	private String[] companySizeList;
	private String[] companyBusinessTypeList;
	private String[] companyWorkTypeList;

	private String companySize, companyBusinessType, companyWorkType;

	private ImageButton companyEditProfilePageCommitBtn,
			companyCertificationBtn;

	private String companyLoginId;

	private String registrationIdOfDevice;

	private LoadingProgressDialog loadingProgressDialog;

	private Boolean IsProfessorCodeSuccess = false, IsDupplicationChecked = false;
	
	private Button companyDuplicationBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.company_edit_profile_page_activity_layout);

		companySizeList = getResources().getStringArray(
				R.array.company_size_list);
		companyBusinessTypeList = getResources().getStringArray(
				R.array.company_businesstype_list);
		companyWorkTypeList = getResources().getStringArray(
				R.array.company_worktype_list);

		companyId = (EditText) findViewById(R.id.company_id);
		companyName = (EditText) findViewById(R.id.company_name);
		companyPasswd = (EditText) findViewById(R.id.company_passwd);
		companyRepasswd = (EditText) findViewById(R.id.company_repasswd);
		companyRecruitmentArea = (EditText) findViewById(R.id.company_recruitment_area);
		companyWorkingArea = (EditText) findViewById(R.id.company_working_area);
		companySalary = (EditText) findViewById(R.id.company_salary);
		companyRecruitmentVolume = (EditText) findViewById(R.id.company_recruitment);
		comapanyRecruitmentPeriod = (EditText) findViewById(R.id.company_recruitment_period);
		companyEmail = (EditText) findViewById(R.id.company_email);
		companyHomepage = (EditText) findViewById(R.id.company_homepage);
		companyCertificationNumber = (EditText) findViewById(R.id.company_certification_number);

		companyId.setOnFocusChangeListener(this);
		companyName.setOnFocusChangeListener(this);
		companyPasswd.setOnFocusChangeListener(this);
		companyRepasswd.setOnFocusChangeListener(this);
		companyRecruitmentArea.setOnFocusChangeListener(this);
		companyWorkingArea.setOnFocusChangeListener(this);
		companySalary.setOnFocusChangeListener(this);
		companyRecruitmentVolume.setOnFocusChangeListener(this);
		comapanyRecruitmentPeriod.setOnFocusChangeListener(this);
		companyEmail.setOnFocusChangeListener(this);
		companyHomepage.setOnFocusChangeListener(this);
		companyCertificationNumber.setOnFocusChangeListener(this);

		companySizeSpinner = (Spinner) findViewById(R.id.company_size);
		companySizeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, companySizeList);
		companySizeSpinner.setAdapter(companySizeAdapter);
		companySizeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		companySizeSpinner.setSelection(0);
		companySizeSpinner.setOnItemSelectedListener(this);

		companyBusinessTypeSpinner = (Spinner) findViewById(R.id.company_industry);
		companyBusinessTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, companyBusinessTypeList);
		companyBusinessTypeSpinner.setAdapter(companyBusinessTypeAdapter);
		companyBusinessTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		companyBusinessTypeSpinner.setSelection(0);
		companyBusinessTypeSpinner.setOnItemSelectedListener(this);

		companyWorkTypeSpinner = (Spinner) findViewById(R.id.company_working_form);
		companyWorkTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, companyWorkTypeList);
		companyWorkTypeSpinner.setAdapter(companyWorkTypeAdapter);
		companyWorkTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		companyWorkTypeSpinner.setSelection(0);
		companyWorkTypeSpinner.setOnItemSelectedListener(this);

		companySizeSpinner.setFocusable(true);
		companyBusinessTypeSpinner.setFocusable(true);
		companyWorkTypeSpinner.setFocusable(true);
		companySizeSpinner.setFocusableInTouchMode(true);
		companyBusinessTypeSpinner.setFocusableInTouchMode(true);
		companyWorkTypeSpinner.setFocusableInTouchMode(true);

		companySizeSpinner.setOnFocusChangeListener(this);
		companyBusinessTypeSpinner.setOnFocusChangeListener(this);
		companyWorkTypeSpinner.setOnFocusChangeListener(this);

		companyEditProfilePageCommitBtn = (ImageButton) findViewById(R.id.company_commit);
		companyEditProfilePageCommitBtn.setOnClickListener(this);
		companyEditProfilePageCommitBtn.setOnTouchListener(this);

		companyCertificationBtn = (ImageButton) findViewById(R.id.company_certification_btn);
		companyCertificationBtn.setOnClickListener(this);
		companyCertificationBtn.setOnTouchListener(this);

		companyDuplicationBtn = (Button) findViewById(R.id.companyid_duplicationbtn);
		companyDuplicationBtn.setOnClickListener(this);
		loadingProgressDialog = new LoadingProgressDialog(this);

		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);

		companyLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();

		if (companyLoginId != null) {
			// 최초 로그인이 아니기 때문에 비밀번호, 비밀번호 확인, 아이디, 인증번호는 못고치도록 해야함
			companyId.setEnabled(false);
			companyPasswd.setVisibility(View.GONE);
			companyRepasswd.setVisibility(View.GONE);
			companyCertificationNumber.setEnabled(false);
			companyCertificationBtn.setVisibility(View.GONE);
			
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new CompanyPersonnelInfoParser());
			searchAPI.searchCompanyInfo(companyLoginId);
			loadingProgressDialog.show();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View arg1, int position,
			long arg3) {

		switch (parent.getId()) {
		case R.id.company_size:
			companySize = parent.getItemAtPosition(position).toString();
			break;
		case R.id.company_industry:
			companyBusinessType = parent.getItemAtPosition(position).toString();
			break;
		case R.id.company_working_form:
			companyWorkType = parent.getItemAtPosition(position).toString();
			break;
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onClick(View v) {
		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		if (v.getId() == R.id.company_certification_btn) {
			registerAPI.confirmProfessorCode(companyCertificationNumber
					.getText().toString(),false);
		}
		if (v.getId() == R.id.company_commit) {

			if (!isFilledInTheBlanks()) {
				return;
			}

			if (!companyPasswd.getText().toString()
					.equals(companyRepasswd.getText().toString())) {
				Toast.makeText(this, "비밀번호와 비밀번호확인이 일치하지 않습니다.",
						Toast.LENGTH_LONG).show();
			}

			// loginId
			// deviceId
			if(companyLoginId == null){
				registerAPI.registerCompany(getFilledCompanyPersonnelInfo());
			}else{
				UpdateAPI updateAPI = new UpdateAPIImpl(this);
				updateAPI.updateCompanyProfile(getFilledCompanyPersonnelInfo());
			}
			
		}
		if(v.getId() == R.id.companyid_duplicationbtn){
			if(companyLoginId == null){
				IsDupplicationChecked = false;
				registerAPI.idDuplicationCheck(companyId.getText().toString());
			}
		}
		loadingProgressDialog.show();
	}

	private boolean isFilledInTheBlanks() {
		if (!isFilledInTheBlank(companyId.getText().toString(), "아이디")) {
			return false;
		} else if (!isFilledInTheBlank(companyName.getText().toString(), "이름")) {
			return false;
		} else if (companyLoginId == null && !isFilledInTheBlank(companyPasswd.getText().toString(),
				"비밀번호")) {
			return false;
		} else if (companyLoginId == null && !isFilledInTheBlank(companyRepasswd.getText().toString(),
				"비밀번호확인")) {
			return false;
		} else if (!isFilledInTheBlank(companySize, "회사규모")) {
			return false;
		} else if (!isFilledInTheBlank(companyBusinessType, "업종")) {
			return false;
		} else if (!isFilledInTheBlank(companyWorkType, "근무형태")) {
			return false;
		} else if (!isFilledInTheBlank(companyRecruitmentArea.getText()
				.toString(), "모집분야")) {
			return false;
		} else if (!isFilledInTheBlank(companyWorkingArea.getText().toString(),
				"근무지역")) {
			return false;
		} else if (!isFilledInTheBlank(companySalary.getText().toString(), "연봉")) {
			return false;
		} else if (!isFilledInTheBlank(companyRecruitmentVolume.getText()
				.toString(), "모집인원")) {
			return false;
		} else if (!isFilledInTheBlank(comapanyRecruitmentPeriod.getText()
				.toString(), "모집기간")) {
			return false;
		} else if (!isFilledInTheBlank(companyHomepage.getText().toString(),
				"홈페이지")) {
			return false;
		} else if (!isFilledInTheBlank(companyEmail.getText().toString(), "이메일")) {
			return false;
		} else if (!isFilledInTheBlank(companyCertificationNumber.getText()
				.toString(), "인증번호")) {
			return false;
		} else if(companyLoginId == null && IsProfessorCodeSuccess == false){
			Toast.makeText(getApplicationContext(), "인증번호를 확인해 주세요",
					Toast.LENGTH_LONG).show();
			return false;
		}else if(companyLoginId == null && IsDupplicationChecked == false){
			Toast.makeText(getApplicationContext(),"아이디 중복검사를 해주세요.",
					Toast.LENGTH_LONG).show();
			return false;
		}

		return true;
	}

	private boolean isFilledInTheBlank(String value, String name) {

		if (value.toString().length() == 0 || value.equals("선택")) {
			Toast.makeText(getApplicationContext(), name + "을 입력해주세요.",
					Toast.LENGTH_LONG).show();
			return false;
		} else {
			return true;
		}
	}

	private CompanyPersonnelInfo getFilledCompanyPersonnelInfo() {

		CompanyPersonnelInfo companyPersonnelInfo = new CompanyPersonnelInfo();

		companyPersonnelInfo.setCompanyLoginId(companyId.getText().toString());
		companyPersonnelInfo.setCompanyPasswd(companyPasswd.getText().toString());
		companyPersonnelInfo.setCompanyName(companyName.getText().toString());
		companyPersonnelInfo.setCompanyRecruitmentArea(companyRecruitmentArea
				.getText().toString());
		companyPersonnelInfo.setCompanyWorkingArea(companyWorkingArea.getText()
				.toString());
		companyPersonnelInfo.setCompanySalary(companySalary.getText()
				.toString());
		companyPersonnelInfo
				.setCompanyRecruitmentVolume(companyRecruitmentVolume.getText()
						.toString());
		companyPersonnelInfo
				.setComapanyRecruitmentPeriod(comapanyRecruitmentPeriod
						.getText().toString());
		companyPersonnelInfo.setCompanyEmail(companyEmail.getText().toString());
		companyPersonnelInfo.setCompanyHomepage(companyHomepage.getText()
				.toString());
		companyPersonnelInfo
				.setCompanyCertificationNumber(companyCertificationNumber
						.getText().toString());

		companyPersonnelInfo.setRegistrationIdOfDevice(registrationIdOfDevice);

		companyPersonnelInfo.setCompanySize(companySize);
		companyPersonnelInfo.setCompanyBusinessType(companyBusinessType);
		companyPersonnelInfo.setCompanyWorkType(companyWorkType);

		return companyPersonnelInfo;
	}

	@Override
	public void onBackPressed() {
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageButton view = (ImageButton) v;

		if (view.getId() == R.id.company_certification_btn) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.certification_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.certification_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.certification_btn_01_basic);
			}
		} else if (view.getId() == R.id.company_commit) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.save_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
		}

		return false;
	}

	@Override
	public void onSuccessRegister() {
		loadingProgressDialog.dismiss();
		if (companyLoginId == null && !IsProfessorCodeSuccess) {
			IsProfessorCodeSuccess = true;
			Toast.makeText(getApplicationContext(), "인증번호가 확인되었습니다.",
					Toast.LENGTH_SHORT).show();
			companyCertificationBtn.setEnabled(false);
			companyCertificationNumber.setEnabled(false);
			return;
		}
		Toast.makeText(getApplicationContext(), "등록에 성공하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailRegister() {
		loadingProgressDialog.dismiss();
		if (companyLoginId == null && !IsProfessorCodeSuccess) {
			IsProfessorCodeSuccess=false;
			Toast.makeText(getApplicationContext(), "인증번호를 다시 확인해주세요.",
					Toast.LENGTH_SHORT).show();
			return;
		}
		
		Toast.makeText(getApplicationContext(), "등록에 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		CompanyPersonnelInfo companyPersonnelInfo = pageInfo
				.getCompanyPersonnelInfo();
		setCompanyInfo(companyPersonnelInfo);

		loadingProgressDialog.dismiss();
	}

	@Override
	public void onFailSearch() {
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	private void setCompanyInfo(CompanyPersonnelInfo companyPersonnelInfo) {

		companyId.setText(companyPersonnelInfo.getCompanyLoginId());
		companyName.setText(companyPersonnelInfo.getCompanyName());
		companyRecruitmentArea.setText(companyPersonnelInfo
				.getCompanyRecruitmentArea());
		companyWorkingArea
				.setText(companyPersonnelInfo.getCompanyWorkingArea());
		companySalary.setText(companyPersonnelInfo.getCompanySalary());
		companyRecruitmentVolume.setText(companyPersonnelInfo
				.getCompanyRecruitmentVolume());

		companySalary.setText(companyPersonnelInfo.getCompanySalary());
		comapanyRecruitmentPeriod.setText(companyPersonnelInfo
				.getComapanyRecruitmentPeriod());
		companyEmail.setText(companyPersonnelInfo.getCompanyEmail());
		companyHomepage.setText(companyPersonnelInfo.getCompanyHomepage());
		companyCertificationNumber.setText(companyPersonnelInfo
				.getCompanyCertificationNumber());

		companySize = companyPersonnelInfo.getCompanySize();
		companyBusinessType = companyPersonnelInfo.getCompanyBusinessType();
		companyWorkType = companyPersonnelInfo.getCompanyWorkType();
		companySizeSpinner.setSelection(getSpinnerNumber(companySizeList,
				companySize));
		companyBusinessTypeSpinner.setSelection(getSpinnerNumber(
				companyBusinessTypeList, companyBusinessType));
		companyWorkTypeSpinner.setSelection(getSpinnerNumber(
				companyWorkTypeList, companyWorkType));
	}

	private int getSpinnerNumber(String[] list, String value) {
		int number = 0;

		value = value.replace(" ", "");

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(value)) {
				number = i;
			}
		}

		return number;
	}

	@Override
	public void onTimeoutToSearch() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.company_size
				|| v.getId() == R.id.company_industry
				|| v.getId() == R.id.company_working_form) {
			Spinner view = (Spinner) v;
			if (hasFocus) {
				view.setBackgroundResource(R.drawable.textbox_large_02_active);
			} else {
				view.setBackgroundResource(R.drawable.textbox_large_01_basic);
			}
			return;
		}

		EditText view = (EditText) v;
		if (hasFocus) {
			view.setBackgroundResource(R.drawable.textbox_large_02_active);
		} else {
			view.setBackgroundResource(R.drawable.textbox_large_01_basic);
		}
		if (v.getId() == R.id.company_id || v.getId() == R.id.company_name
				|| v.getId() == R.id.company_passwd
				|| v.getId() == R.id.company_repasswd) {
			if (hasFocus) {
				view.setBackgroundResource(R.drawable.textbox_small_02_active);
			} else {
				view.setBackgroundResource(R.drawable.textbox_small_01_basic);
			}
		}

	}

	@Override
	public void onSuccessUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보를 성공적으로 업데이트 하였습니다.",
				Toast.LENGTH_SHORT).show();
		startActivity(new Intent(this, CompanyViewProfilePageActivity.class));
	}

	@Override
	public void onFailUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보 업데이트를 실패 하였습니다. 다시 시도해주세요",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=true;
		Toast.makeText(getApplicationContext(), "중복되지 않는 아이디입니다. 사용하실수 있습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDupplication() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=false;
		Toast.makeText(getApplicationContext(), "중복되는 아이디입니다. 다른 아이디를 사용하세요.",
				Toast.LENGTH_SHORT).show();
	}

}
