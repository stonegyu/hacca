package com.broccoli.hacca.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.broccoli.hacca.R;
import com.broccoli.hacca.adapter.noticeboard.NoticeBoardPagerAdapter;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.opensource.PagerSlidingTabStrip;

public class NoticeBoardPageActivity extends FragmentActivity implements OnClickListener{
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private NoticeBoardPagerAdapter adapter;
	private Button Notification_add_btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.noticeboard_page_activity_layout);
		
		tabs = (PagerSlidingTabStrip)findViewById(R.id.pager_tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		adapter = new NoticeBoardPagerAdapter(this);

		pager.setAdapter(adapter);

		tabs.setViewPager(pager);
		
		//교수타입만 이 버튼이 보여야 함.
		/*
		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(this);
		
		///////!!!!!!!!!!!!!!!! storage를 delete
		internalStorageAPI.setUserType("professor");
		*/
		Notification_add_btn = (Button)findViewById(R.id.notification_add_btn);
		Notification_add_btn.setOnClickListener(this);
		/*
		if(internalStorageAPI.getUserType() == "professor"){
			Notification_add_btn.setOnClickListener(this);
		}else{
			Notification_add_btn.setVisibility(View.GONE);
		}
		*/
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.notification_add_btn){
			this.startActivity(new Intent(this,NotificationAddActivity.class));
		}
	}
}
