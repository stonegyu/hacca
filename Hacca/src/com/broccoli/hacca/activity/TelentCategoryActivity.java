package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.broccoli.hacca.R;

public class TelentCategoryActivity extends Activity implements
		OnClickListener, OnTouchListener {

	ImageButton telentCategoryImage[];

	ImageButton telentCategorySubImage[];
	
	ImageButton telentSearchBtn;
	
	ImageButton telentExitBtn;
	
	EditText telentSearchEditText;

	int telentCategoryId[] = { R.id.telent_category_total,
			R.id.telent_category_muyong, R.id.telent_category_hehwa, 
			R.id.telent_category_sigak, R.id.telent_category_animation,
			R.id.telent_category_passion, R.id.telent_category_urue,
			R.id.telent_category_interea };

	int telentCategorySubId[] = { R.id.telent_category_total_sub,
			R.id.telent_category_muyong_sub, R.id.telent_category_hehwa_sub,
			R.id.telent_category_sigak_sub, R.id.telent_category_animation_sub,
			R.id.telent_category_passion_sub, R.id.telent_category_urue_sub,
			R.id.telent_category_interea_sub };

	int telentCategoryImageId[][] = {
			{ R.drawable.total_job_btn_01_basic,R.drawable.total_job_btn_02_active },
			{ R.drawable.muyong_telent_btn_01_basic,R.drawable.muyong_telent__btn_02_active },
			{ R.drawable.hehwa_telent_btn_01_basic,	R.drawable.hehwa_telent_btn_02_active },
			{ R.drawable.sigak_telent_btn_01_basic,	R.drawable.sigak_telent_btn_02_active },
			{ R.drawable.animation_telent_btn_01_basic,	R.drawable.animation_telent_btn_02_active },
			{ R.drawable.passion_telent_btn_01_basic,R.drawable.passion_telent_btn_02_active },
			{ R.drawable.urue_telent_btn_01_basic,	R.drawable.urue_telent_btn_02_active },
			{ R.drawable.interea_telent_btn_01_basic,R.drawable.interea_telent_btn_02_active } };

	int telentCategorySubImageId[] = { R.drawable.select_btn_01_basic,
			R.drawable.select_btn_02_active };

	private String[] departmentList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.telent_category_list_page);
		telentCategoryImage = new ImageButton[telentCategoryId.length];
		telentCategorySubImage = new ImageButton[telentCategoryId.length];
		
		for (int i = 0; i < telentCategoryId.length; i++) {
			telentCategoryImage[i] = (ImageButton) findViewById(telentCategoryId[i]);
			telentCategorySubImage[i] = (ImageButton) findViewById(telentCategorySubId[i]);
			telentCategoryImage[i].setOnClickListener(this);
			telentCategoryImage[i].setOnTouchListener(this);
		}
		departmentList = getResources().getStringArray(R.array.departments);
		departmentList[0]=null;
		
		telentSearchEditText = (EditText)findViewById(R.id.telent_search_box);
		telentSearchBtn = (ImageButton)findViewById(R.id.telent_search_icon);
		telentExitBtn = (ImageButton) findViewById(R.id.telent_category_exit);
		telentSearchBtn.setOnTouchListener(this);
		telentSearchBtn.setOnClickListener(this);
		telentExitBtn.setOnTouchListener(this);
		telentExitBtn.setOnClickListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		ImageButton view = (ImageButton) v;
		for (int i = 0; i < telentCategoryId.length; i++) {
			if (v.getId() == telentCategoryId[i]) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					view.setBackgroundResource(telentCategoryImageId[i][1]);
					telentCategorySubImage[i]
							.setBackgroundResource(telentCategorySubImageId[1]);
				}
				if (event.getAction() == MotionEvent.ACTION_UP) {
					view.setBackgroundResource(telentCategoryImageId[i][0]);
					telentCategorySubImage[i]
							.setBackgroundResource(telentCategorySubImageId[0]);
				}
				if (event.getAction() == MotionEvent.ACTION_CANCEL) {
					view.setBackgroundResource(telentCategoryImageId[i][0]);
					telentCategorySubImage[i]
							.setBackgroundResource(telentCategorySubImageId[0]);
				}
			}
		}
		if(v.getId() == R.id.telent_search_icon){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.search_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.search_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.search_btn_01_basic);
			}
		}
		if(v.getId() == R.id.telent_category_exit){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.close_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String category;
		Intent intent;
		for (int i = 0; i < telentCategoryId.length; i++) {
			if (v.getId() == telentCategoryId[i]) {
				category = departmentList[i];
				intent = new Intent(this, StudentListPageActivity.class);
				intent.putExtra("category", category);
				startActivity(intent);
			}
		}
		if(v.getId() == R.id.telent_search_icon){
			if(telentSearchEditText.getText().toString().length() == 0){
				Toast.makeText(getApplicationContext(),
						"검색명을 입력해주세요.", Toast.LENGTH_SHORT).show();
			}else{
				intent = new Intent(this, StudentListPageActivity.class);
				intent.putExtra("category", telentSearchEditText.getText().toString());
				telentSearchEditText.setText("");
				startActivity(intent);
			}
		}
		if(v.getId() == R.id.telent_category_exit){
			finish();
		}
	}

}
