package com.broccoli.hacca.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.adapter.student.StudentAdapterItem;
import com.broccoli.hacca.adapter.student.StudentListPageAdapter;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.StudentsInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class StudentListPageActivity extends Activity implements
		OnScrollListener, OnTouchListener, OnSearchAPIListener{
	
	private EditText searchBox;
	private ImageButton searchBtn;
	
	private ImageButton exitBtn;

	private View footerView;
	
	private ArrayList<StudentAdapterItem> studentAdapterItems;
	private StudentListPageAdapter studentListPageAdapter;
	private ExpandableListView expandableListView;
	
	private boolean isSearchingData = false;
	
	private SearchAPI searchAPI;
	
	private DisplayMetrics metrics;
	
	private String searchText = null;
	
	Intent intent = null;
	private TextView CategoryNameTv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.student_list_page_activity_layout);
		CategoryNameTv = (TextView)findViewById(R.id.student_list_category_name);
		intent = getIntent();
		if(intent != null && searchText == null){
			searchText = intent.getStringExtra("category");
			if(searchText == null){
				CategoryNameTv.setText("전체보기");
			}else{
				CategoryNameTv.setText(searchText);
			}
			
		}
		
		searchAPI = new SearchAPIImpl(this,new StudentsInfoParser());
		
		expandableListView = (ExpandableListView) findViewById(R.id.student_list_page_activity_expandabellistview);

		searchBox = (EditText) findViewById(R.id.student_list_page_activity_layout_search_box);
		searchBtn = (ImageButton) findViewById(R.id.student_list_page_activity_layout_search_icon);	
		
		exitBtn = (ImageButton) findViewById(R.id.student_list_page_activity_layout_exit);
		
		footerView = getLayoutInflater().inflate(R.layout.listviewfooter, null);
		
		studentAdapterItems = new ArrayList<StudentAdapterItem>();
		
		studentListPageAdapter = new StudentListPageAdapter(this,studentAdapterItems);
		studentListPageAdapter.setGroupViewLayout(R.layout.student_list_page_adapter_parent_layout);
		studentListPageAdapter.setChildViewLayout(R.layout.student_list_page_adapter_child_layout);
		studentListPageAdapter.setLoadingViewLayout(R.layout.listviewfooter);
		studentListPageAdapter.setUserType(UserType.PROFESSOR);
		
		expandableListView.setAdapter(studentListPageAdapter);

		expandableListView.setDivider(null);
		expandableListView.setOnScrollListener(this);
		
		searchBtn.setOnTouchListener(this);
		exitBtn.setOnTouchListener(this);
		
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}


	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (firstVisibleItem + visibleItemCount >= totalItemCount
				&& !isSearchingData) {
			expandableListView.addFooterView(footerView);
			
			isSearchingData = true;
			
			searchAPI.searchStudentsInfo(null, searchText, String.valueOf(studentAdapterItems.size()));
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {
		case R.id.student_list_page_activity_layout_search_icon:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				searchBtn.setImageResource(R.drawable.search_btn_02_active);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				searchBtn.setImageResource(R.drawable.search_btn_01_basic);
				if (searchBox.getText().length() == 0) {
					Toast.makeText(getApplicationContext(),
							"검색명을 입력해주세요.", Toast.LENGTH_SHORT).show();
				} else {
					//search
					studentAdapterItems.removeAll(studentAdapterItems);
					searchText = searchBox.getText().toString();
					searchBox.setText("");
					expandableListView.addFooterView(footerView);
					isSearchingData = true;
					searchAPI.searchStudentsInfo(null, searchText, String.valueOf(studentAdapterItems.size()));
				}
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.search_btn_01_basic);
			}
			break;
		case R.id.student_list_page_activity_layout_exit:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.close_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
				finish();
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
			break;
		}
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putString("category", searchText);
		super.onSaveInstanceState(outState);
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		searchText = savedInstanceState.getString("category");
		if(searchText == null){
			CategoryNameTv.setText("전체보기");
		}else{
			CategoryNameTv.setText(searchText);
		}
		super.onRestoreInstanceState(savedInstanceState);
	}


	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		
		if(pageInfo.getStudentPersonnelInfos().size() == 0){
			expandableListView.removeFooterView(footerView);
			return;
		}
		
		for(int i=0;i<pageInfo.getStudentPersonnelInfos().size();i++){
			StudentAdapterItem studentAdapterItem = new StudentAdapterItem(pageInfo.getStudentPersonnelInfos().get(i));
			
			studentAdapterItems.add(studentAdapterItem);
		}
		
		expandableListView.removeFooterView(footerView);
		
		studentListPageAdapter.notifyDataSetChanged();
		isSearchingData = false;
	}

	@Override
	public void onFailSearch() {
		Toast.makeText(this, "로드에 실패했습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSearch() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		/*
		if(searchText != null){
			searchText = null;
			studentAdapterItems.removeAll(studentAdapterItems);
			isSearchingData = false;
			studentListPageAdapter.notifyDataSetChanged();
		}else{
			finish();
		}
		*/
		finish();
	}
	
}
