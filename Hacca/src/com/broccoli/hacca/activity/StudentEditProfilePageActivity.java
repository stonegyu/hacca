package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.StudentPersonnelInfo;
import com.broccoli.hacca.passer.searchparser.StudentPersonnelInfoParser;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;
import com.broccoli.hacca.registrationidofdeviceapi.OnRegisterIdOfDeviceGetterListener;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;
import com.broccoli.hacca.updateapi.OnUpdateAPIListener;
import com.broccoli.hacca.updateapi.UpdateAPI;
import com.broccoli.hacca.updateapi.UpdateAPIImpl;

public class StudentEditProfilePageActivity extends Activity implements
		OnItemSelectedListener, OnClickListener, OnTouchListener,
		OnRegisterAPIListener, OnSearchAPIListener, OnFocusChangeListener, OnUpdateAPIListener {

	private EditText studentId, studentPasswd, studentRepasswd, studentName,
			studentCareer, studentHope, studentBlog, studentMail,
			studentAbility, studentAge;

	private ArrayAdapter<String> studentAgeAdapter = null;
	private ArrayAdapter<String> studentSexAdapter = null;
	private ArrayAdapter<String> studentDepartmentAdapter = null;

	private String[] ageList;
	private String[] sexList;
	private String[] departmentList;

	// private String studentAge;
	private String studentSex;
	private String studentDepartment;

	private Spinner studentAgeSpinner;
	private Spinner studentSexSpinner;
	private Spinner studentDepartmentSpinner;

	private ImageButton studentEditProfilePageCommitBtn;
	private Button studentDuplicationBtn;

	private String studentLoginId;
	private String studentPassword;
	private String registrationIdOfDevice;

	private LoadingProgressDialog loadingProgressDialog;
	
	private Boolean IsDupplicationChecked = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.student_edit_profile_page_activity_layout);

		ageList = getResources().getStringArray(R.array.ages);
		sexList = getResources().getStringArray(R.array.sexs);
		departmentList = getResources().getStringArray(R.array.departments);

		studentId = (EditText) findViewById(R.id.student_id);
		studentPasswd = (EditText) findViewById(R.id.student_passwd);
		studentRepasswd = (EditText) findViewById(R.id.student_repasswd);
		studentName = (EditText) findViewById(R.id.student_name);
		studentCareer = (EditText) findViewById(R.id.student_career);
		studentHope = (EditText) findViewById(R.id.student_hope);
		studentBlog = (EditText) findViewById(R.id.student_blog);
		studentMail = (EditText) findViewById(R.id.student_email);
		studentAbility = (EditText) findViewById(R.id.student_ability);
		studentAge = (EditText) findViewById(R.id.student_age);
		
		studentId.setOnFocusChangeListener(this);
		studentPasswd.setOnFocusChangeListener(this);
		studentRepasswd.setOnFocusChangeListener(this);
		studentName.setOnFocusChangeListener(this);
		studentCareer.setOnFocusChangeListener(this);
		studentHope.setOnFocusChangeListener(this);
		studentBlog.setOnFocusChangeListener(this);
		studentMail.setOnFocusChangeListener(this);
		studentAbility.setOnFocusChangeListener(this);
		studentAge.setOnFocusChangeListener(this);

		// studentAgeSpinner = (Spinner) findViewById(R.id.student_age);
		studentSexSpinner = (Spinner) findViewById(R.id.student_sex);
		studentDepartmentSpinner = (Spinner) findViewById(R.id.student_department);
		/*
		 * studentAgeAdapter = new ArrayAdapter<String>(this,
		 * android.R.layout.simple_spinner_item, ageList); studentAgeAdapter
		 * .setDropDownViewResource
		 * (android.R.layout.simple_spinner_dropdown_item);
		 * studentAgeSpinner.setAdapter(studentAgeAdapter);
		 * studentAgeSpinner.setSelection(0);
		 * studentAgeSpinner.setOnItemSelectedListener(this);
		 */
		studentSexAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sexList);
		studentSexAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		studentSexSpinner.setAdapter(studentSexAdapter);
		studentSexSpinner.setSelection(0);
		studentSexSpinner.setOnItemSelectedListener(this);
		studentSexSpinner.setOnFocusChangeListener(this);
		studentSexSpinner.setFocusable(true);
		studentSexSpinner.setFocusableInTouchMode(true);
		studentSexSpinner.setFocusableInTouchMode(true);

		studentDepartmentAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, departmentList);
		studentDepartmentAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		studentDepartmentSpinner.setAdapter(studentDepartmentAdapter);
		studentDepartmentSpinner.setSelection(0);
		studentDepartmentSpinner.setOnItemSelectedListener(this);
		studentDepartmentSpinner.setOnFocusChangeListener(this);
		studentDepartmentSpinner.setFocusable(true);
		studentDepartmentSpinner.setFocusableInTouchMode(true);
		studentDepartmentSpinner.setFocusableInTouchMode(true);

		studentEditProfilePageCommitBtn = (ImageButton) findViewById(R.id.student_commit);
		studentEditProfilePageCommitBtn.setOnClickListener(this);
		studentEditProfilePageCommitBtn.setOnTouchListener(this);
		
		studentDuplicationBtn = (Button)findViewById(R.id.studentid_duplicationbtn);
		studentDuplicationBtn.setOnClickListener(this);

		loadingProgressDialog = new LoadingProgressDialog(this);

		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);

		studentLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();
		
		if (studentLoginId != null) {
			studentId.setEnabled(false);
			studentDuplicationBtn.setVisibility(View.GONE);
			studentPasswd.setVisibility(View.GONE);
			studentRepasswd.setVisibility(View.GONE);
			
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new StudentPersonnelInfoParser());
			searchAPI.searchStudentInfo(studentLoginId);

			loadingProgressDialog.show();
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		switch (parent.getId()) {
		/*
		 * case R.id.student_age: studentAge =
		 * parent.getItemAtPosition(position).toString(); break;
		 */
		case R.id.student_sex:
			studentSex = parent.getItemAtPosition(position).toString();
			break;
		case R.id.student_department:
			studentDepartment = parent.getItemAtPosition(position).toString();
			break;
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		ImageButton view = (ImageButton)v;
		if(v.getId() == R.id.student_commit){
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.save_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.save_btn_01_basic);
			}
		}
		
		return false;
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.student_commit) {

			if (!isFilledInTheBlanks()) {
				return;
			}

			// loginId
			// deviceId
			if(studentLoginId == null){
				RegisterAPI registerAPI = new RegisterAPIImpl(this);
				registerAPI.registerStudent(getFilledStduentPersonnelInfo());
			}else{
				UpdateAPI updateAPI = new UpdateAPIImpl(this);
				updateAPI.updateStudentProfile(getFilledStduentPersonnelInfo());
			}
			

			
		}
		if(v.getId() == R.id.studentid_duplicationbtn){
			if(studentLoginId == null){
				IsDupplicationChecked = false;
				RegisterAPI registerAPI = new RegisterAPIImpl(this);
				registerAPI.idDuplicationCheck(studentId.getText().toString());
			}
		}
		loadingProgressDialog.show();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onBackPressed() {
		// UIInterface.onBackPressed("profileexit", this);
	}

	private boolean isFilledInTheBlanks() {
		if (!isFilledInTheBlank(studentId.getText().toString(), "아이디")) {
			return false;
		} else if (studentLoginId==null && !isFilledInTheBlank(studentPasswd.getText().toString(),
				"비밀번호")) {
			return false;
		} else if (studentLoginId==null && !isFilledInTheBlank(studentRepasswd.getText().toString(),
				"비밀번호 확인")) {
			return false;
		} else if (!isFilledInTheBlank(studentAbility.getText().toString(),
				"특기")) {
			return false;
		} else if (!isFilledInTheBlank(studentCareer.getText().toString(), "경력")) {
			return false;
		} else if (!isFilledInTheBlank(studentName.getText().toString(), "이름")) {
			return false;
		} else if (!isFilledInTheBlank(studentHope.getText().toString(), "희망분야")) {
			return false;
		} else if (!isFilledInTheBlank(studentBlog.getText().toString(), "블로그")) {
			return false;
		} else if (!isFilledInTheBlank(studentMail.getText().toString(), "메일")) {
			return false;
		} else if (!isFilledInTheBlank(studentAge.getText().toString(), "생년월일")) {
			return false;
		} else if (!isFilledInTheBlank(studentSex, "성별")) {
			return false;
		} else if (!isFilledInTheBlank(studentDepartment, "학과")) {
			return false;
		} else if(studentLoginId == null && IsDupplicationChecked == false){
			Toast.makeText(getApplicationContext(),"아이디 중복검사를 해주세요.",
					Toast.LENGTH_LONG).show();
			return false;
		}

		return true;
	}

	private boolean isFilledInTheBlank(String value, String name) {

		if (value.toString().length() == 0 || value.equals("선택")) {
			Toast.makeText(getApplicationContext(), name + "을 입력해주세요.",
					Toast.LENGTH_LONG).show();
			return false;
		} else {
			return true;
		}
	}

	private StudentPersonnelInfo getFilledStduentPersonnelInfo() {

		StudentPersonnelInfo studentPersonnelInfo = new StudentPersonnelInfo();
		
		studentPersonnelInfo.setStudentName(studentName.getText().toString());
		studentPersonnelInfo.setStudentCareer(studentCareer.getText()
				.toString());
		studentPersonnelInfo.setStudentMail(studentMail.getText().toString());
		studentPersonnelInfo.setStudentHope(studentHope.getText().toString());
		studentPersonnelInfo.setStudentBlog(studentBlog.getText().toString());
		studentPersonnelInfo.setStudentAbility(studentAbility.getText()
				.toString());
		studentPersonnelInfo.setStudentAge(studentAge.getText().toString());
		studentPersonnelInfo.setStudentSex(studentSex);
		studentPersonnelInfo.setStudentDepartment(studentDepartment);
		studentPersonnelInfo.setStudentLoginId(studentId.getText().toString());
		studentPersonnelInfo.setRegistrationIdOfDevice(registrationIdOfDevice);
		studentPersonnelInfo.setStudentPassword(studentPassword);

		return studentPersonnelInfo;
	}

	@Override
	public void onSuccessRegister() {
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "등록에 성공하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailRegister() {
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "등록에 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		StudentPersonnelInfo studentPersonnelInfo = pageInfo
				.getStudentPersonnelInfo();
		setStudentInfo(studentPersonnelInfo);

		loadingProgressDialog.dismiss();
	}

	@Override
	public void onFailSearch() {
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	private void setStudentInfo(StudentPersonnelInfo studentPersonnelInfo) {
		studentId.setText(studentPersonnelInfo.getStudentLoginId());
		studentName.setText(studentPersonnelInfo.getStudentName());
		studentCareer.setText(studentPersonnelInfo.getStudentCareer());
		studentMail.setText(studentPersonnelInfo.getStudentMail());
		studentHope.setText(studentPersonnelInfo.getStudentHope());
		studentBlog.setText(studentPersonnelInfo.getStudentBlog());
		studentAbility.setText(studentPersonnelInfo.getStudentAbility());
		studentAge.setText(studentPersonnelInfo.getStudentAge());
		studentSex = studentPersonnelInfo.getStudentSex();
		studentDepartment = studentPersonnelInfo.getStudentDepartment();
		studentSexSpinner.setSelection(getSpinnerNumber(sexList, studentSex));
		studentDepartmentSpinner.setSelection(getSpinnerNumber(departmentList,
				studentDepartment));
	}

	private int getSpinnerNumber(String[] list, String value) {
		int number = 0;

		value = value.replace(" ", "");

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(value)) {
				number = i;
			}
		}

		return number;
	}

	@Override
	public void onTimeoutToSearch() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.student_sex
				|| v.getId() == R.id.student_department) {
			Spinner view = (Spinner) v;
			if(v.getId() == R.id.student_sex){
				if (hasFocus) {
					view.setBackgroundResource(R.drawable.textbox_small_02_active);
				} else {
					view.setBackgroundResource(R.drawable.textbox_small_01_basic);
				}
			}else if(v.getId() == R.id.student_department){
				if (hasFocus) {
					view.setBackgroundResource(R.drawable.textbox_large_02_active);
				} else {
					view.setBackgroundResource(R.drawable.textbox_large_01_basic);
				}
			}
			return;
		}

		EditText view = (EditText) v;
		if (hasFocus) {
			view.setBackgroundResource(R.drawable.textbox_large_02_active);
		} else {
			view.setBackgroundResource(R.drawable.textbox_large_01_basic);
		}
		if (v.getId() == R.id.student_id || v.getId() == R.id.student_name
				|| v.getId() == R.id.student_passwd
				|| v.getId() == R.id.student_repasswd || v.getId() == R.id.student_age) {
			if (hasFocus) {
				view.setBackgroundResource(R.drawable.textbox_small_02_active);
			} else {
				view.setBackgroundResource(R.drawable.textbox_small_01_basic);
			}
		}

	}

	@Override
	public void onSuccessUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보를 성공적으로 업데이트 하였습니다.",
				Toast.LENGTH_SHORT).show();
		startActivity(new Intent(this, StudentViewProfilePageActivity.class));
	}

	@Override
	public void onFailUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "개인정보 업데이트를 실패 하였습니다. 다시 시도해주세요",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToUpdate() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=true;
		Toast.makeText(getApplicationContext(), "중복되지 않는 아이디입니다. 사용하실수 있습니다.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDupplication() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		IsDupplicationChecked=false;
		Toast.makeText(getApplicationContext(), "중복되는 아이디입니다. 다른 아이디를 사용하세요.",
				Toast.LENGTH_SHORT).show();
	}
}
