package com.broccoli.hacca.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.adapter.company.CompanyAdapterItem;
import com.broccoli.hacca.adapter.company.CompanyListPageAdapter;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.CompanysInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class CompanyListPageActivity extends Activity implements
		OnScrollListener, OnTouchListener, OnSearchAPIListener{
	private TextView CategoryNameTv;
	private EditText searchBox;
	private ImageButton searchBtn;
	
	private ImageButton exitBtn;

	private View footerView;
	
	private ArrayList<CompanyAdapterItem> companyAdapterItems;
	private CompanyListPageAdapter companyListPageAdapter;
	private ExpandableListView expandableListView;
	
	private boolean isSearchingData = false;
	
	private SearchAPI searchAPI;
	
	private DisplayMetrics metrics;
	
	private String searchText = null;
	
	private Intent intent = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.company_list_page_activity_layout);
		
		CategoryNameTv = (TextView)findViewById(R.id.company_list_category_name);
		intent = getIntent();
		if(intent != null && searchText == null){
			searchText = intent.getStringExtra("category");
			if(searchText == null){
				CategoryNameTv.setText("전체보기");
			}else{
				CategoryNameTv.setText(searchText);
			}
		}
		searchAPI = new SearchAPIImpl(this,new CompanysInfoParser());
		
		expandableListView = (ExpandableListView) findViewById(R.id.company_list_page_activity_expandabellistview);

		searchBox = (EditText) findViewById(R.id.company_list_page_activity_layout_search_box);
		searchBtn = (ImageButton) findViewById(R.id.company_list_page_activity_layout_search_icon);	
		
		exitBtn = (ImageButton) findViewById(R.id.company_list_page_activity_layout_exit);
		
		footerView = getLayoutInflater().inflate(R.layout.listviewfooter, null);
		
		companyAdapterItems = new ArrayList<CompanyAdapterItem>();
		
		companyListPageAdapter = new CompanyListPageAdapter(this,companyAdapterItems);
		companyListPageAdapter.setGroupViewLayout(R.layout.company_list_page_adapter_parent_layout);
		companyListPageAdapter.setChildViewLayout(R.layout.company_list_page_adapter_child_layout);
		companyListPageAdapter.setLoadingViewLayout(R.layout.listviewfooter);
		companyListPageAdapter.setUserType(UserType.PROFESSOR);
		
		expandableListView.setAdapter(companyListPageAdapter);

		expandableListView.setDivider(null);
		expandableListView.setOnScrollListener(this);
		
		searchBtn.setOnTouchListener(this);
		exitBtn.setOnTouchListener(this);
		
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}


	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (firstVisibleItem + visibleItemCount >= totalItemCount
				&& !isSearchingData) {
			expandableListView.addFooterView(footerView);
			
			isSearchingData = true;
			
			searchAPI.searchCompanysInfo(null, searchText, String.valueOf(companyAdapterItems.size()));
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {
		case R.id.company_list_page_activity_layout_search_icon:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.search_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.search_btn_01_basic);
				if (searchBox.getText().length() == 0) {
					Toast.makeText(getApplicationContext(),
							"검색명을 입력해주세요.", Toast.LENGTH_SHORT).show();
				} else {
					//search
					companyAdapterItems.removeAll(companyAdapterItems);
					searchText = searchBox.getText().toString();
					searchBox.setText("");
					expandableListView.addFooterView(footerView);
					isSearchingData = true;
					searchAPI.searchCompanysInfo(null, searchText, String.valueOf(companyAdapterItems.size()));
				}
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.search_btn_01_basic);
			}
			break;
		case R.id.company_list_page_activity_layout_exit:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				v.setBackgroundResource(R.drawable.close_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
				finish();
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				v.setBackgroundResource(R.drawable.close_btn_01_basic);
			}
			break;
		}
		return true;
	}

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		
		if(pageInfo.getCompanyPersonnelInfos().size() == 0){
			expandableListView.removeFooterView(footerView);
			return;
		}
		
		for(int i=0;i<pageInfo.getCompanyPersonnelInfos().size();i++){
			CompanyAdapterItem companyAdapterItem = new CompanyAdapterItem(pageInfo.getCompanyPersonnelInfos().get(i)); 
			
			companyAdapterItems.add(companyAdapterItem);
		}
		
		expandableListView.removeFooterView(footerView);
		
		companyListPageAdapter.notifyDataSetChanged();
		isSearchingData = false;
	}

	@Override
	public void onFailSearch() {
		Toast.makeText(this, "로드에 실패했습니다.", Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onBackPressed() {
		/*
		if(searchText != null){
			searchText = null;
			companyAdapterItems.removeAll(companyAdapterItems);
			isSearchingData = false;
			companyListPageAdapter.notifyDataSetChanged();
		}else{
			finish();
		}
		*/
		finish();
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		searchText = savedInstanceState.getString("category");
		if(searchText == null){
			CategoryNameTv.setText("전체보기");
		}else{
			CategoryNameTv.setText(searchText);
		}
		super.onRestoreInstanceState(savedInstanceState);
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putString("category", searchText);
		super.onSaveInstanceState(outState);
	}


	@Override
	public void onTimeoutToSearch() {
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}
	
}
