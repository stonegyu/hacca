package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.CompanyPersonnelInfo;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.CompanyPersonnelInfoParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class CompanyViewProfilePageActivity extends Activity implements OnClickListener, OnTouchListener, OnSearchAPIListener{
	private TextView companyViewName, companyViewSize, companyViewBussinessType, 
						companyViewRecruitmentArea, companyViewWorkingform, companyViewWorkingarea,
						companyViewSalary, companyViewRecruitmentVolume, companyViewRecruitmentPeriod,
						companyViewHomepage, companyViewEmail;
	private ImageButton companyViewEditBtn;
	private LoadingProgressDialog loadingProgressDialog;
	private String companyLoginId, registrationIdOfDevice;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.company_view_profile_page_activity_layout);
		companyViewName = (TextView)findViewById(R.id.company_view_name);
		companyViewSize = (TextView)findViewById(R.id.company_view_size);
		companyViewBussinessType = (TextView)findViewById(R.id.company_view_industry);
		companyViewRecruitmentArea = (TextView)findViewById(R.id.company_view_recruitmentarea);
		companyViewWorkingform = (TextView)findViewById(R.id.company_view_workingform);
		companyViewWorkingarea = (TextView)findViewById(R.id.company_view_workingarea);
		companyViewSalary = (TextView)findViewById(R.id.company_view_salary);
		companyViewRecruitmentVolume = (TextView)findViewById(R.id.company_view_recruitmentvolume);
		companyViewRecruitmentPeriod = (TextView)findViewById(R.id.company_view_recruitmentperiod);
		companyViewHomepage = (TextView)findViewById(R.id.company_view_homepage);
		companyViewEmail = (TextView)findViewById(R.id.company_view_email);
		
		companyViewEditBtn = (ImageButton)findViewById(R.id.company_view_editbtn);
		companyViewEditBtn.setOnClickListener(this);
		companyViewEditBtn.setOnTouchListener(this);
		
		
		loadingProgressDialog = new LoadingProgressDialog(this);
		
		InternalStorageAPI storageAPI = new InternalStorageAPIImpl(this);
		
		companyLoginId = storageAPI.getLoginId();
		registrationIdOfDevice = storageAPI.getRegistrationIdOfDevice();

		if (companyLoginId != null) {
			SearchAPI searchAPI = new SearchAPIImpl(this,
					new CompanyPersonnelInfoParser());
			searchAPI.searchCompanyInfo(companyLoginId);
			loadingProgressDialog.show();
		}
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		ImageButton view = (ImageButton) v;
		if (view.getId() == R.id.company_view_editbtn) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_02_active);
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
			if (event.getAction() == MotionEvent.ACTION_CANCEL) {
				view.setBackgroundResource(R.drawable.profile_edit_btn_01_basic);
			}
		} 
		return false;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.company_view_editbtn){
			startActivity(new Intent(this, CompanyEditProfilePageActivity.class));
		}
	}
	
	private void setCompanyInfo(CompanyPersonnelInfo companyPersonnelInfo) {
		companyViewName.setText(companyPersonnelInfo.getCompanyName());
		companyViewSize.setText(companyPersonnelInfo.getCompanySize());
		companyViewBussinessType.setText(companyPersonnelInfo.getCompanyBusinessType());
		companyViewRecruitmentArea.setText(companyPersonnelInfo.getCompanyRecruitmentArea());
		companyViewWorkingform.setText(companyPersonnelInfo.getCompanyWorkType());
		companyViewWorkingarea.setText(companyPersonnelInfo.getCompanyWorkingArea());
		companyViewSalary.setText(companyPersonnelInfo.getCompanySalary());
		companyViewRecruitmentVolume.setText(companyPersonnelInfo.getCompanyRecruitmentVolume());
		companyViewRecruitmentPeriod.setText(companyPersonnelInfo.getComapanyRecruitmentPeriod());
		companyViewHomepage.setText(companyPersonnelInfo.getCompanyHomepage());
		companyViewEmail.setText(companyPersonnelInfo.getCompanyEmail());
	}
	@Override
	public void onSuccessSearch(PageInfo pageInfo) {
		// TODO Auto-generated method stub
		CompanyPersonnelInfo companyPersonnelInfo = pageInfo
				.getCompanyPersonnelInfo();
		setCompanyInfo(companyPersonnelInfo);

		loadingProgressDialog.dismiss();
	}
	@Override
	public void onFailSearch() {
		// TODO Auto-generated method stub
		loadingProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), "데이터를 가져오는데 실패하였습니다.",
				Toast.LENGTH_SHORT).show();
	}
	@Override
	public void onTimeoutToSearch() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

}
