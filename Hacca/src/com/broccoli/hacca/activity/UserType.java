package com.broccoli.hacca.activity;

import android.content.Context;
import android.content.Intent;

public enum UserType {
	PROFESSOR("professor") {
		@Override
		public void intentMainPage(Context context) {
			context.startActivity(new Intent(context,
					ProfessorMainPageActivity.class));
		}
	},
	COMPANY("company") {
		@Override
		public void intentMainPage(Context context) {
			context.startActivity(new Intent(context,
					CompanyMainPageActivity.class));
		}
	},
	STUDENT("student") {
		@Override
		public void intentMainPage(Context context) {
			context.startActivity(new Intent(context,
					StudentMainPageActivity.class));
		}
	};

	private String name;

	private UserType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static UserType getUserType(String userTypeName) {
		for (UserType userType : UserType.values()) {
			if (userType.getName().equals(userTypeName)) {
				return userType;
			}
		}
		return null;
	}

	public abstract void intentMainPage(Context context);
}
