package com.broccoli.hacca.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.broccoli.hacca.R;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;

public class SplashPageActivity extends Activity {
	protected void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.splash_page);
		Handler handler = new Handler();
		handler.postDelayed(runnable, 2000);
	}

	Runnable runnable = new Runnable() {
		public void run() {
			InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(
					SplashPageActivity.this);

			if (!internalStorageAPI.isAutoLogin()) {
				SplashPageActivity.this.startActivity(new Intent(
						SplashPageActivity.this, LoginPageActivity.class));
			} else {
				UserType.getUserType(internalStorageAPI.getUserType())
						.intentMainPage(SplashPageActivity.this);
			}

			finish();
			
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);
		}
	};
}
