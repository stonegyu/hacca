package com.broccoli.hacca.sqlitedb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.broccoli.hacca.pageinfo.PushNotificationInfo;

public class SendDBOfPushNotificationHandler {
	private SendDBOfPushNotification handler;
	private SQLiteDatabase db;

	SendDBOfPushNotificationHandler(Context cnt) {
		this.handler = new SendDBOfPushNotification(cnt, "hacca_sendDBOfPushNotification.db", null, 1);
		this.db = handler.getWritableDatabase();
	}
	
	public static SendDBOfPushNotificationHandler open(Context ctx) throws SQLException {
		SendDBOfPushNotificationHandler handler = new SendDBOfPushNotificationHandler(ctx);

		return handler;
	}

	public void close() {
		handler.close();
	}

	public long insert(PushNotificationInfo pushNotificationInfo) {
		ContentValues values = new ContentValues();
		values.put("receiverLoginId", pushNotificationInfo.getSenderLoginId());
		values.put("receiverName", pushNotificationInfo.getSenderName());
		values.put("title", pushNotificationInfo.getTitle());
		values.put("message", pushNotificationInfo.getMessage());
		values.put("date", pushNotificationInfo.getDate());
		values.put("isOpen", false);

		return db.insert("hacca_sendDBOfPushNotification", null, values);
	}
	
	public long delete(String number){
		
		return db.delete("hacca_sendDBOfPushNotification", "number="+number, null);
	}

	public long updateIsOpen(String number){
		ContentValues values = new ContentValues();
		values.put("isOpen", true);
		
		return db.update("hacca_sendDBOfPushNotification", values, "number="+number, null);
	}

	public Cursor select(int start, int end) throws SQLException {
		Cursor cursor = db.rawQuery(
				"select * from hacca_sendDBOfPushNotification order by number desc limit "+start+","+end, null);

		if (cursor != null)
			cursor.moveToFirst();

		return cursor;
	}
}
