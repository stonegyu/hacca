package com.broccoli.hacca.adapter.company;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.adapter.OnAdapterSetterListener;
import com.broccoli.hacca.dialog.OnCommentDialogListener;
import com.broccoli.hacca.dialog.CommentDialog;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;

public class CompanyAdapterChildViewSetter implements ChildViewAdapterSettable,
		OnTouchListener, OnCommentDialogListener {

	private View childView;
	private boolean isCompletedSetting = false;
	private CompanyAdapterItem adapterItem;

	private ImageView commitBtn;
	private UserType userType;
	private Context context;
	private CommentDialog professorCommentBoardDialog;

	private Animation anim;

	private String loginId;
	private String name;
	private OnAdapterSetterListener adapterSetterListener;
	
	private LayoutInflater layoutInflater;
	private LinearLayout companyPortfolioLayout;

	public CompanyAdapterChildViewSetter(Context context,
			OnAdapterSetterListener adapterSetterListener) {
		this.context = context;
		this.adapterSetterListener = adapterSetterListener;
		this.layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(context);
		
		loginId = internalStorageAPI.getLoginId();
		name = internalStorageAPI.getUserName();
	}

	@Override
	public void setView(View view, Object personnelInfo) {
		this.childView = view;
		this.adapterItem = (CompanyAdapterItem) personnelInfo;

		commitBtn = (ImageView) childView
				.findViewById(R.id.push_icon);
		
		companyPortfolioLayout = (LinearLayout) childView.findViewById(R.id.company_portfolio_layout);
		
		View companyPortfolioView = layoutInflater.inflate(
				R.layout.company_portfolio_layout, null);
		
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_size)).setText(adapterItem.getCompanyPersonnelInfo().getCompanySize());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_businesstype)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyBusinessType());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_recruitment_area)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyRecruitmentArea());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_worktype)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyWorkType());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_area)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyWorkingArea());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_salary)).setText(adapterItem.getCompanyPersonnelInfo().getCompanySalary());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_recruitvolume)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyRecruitmentVolume());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_recruitperiod)).setText(adapterItem.getCompanyPersonnelInfo().getComapanyRecruitmentPeriod());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_homepage)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyHomepage());
		((TextView)companyPortfolioView.findViewById(R.id.company_portfolio_email)).setText(adapterItem.getCompanyPersonnelInfo().getCompanyEmail());
		
		companyPortfolioLayout.addView(companyPortfolioView);
		commitBtn.setOnTouchListener(this);
	}

	@Override
	public View getView() {
		return childView;
	}

	@Override
	public boolean isCompletedSetting() {
		return isCompletedSetting;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.push_icon) {

			if (event.getAction() == MotionEvent.ACTION_UP) {

				professorCommentBoardDialog = new CommentDialog(
						context);
				professorCommentBoardDialog.setOnDialogListener(this);
				professorCommentBoardDialog.setTitle(name + " 님");
				professorCommentBoardDialog.show();
			}
		}

		return true;
	}

	@Override
	public void onCompletedCommentDialog(String content) {
		professorCommentBoardDialog.dismiss();

		if (userType == UserType.PROFESSOR) {

		} else if (userType == UserType.STUDENT) {

		}
	}

	@Override
	public void startAnimation() {
		if (anim == null) {
			anim = new ScaleAnimation(1, 1, 0, 1);
			anim.setDuration(500);
			childView.startAnimation(anim);
		}
	}

	@Override
	public void resetAnimation() {
		anim = null;
	}
}
