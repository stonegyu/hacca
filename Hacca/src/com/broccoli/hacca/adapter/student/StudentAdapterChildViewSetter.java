package com.broccoli.hacca.adapter.student;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.adapter.OnAdapterSetterListener;
import com.broccoli.hacca.dialog.CommentDialog;
import com.broccoli.hacca.dialog.OnCommentDialogListener;
import com.broccoli.hacca.dialog.OnPushNoticeDialogListener;
import com.broccoli.hacca.dialog.PushNotificationDialog;
import com.broccoli.hacca.gcm.GCMSender;
import com.broccoli.hacca.gcm.GCMSenderImpl;
import com.broccoli.hacca.gcm.OnGCMSenderListner;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPI;
import com.broccoli.hacca.internalstorageapi.InternalStorageAPIImpl;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.pageinfo.ProfessorCommentInfo;
import com.broccoli.hacca.pageinfo.PushNotificationInfo;
import com.broccoli.hacca.passer.searchparser.CommentInfoListParser;
import com.broccoli.hacca.registerapi.OnRegisterAPIListener;
import com.broccoli.hacca.registerapi.RegisterAPI;
import com.broccoli.hacca.registerapi.RegisterAPIImpl;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class StudentAdapterChildViewSetter implements ChildViewAdapterSettable,
		OnSearchAPIListener, OnTouchListener, OnCommentDialogListener,
		OnPushNoticeDialogListener, OnRegisterAPIListener, OnGCMSenderListner {

	private final String TAG = "StudentAdapterChildViewSetter";

	private View childView;
	private boolean isCompletedSetting = false;
	private StudentAdapterItem adapterItem;
	private OnAdapterSetterListener adapterSetterListener;
	private LayoutInflater layoutInflater;

	private TextView blog;
	private TextView mail;

	private LinearLayout commentListLayout;
	private ImageButton commitBtn;
	private Context context;
	private CommentDialog professorCommentBoardDialog;
	private PushNotificationDialog pushNoticeDialog;

	private Animation anim;

	private String loginId;
	private UserType userType;
	private String name;

	private String mComment;

	public StudentAdapterChildViewSetter(Context context,
			OnAdapterSetterListener adapterSetterListener) {
		this.context = context;
		this.adapterSetterListener = adapterSetterListener;
		this.layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		InternalStorageAPI internalStorageAPI = new InternalStorageAPIImpl(
				context);

		loginId = internalStorageAPI.getLoginId();
		name = internalStorageAPI.getUserName();
		userType = UserType.getUserType(internalStorageAPI.getUserType());
	}

	@Override
	public void setView(View view, Object personnelInfo) {
		this.childView = view;
		this.adapterItem = (StudentAdapterItem) personnelInfo;

		commentListLayout = (LinearLayout) childView
				.findViewById(R.id.commet_layout);

		commitBtn = (ImageButton) childView.findViewById(R.id.push_icon);
		commitBtn.setOnTouchListener(this);

		if (userType == UserType.PROFESSOR) {
			commitBtn.setImageResource(R.drawable.comment_image_up);
		} else if (userType == UserType.COMPANY) {
			commitBtn.setImageResource(R.drawable.push_icon_up);
		}

		LinearLayout studentPortfolioLayout = (LinearLayout) childView
				.findViewById(R.id.student_portfolio_layout);

		View studentPortfolioView = layoutInflater.inflate(
				R.layout.student_portfolio_layout, null);

		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_sex)).setText(adapterItem
				.getStudentPersonnelInfo().getStudentSex());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_dateofbrith)).setText("");
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_department))
				.setText(adapterItem.getStudentPersonnelInfo()
						.getStudentDepartment());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_hope)).setText(adapterItem
				.getStudentPersonnelInfo().getStudentHope());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_ability))
				.setText(adapterItem.getStudentPersonnelInfo()
						.getStudentAbility());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_career))
				.setText(adapterItem.getStudentPersonnelInfo()
						.getStudentCareer());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_blog)).setText(adapterItem
				.getStudentPersonnelInfo().getStudentBlog());
		((TextView) studentPortfolioView
				.findViewById(R.id.student_portfolio_email))
				.setText(adapterItem.getStudentPersonnelInfo().getStudentMail());

		studentPortfolioLayout.addView(studentPortfolioView);

		SearchAPI searchAPI = new SearchAPIImpl(this,
				new CommentInfoListParser());
		searchAPI.searchProfessorComment(adapterItem.getStudentPersonnelInfo()
				.getStudentLoginId());
	}

	@Override
	public View getView() {
		return childView;
	}

	@Override
	public boolean isCompletedSetting() {
		return isCompletedSetting;
	}

	public void updateComment(String studnetLoginId) {
		SearchAPI searchAPI = new SearchAPIImpl(this,
				new CommentInfoListParser());
		searchAPI.searchProfessorComment(studnetLoginId);
	}

	private void setComment(ProfessorCommentInfo commentInfo) {

		View commentView = layoutInflater.inflate(
				R.layout.professor_comment_layout, null);

		TextView professorId = (TextView) commentView
				.findViewById(R.id.professor_id);
		TextView comment = (TextView) commentView.findViewById(R.id.comment);

		professorId.setText("[" + commentInfo.getProfessorName() + "]님");
		comment.setText(commentInfo.getComment());

		commentListLayout.addView(commentView);

		if (commentInfo.getProfessorLoginId().equals(loginId)) {
			mComment = commentInfo.getComment();
		}
	}

	@Override
	public void onSuccessSearch(PageInfo pageInfo) {

		commentListLayout.removeAllViews();

		for (ProfessorCommentInfo commentInfo : pageInfo.getCommentInfos()) {
			setComment(commentInfo);
		}

		adapterSetterListener.onCompletedSettings();

		isCompletedSetting = true;
		adapterSetterListener.onCompletedSettings();
	}

	@Override
	public void onFailSearch() {
		adapterSetterListener.onFailSettings();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		if (v.getId() == R.id.push_icon) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				Log.i(TAG, "down");
				if (userType == UserType.PROFESSOR) {
					commitBtn.setImageResource(R.drawable.comment_image_down);

				} else if (userType == UserType.COMPANY) {
					commitBtn.setImageResource(R.drawable.push_icon_down);

				}
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				Log.i(TAG, "up");
				if (userType == UserType.PROFESSOR) {
					commitBtn.setImageResource(R.drawable.comment_image_up);
					
					professorCommentBoardDialog = new CommentDialog(context);
					professorCommentBoardDialog.setOnDialogListener(this);
					professorCommentBoardDialog.setTitle(name + " 님");
					professorCommentBoardDialog.setContent(mComment);
					professorCommentBoardDialog.show();
				} else if (userType == UserType.COMPANY) {
					commitBtn.setImageResource(R.drawable.push_icon_up);
					
					pushNoticeDialog = new PushNotificationDialog(context);
					pushNoticeDialog.setOnDialogListener(this);
					pushNoticeDialog.setTitle(name + " 님");
					pushNoticeDialog.show();
				}
			}
			
		}

		return false;
	}

	@Override
	public void onCompletedCommentDialog(String content) {
		professorCommentBoardDialog.dismiss();

		ProfessorCommentInfo commentInfo = new ProfessorCommentInfo();

		commentInfo.setProfessorLoginId(loginId);
		commentInfo.setStudentLoginId(adapterItem.getStudentPersonnelInfo()
				.getStudentLoginId());
		commentInfo.setComment(content);

		RegisterAPI registerAPI = new RegisterAPIImpl(this);
		registerAPI.registerProfessorComment(commentInfo);
	}

	@Override
	public void onCompletedPushNoticeDialog(String message) {
		pushNoticeDialog.dismiss();

		PushNotificationInfo pushNoticeInfo = new PushNotificationInfo();
		pushNoticeInfo.setSenderLoginId(loginId);
		pushNoticeInfo.setReceiverLoginId(adapterItem.getStudentPersonnelInfo()
				.getStudentLoginId());
		pushNoticeInfo.setTitle(name);
		pushNoticeInfo.setMessage(message);

		GCMSender gcmSender = new GCMSenderImpl(this);
		gcmSender.sendGCMMessage(pushNoticeInfo);
	}

	@Override
	public void onSuccessRegister() {
		Toast.makeText(context, "등록에 성공하였습니다.", Toast.LENGTH_SHORT).show();
		updateComment(adapterItem.getStudentPersonnelInfo().getStudentLoginId());
	}

	@Override
	public void onFailRegister() {
		Toast.makeText(context, "입력에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void startAnimation() {
		if (anim == null) {
			anim = new ScaleAnimation(1, 1, 0, 1);
			anim.setDuration(500);
			childView.startAnimation(anim);
		}
	}

	@Override
	public void resetAnimation() {
		anim = null;
	}

	@Override
	public void onCompletedSendMessage() {
		Toast.makeText(context, "메시지를 보냈습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailToSendMessage() {
		Toast.makeText(context, "메시지전송에 실패하였습니다.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSendMessage() {
		Toast.makeText(context, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToRegister() {
		Toast.makeText(context, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTimeoutToSearch() {
		Toast.makeText(context, "연결시간 지연에따른 연결실패", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNotDupplication() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDupplication() {
		// TODO Auto-generated method stub
		
	}
}
