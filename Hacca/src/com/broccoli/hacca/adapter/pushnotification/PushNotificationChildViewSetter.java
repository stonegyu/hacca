package com.broccoli.hacca.adapter.pushnotification;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.adapter.ChildViewAdapterSettable;
import com.broccoli.hacca.deleteapi.DeleteAPI;
import com.broccoli.hacca.deleteapi.DeleteAPIImpl;
import com.broccoli.hacca.deleteapi.OnDeleteAPIListener;
import com.broccoli.hacca.dialog.LoadingProgressDialog;
import com.broccoli.hacca.dialog.NoticeBoardEditableBoardDialog;

public class PushNotificationChildViewSetter implements ChildViewAdapterSettable,
		OnTouchListener{

	private View childView;
	private boolean isCompletedSetting = false;
	private PushNotificationAdapterItem adapterItem;

	private ImageView deleteBtn;
	private Context context;
	private LoadingProgressDialog loadingProgressDialog;

	private Animation anim;

	private String loginId;
	
	private String content;
	
	private TextView contentTextView;
	
	private OnPushNotificationChildViewSetterListner listener;
	private int groupPosition;

	public PushNotificationChildViewSetter(Context context,
			int groupPosition,OnPushNotificationChildViewSetterListner adapterSetterListener) {
		this.context = context;
		this.groupPosition = groupPosition;
		this.listener = adapterSetterListener;

		// InternalStorageAPI internalStorageAPI = new
		// InternalStorageAPIImpl(context);
		// loginId = internalStorageAPI.getDeviceId();
		// deviceId = internalStorageAPI.getDeviceId();

		loginId = "admin";
	}

	@Override
	public void setView(View view, Object personnelInfo) {
		this.childView = view;
		this.adapterItem = (PushNotificationAdapterItem) personnelInfo;

//		titleTextView = (TextView) childView.findViewById(R.id.inform_date);
//		titleTextView.setText(adapterItem.getNoticeBoardInfo().getDate());
//		
//		contentTextView = (TextView) childView.findViewById(R.id.inform_content);
//		contentTextView.setText(adapterItem.getNoticeBoardInfo().getContent());
//
//		editBtn = (ImageView) childView.findViewById(R.id.inform_editbtn);
//		deleteBtn = (ImageView) childView.findViewById(R.id.inform_deletebtn);

		deleteBtn.setOnTouchListener(this);
	}

	@Override
	public View getView() {
		return childView;
	}

	@Override
	public boolean isCompletedSetting() {
		return isCompletedSetting;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.inform_deletebtn) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				listener.onDeletePushNotification(groupPosition);
			}
		}

		return true;
	}

	@Override
	public void startAnimation() {
		if (anim == null) {
			anim = new ScaleAnimation(1, 1, 0, 1);
			anim.setDuration(500);
			childView.startAnimation(anim);
		}
	}

	@Override
	public void resetAnimation() {
		anim = null;
	}
}
