package com.broccoli.hacca.adapter.pushnotification;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.broccoli.hacca.R;
import com.broccoli.hacca.activity.UserType;
import com.broccoli.hacca.pageinfo.NoticeBoardInfo;
import com.broccoli.hacca.pageinfo.PageInfo;
import com.broccoli.hacca.passer.searchparser.NoticeBoardListParser;
import com.broccoli.hacca.searchapi.OnSearchAPIListener;
import com.broccoli.hacca.searchapi.SearchAPI;
import com.broccoli.hacca.searchapi.SearchAPIImpl;

public class PushNotificationListView implements OnScrollListener,OnPushNotificationListViewApaterChangedListener{
	private View pagerItemView;
	private View footerView;
	private ExpandableListView expandableListView;
	
	private PushNotificationListViewAdapter adapter;
	private ArrayList<PushNotificationAdapterItem> pushNotificationAdapterItems;
	
	private String category;
	
	private boolean isSearchingData = false;
	private Context context;
	
	public PushNotificationListView(Context context,String category) {
		this.context = context;
		this.category = category;
		
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		pagerItemView = layoutInflater.inflate(R.layout.noticeboard_listview_layout, null);
		
		expandableListView = (ExpandableListView) pagerItemView.findViewById(R.id.expandableListView);
		
		footerView = layoutInflater.inflate(R.layout.listviewfooter, null);
		
		pushNotificationAdapterItems = new ArrayList<PushNotificationAdapterItem>();
		
		adapter = new PushNotificationListViewAdapter(context,pushNotificationAdapterItems);
		adapter.setOnPushNotificationListViewAdapterChangedListner(this);
		adapter.setGroupViewLayout(R.layout.noticeboard_adapter_parent_layour);
		adapter.setChildViewLayout(R.layout.noticeboard_adapter_child_layout);
		
		expandableListView.setAdapter(adapter);

		expandableListView.setDivider(null);
		expandableListView.setOnScrollListener(this);
	}

	public View getView(){
		return pagerItemView;
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (firstVisibleItem + visibleItemCount >= totalItemCount
				&& !isSearchingData) {
			
			expandableListView.addFooterView(footerView);
			
			isSearchingData = true;
			
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}
	
	@Override
	public void onDeletePushNotification(int position) {
		pushNotificationAdapterItems.remove(position);
		adapter.notifyDataSetChanged();
	}
}
