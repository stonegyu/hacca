package com.broccoli.hacca.adapter.noticeboard;

public interface OnNoticeBoardListViewAdapterChangedListener {
	void onDeletedNoticeBoard(int groupPosition);
}
