package com.broccoli.hacca.hansungunivcertification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;

public class HansungUnivCertificatorWorker extends AsyncTask<String, Void, String>{
	
	private OnHansungUnivCertificatorWorkerListener onHansungUnivCertificatorWorkerListener;

	public HansungUnivCertificatorWorker(OnHansungUnivCertificatorWorkerListener onHansungUnivCertificatorWorkerListener){
		this.onHansungUnivCertificatorWorkerListener = onHansungUnivCertificatorWorkerListener;
	}
	
	@Override
	protected String doInBackground(String... urls) {
		String result = false+"";

		String url = urls[0];
		String id = urls[1];
		String passwd = urls[2];
		
		HttpClient http = new DefaultHttpClient();
		
		try { 
			ArrayList<NameValuePair> nameValuePairs = 
					new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("id", id));
			nameValuePairs.add(new BasicNameValuePair("passwd", passwd));

			HttpParams params = http.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			HttpPost httpPost = new HttpPost(url);
			UrlEncodedFormEntity entityRequest = 
					new UrlEncodedFormEntity(nameValuePairs, "EUC-KR");
			
			httpPost.setEntity(entityRequest);
			
			http.execute(httpPost);
			
			List<Cookie> cookies = ((DefaultHttpClient)http).getCookieStore().getCookies();
			
			if(cookies.size()>0){
				result = true+"";
			}else{
				result = false+"";
			}
			
//			HttpPost httpPost = new HttpPost("http://info.hansung.ac.kr/fuz/common/include/default/top.jsp");
//			HttpResponse responsePost = http.execute(httpPost);
//			HttpEntity resEntity = responsePost.getEntity();
//			
//			Log.i("test", EntityUtils.toString(resEntity));
//			Log.i("test", cookies.get(0)+"");
			
		}catch (ConnectTimeoutException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		onHansungUnivCertificatorWorkerListener.onCompletedHansungUnivCertification(result);
	}
}
