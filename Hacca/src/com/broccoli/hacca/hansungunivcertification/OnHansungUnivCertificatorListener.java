package com.broccoli.hacca.hansungunivcertification;

public interface OnHansungUnivCertificatorListener {
	void onSuccessToCertificateHansungUniv();
	void onFailToCertificateHansungUniv();
}
